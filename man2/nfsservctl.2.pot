# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: nfsservctl.2:5
#, no-wrap
msgid "NFSSERVCTL"
msgstr ""

# type: TH
#: nfsservctl.2:5
#, no-wrap
msgid "1997-07-16"
msgstr ""

# type: TH
#: nfsservctl.2:5
#, no-wrap
msgid "Linux 2.1.32"
msgstr ""

# type: TH
#: nfsservctl.2:5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: nfsservctl.2:6
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: nfsservctl.2:8
msgid "nfsservctl - syscall interface to kernel nfs daemon"
msgstr ""

# type: SH
#: nfsservctl.2:8
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: nfsservctl.2:10
msgid "B<#include E<lt>linux/nfsd/syscall.hE<gt>>"
msgstr ""

# type: Plain text
#: nfsservctl.2:12
msgid ""
"B<nfsservctl(int >I<cmd>B<, struct nfsctl_arg *>I<argp>B<, union nfsctl_res "
"*>I<resp>B<);>"
msgstr ""

# type: SH
#: nfsservctl.2:12
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: nfsservctl.2:24
#, no-wrap
msgid ""
"/*\n"
" * These are the commands understood by nfsctl().\n"
" */\n"
"#define NFSCTL_SVC              0       /* This is a server process. */\n"
"#define NFSCTL_ADDCLIENT        1       /* Add an NFS client. */\n"
"#define NFSCTL_DELCLIENT        2       /* Remove an NFS client. */\n"
"#define NFSCTL_EXPORT           3       /* export a file system. */\n"
"#define NFSCTL_UNEXPORT         4       /* unexport a file system. */\n"
"#define NFSCTL_UGIDUPDATE       5       /* update a client's uid/gid "
"map. */\n"
"#define NFSCTL_GETFH            6       /* get an fh (used by mountd) */\n"
msgstr ""

# type: Plain text
#: nfsservctl.2:36
#, no-wrap
msgid ""
"struct nfsctl_arg {\n"
"        int                     ca_version;     /* safeguard */\n"
"        union {\n"
"                struct nfsctl_svc       u_svc;\n"
"                struct nfsctl_client    u_client;\n"
"                struct nfsctl_export    u_export;\n"
"                struct nfsctl_uidmap    u_umap;\n"
"                struct nfsctl_fhparm    u_getfh;\n"
"                unsigned int            u_debug;\n"
"        } u;\n"
"}\n"
msgstr ""

# type: Plain text
#: nfsservctl.2:41
#, no-wrap
msgid ""
"union nfsctl_res {\n"
"        struct knfs_fh          cr_getfh;\n"
"        unsigned int            cr_debug;\n"
"};\n"
msgstr ""

# type: SH
#: nfsservctl.2:42
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: nfsservctl.2:46
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""

# type: SH
#: nfsservctl.2:46
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: nfsservctl.2:47
msgid "This call is Linux-specific."
msgstr ""
