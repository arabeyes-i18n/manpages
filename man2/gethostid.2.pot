# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:04+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: gethostid.2:29
#, no-wrap
msgid "GETHOSTID"
msgstr ""

# type: TH
#: gethostid.2:29
#, no-wrap
msgid "1993-11-29"
msgstr ""

# type: TH
#: gethostid.2:29
#, no-wrap
msgid "Linux 0.99.13"
msgstr ""

# type: TH
#: gethostid.2:29
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: gethostid.2:30
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: gethostid.2:32
msgid "gethostid, sethostid - get or set the unique identifier of the current host"
msgstr ""

# type: SH
#: gethostid.2:32
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: gethostid.2:34
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr ""

# type: Plain text
#: gethostid.2:36
msgid "B<long gethostid(void);>"
msgstr ""

# type: Plain text
#: gethostid.2:38
msgid "B<int sethostid(long >I<hostid>B<);>"
msgstr ""

# type: SH
#: gethostid.2:38
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: gethostid.2:45
msgid ""
"Get or set a unique 32-bit identifier for the current machine.  The 32-bit "
"identifier is intended to be unique among all UNIX systems in "
"existence. This normally resembles the Internet address for the local "
"machine, as returned by B<gethostbyname>(3), and thus usually never needs to "
"be set."
msgstr ""

# type: Plain text
#: gethostid.2:49
msgid "The B<sethostid> call is restricted to the superuser."
msgstr ""

# type: Plain text
#: gethostid.2:54
msgid "The I<hostid> argument is stored in the file I</etc/hostid>."
msgstr ""

# type: SH
#: gethostid.2:54
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: gethostid.2:58
msgid ""
"B<gethostid> returns the 32-bit identifier for the current host as set by "
"B<sethostid>(2)."
msgstr ""

# type: SH
#: gethostid.2:58
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: gethostid.2:65
msgid ""
"4.2BSD.  These functions were dropped in 4.4BSD.  POSIX.1 does not define "
"these functions, but ISO/IEC 9945-1:1990 mentions them in B.4.4.1.  SVr4 "
"includes B<gethostid> but not B<sethostid>."
msgstr ""

# type: SH
#: gethostid.2:65
#, no-wrap
msgid "FILES"
msgstr ""

# type: Plain text
#: gethostid.2:67
msgid "I</etc/hostid>"
msgstr ""

# type: SH
#: gethostid.2:67
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: gethostid.2:69
msgid "B<hostid>(1), B<gethostbyname>(3)"
msgstr ""
