# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:04+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: getsockname.2:38
#, no-wrap
msgid "GETSOCKNAME"
msgstr ""

# type: TH
#: getsockname.2:38
#, no-wrap
msgid "1993-07-24"
msgstr ""

# type: TH
#: getsockname.2:38
#, no-wrap
msgid "BSD Man Page"
msgstr ""

# type: TH
#: getsockname.2:38
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: getsockname.2:39
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: getsockname.2:41
msgid "getsockname - get socket name"
msgstr ""

# type: SH
#: getsockname.2:41
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: getsockname.2:44
#, no-wrap
msgid "B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr ""

# type: Plain text
#: getsockname.2:46
#, no-wrap
msgid ""
"B<int getsockname(int >I<s>B<, struct sockaddr *>I<name>B<, socklen_t "
"*>I<namelen>B<);>\n"
msgstr ""

# type: SH
#: getsockname.2:47
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: getsockname.2:58
msgid ""
"B<Getsockname> returns the current I<name> for the specified socket.  The "
"I<namelen> parameter should be initialized to indicate the amount of space "
"pointed to by I<name>.  On return it contains the actual size of the name "
"returned (in bytes)."
msgstr ""

# type: SH
#: getsockname.2:58
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: getsockname.2:62
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""

# type: SH
#: getsockname.2:62
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: getsockname.2:63
#, no-wrap
msgid "B<EBADF>"
msgstr ""

# type: Plain text
#: getsockname.2:68
msgid "The argument I<s> is not a valid descriptor."
msgstr ""

# type: TP
#: getsockname.2:68
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

# type: Plain text
#: getsockname.2:74
msgid ""
"The I<name> parameter points to memory not in a valid part of the process "
"address space."
msgstr ""

# type: TP
#: getsockname.2:74
#, no-wrap
msgid "B<ENOBUFS>"
msgstr ""

# type: Plain text
#: getsockname.2:78
msgid ""
"Insufficient resources were available in the system to perform the "
"operation."
msgstr ""

# type: TP
#: getsockname.2:78
#, no-wrap
msgid "B<ENOTSOCK>"
msgstr ""

# type: Plain text
#: getsockname.2:83
msgid "The argument I<s> is a file, not a socket."
msgstr ""

# type: SH
#: getsockname.2:83
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: getsockname.2:88
msgid ""
"SVr4, 4.4BSD (the B<getsockname> function call appeared in 4.2BSD).  SVr4 "
"documents additional ENOMEM and ENOSR error codes."
msgstr ""

# type: SH
#: getsockname.2:88
#, no-wrap
msgid "NOTE"
msgstr ""

# type: Plain text
#: getsockname.2:95
msgid ""
"The third argument of B<getsockname> is in reality an `int *' (and this is "
"what BSD 4.* and libc4 and libc5 have).  Some POSIX confusion resulted in "
"the present socklen_t, also used by glibc.  See also B<accept>(2)."
msgstr ""

# type: SH
#: getsockname.2:95
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: getsockname.2:97
msgid "B<bind>(2), B<socket>(2)"
msgstr ""
