# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: prctl.2:33
#, no-wrap
msgid "PRCTL"
msgstr ""

# type: TH
#: prctl.2:33
#, no-wrap
msgid "2002-06-27"
msgstr ""

# type: TH
#: prctl.2:33
#, no-wrap
msgid "Linux 2.4.18"
msgstr ""

# type: TH
#: prctl.2:33
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: prctl.2:34
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: prctl.2:36
msgid "prctl - operations on a process"
msgstr ""

# type: SH
#: prctl.2:36
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: prctl.2:38
msgid "B<#include E<lt>sys/prctl.hE<gt>>"
msgstr ""

# type: Plain text
#: prctl.2:41
msgid ""
"B<int prctl(int >I<option>B<, unsigned long >I<arg2>B<, unsigned long "
">I<arg3> B<, unsigned long >I<arg4>B<, unsigned long >I<arg5>B<);>"
msgstr ""

# type: SH
#: prctl.2:41
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: prctl.2:47
msgid ""
"B<prctl> is called with a first argument describing what to do (with values "
"defined in E<lt>I<linux/prctl.h>E<gt>), and further parameters with a "
"significance depending on the first one.  The first argument can be:"
msgstr ""

# type: TP
#: prctl.2:47
#, no-wrap
msgid "B<PR_SET_PDEATHSIG>"
msgstr ""

# type: Plain text
#: prctl.2:55
msgid ""
"(since Linux 2.1.57)  Set the parent process death signal of the current "
"process to I<arg2> (either a signal value in the range 1..maxsig, or 0 to "
"clear).  This is the signal that the current process will get when its "
"parent dies. This value is cleared upon a fork()."
msgstr ""

# type: TP
#: prctl.2:55
#, no-wrap
msgid "B<PR_GET_PDEATHSIG>"
msgstr ""

# type: Plain text
#: prctl.2:60
msgid ""
"(since Linux 2.3.15)  Read the current value of the parent process death "
"signal into the (int *) I<arg2>."
msgstr ""

# type: TP
#: prctl.2:60
#, no-wrap
msgid "B<PR_SET_DUMPABLE>"
msgstr ""

# type: Plain text
#: prctl.2:71
msgid ""
"(Since Linux 2.4)  Set the state of the flag determining whether core dumps "
"are produced for this process upon delivery of a signal whose default "
"behaviour is to produce a core dump.  (Normally this flag is set for a "
"process by default, but it is cleared when a set-UID or set-GID program is "
"executed and also by various system calls that manipulate process UIDs and "
"GIDs).  I<arg2> must be either 0 (process is not dumpable) or 1 (process is "
"dumpable)."
msgstr ""

# type: TP
#: prctl.2:71
#, no-wrap
msgid "B<PR_GET_DUMPABLE>"
msgstr ""

# type: Plain text
#: prctl.2:76
msgid ""
"(Since Linux 2.4)  Return (as the function result) the current state of the "
"calling process's dumpable flag."
msgstr ""

# type: TP
#: prctl.2:76
#, no-wrap
msgid "B<PR_SET_KEEPCAPS>"
msgstr ""

# type: Plain text
#: prctl.2:86
msgid ""
"Set the state of the process's \"keep capabilities\" flag, which determines "
"whether the process's effective and permitted capability sets are cleared "
"when a change is made to the process's user IDs such that all of the "
"process's real, effective, and saved set user IDs become non-zero when at "
"least one of them previously had the value 0.  (By default, these credential "
"sets are cleared).  I<arg2> must be either 0 (capabilities are cleared) or 1 "
"(capabilities are kept)."
msgstr ""

# type: TP
#: prctl.2:86
#, no-wrap
msgid "B<PR_GET_KEEPCAPS>"
msgstr ""

# type: Plain text
#: prctl.2:90
msgid ""
"Return (as the function result) the current state of the calling process's "
"\"keep capabilities\" flag."
msgstr ""

# type: SH
#: prctl.2:90
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: prctl.2:101
msgid ""
"B<PR_GET_DUMPABLE> and B<PR_GET_KEEPCAPS> return 0 or 1 on success.  All "
"other I<option> values return 0 on success.  On error, -1 is returned, and "
"I<errno> is set appropriately."
msgstr ""

# type: SH
#: prctl.2:101
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: prctl.2:102
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: prctl.2:111
msgid ""
"The value of I<option> is not recognized, or it is B<PR_SET_PDEATHSIG> and "
"I<arg2> is not zero or a signal number."
msgstr ""

# type: SH
#: prctl.2:111
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: prctl.2:116
msgid ""
"This call is Linux-specific.  IRIX has a prctl system call (also introduced "
"in Linux 2.1.44 as irix_prctl on the MIPS architecture), with prototype"
msgstr ""

# type: Plain text
#: prctl.2:118
msgid "B<ptrdiff_t prctl(int >I<option>B<, int >I<arg2>B<, int >I<arg3>B<);>"
msgstr ""

# type: Plain text
#: prctl.2:123
msgid ""
"and options to get the maximum number of processes per user, get the maximum "
"number of processors the calling process can use, find out whether a "
"specified process is currently blocked, get or set the maximum stack size, "
"etc., etc."
msgstr ""

# type: SH
#: prctl.2:123
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

# type: Plain text
#: prctl.2:126
msgid ""
"The prctl() systemcall was introduced in Linux 2.1.57.  There is no prctl() "
"library call as yet."
msgstr ""

# type: SH
#: prctl.2:126
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: prctl.2:127
msgid "B<signal>(2)"
msgstr ""
