# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:04+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: cacheflush.2:24
#, no-wrap
msgid "CACHEFLUSH"
msgstr ""

# type: TH
#: cacheflush.2:24
#, no-wrap
msgid "1995-06-27"
msgstr ""

# type: TH
#: cacheflush.2:24
#, no-wrap
msgid "Linux 2.0.32"
msgstr ""

# type: TH
#: cacheflush.2:24
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: cacheflush.2:25
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: cacheflush.2:27
msgid "cacheflush - flush contents of instruction and/or data cache"
msgstr ""

# type: SH
#: cacheflush.2:27
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: cacheflush.2:30
#, no-wrap
msgid "B<#include E<lt>asm/cachectl.hE<gt>>\n"
msgstr ""

# type: Plain text
#: cacheflush.2:32
#, no-wrap
msgid "B<int cacheflush(char *>I<addr>B<, int >I<nbytes>B<, int >I<cache>B<);>\n"
msgstr ""

# type: SH
#: cacheflush.2:33
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: cacheflush.2:37
msgid ""
"B<cacheflush> flushes contents of indicated cache(s) for user addresses in "
"the range addr to (addr+nbytes-1). Cache may be one of:"
msgstr ""

# type: TP
#: cacheflush.2:37
#, no-wrap
msgid "B<ICACHE>"
msgstr ""

# type: Plain text
#: cacheflush.2:40
msgid "Flush the instruction cache."
msgstr ""

# type: TP
#: cacheflush.2:40
#, no-wrap
msgid "B<DCACHE>"
msgstr ""

# type: Plain text
#: cacheflush.2:43
msgid "Write back to memory and invalidate the affected valid cache lines."
msgstr ""

# type: TP
#: cacheflush.2:43
#, no-wrap
msgid "B<BCACHE>"
msgstr ""

# type: Plain text
#: cacheflush.2:47
msgid "Same as B<(ICACHE|DCACHE).>"
msgstr ""

# type: SH
#: cacheflush.2:48
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: cacheflush.2:52
msgid ""
"B<cacheflush> returns 0 on success or -1 on error. If errors are detected, "
"errno will indicate the error."
msgstr ""

# type: SH
#: cacheflush.2:52
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: cacheflush.2:53
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

# type: Plain text
#: cacheflush.2:56
msgid "Some or all of the address range addr to (addr+nbytes-1) is not accessible."
msgstr ""

# type: TP
#: cacheflush.2:56
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: cacheflush.2:59
msgid "cache parameter is not one of ICACHE, DCACHE, or BCACHE."
msgstr ""

# type: SH
#: cacheflush.2:60
#, no-wrap
msgid "BUGS"
msgstr ""

# type: Plain text
#: cacheflush.2:63
msgid ""
"The current implementation ignores the addr and nbytes parameters.  "
"Therefore always the whole cache is flushed."
msgstr ""

# type: SH
#: cacheflush.2:63
#, no-wrap
msgid "NOTE"
msgstr ""

# type: Plain text
#: cacheflush.2:65
msgid ""
"This system call is only available on MIPS based systems.  It should not be "
"used in programs intended to be portable."
msgstr ""
