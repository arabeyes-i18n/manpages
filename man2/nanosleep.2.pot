# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: nanosleep.2:28
#, no-wrap
msgid "NANOSLEEP"
msgstr ""

# type: TH
#: nanosleep.2:28
#, no-wrap
msgid "2004-10-24"
msgstr ""

# type: TH
#: nanosleep.2:28
#, no-wrap
msgid "Linux 2.6.9"
msgstr ""

# type: TH
#: nanosleep.2:28
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: nanosleep.2:29
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: nanosleep.2:31
msgid "nanosleep - pause execution for a specified time"
msgstr ""

# type: SH
#: nanosleep.2:31
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: nanosleep.2:33
msgid "B<#include E<lt>time.hE<gt>>"
msgstr ""

# type: Plain text
#: nanosleep.2:35
msgid ""
"B<int nanosleep(const struct timespec *>I<req>B<, struct timespec "
"*>I<rem>B<);>"
msgstr ""

# type: SH
#: nanosleep.2:36
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: nanosleep.2:55
msgid ""
"B<nanosleep> delays the execution of the program for at least the time "
"specified in I<*req>.  The function can return earlier if a signal has been "
"delivered to the process. In this case, it returns -1, sets I<errno> to "
"B<EINTR>, and writes the remaining time into the structure pointed to by "
"I<rem> unless I<rem> is B<NULL>.  The value of I<*rem> can then be used to "
"call B<nanosleep> again and complete the specified pause."
msgstr ""

# type: Plain text
#: nanosleep.2:62
msgid ""
"The structure I<timespec> is used to specify intervals of time with "
"nanosecond precision. It is specified in I<E<lt>time.hE<gt>> and has the "
"form"
msgstr ""

# type: Plain text
#: nanosleep.2:72
#, no-wrap
msgid ""
"struct timespec\n"
"{\n"
"\ttime_t\ttv_sec;\t\t\t/* seconds */\n"
"\tlong\ttv_nsec;\t\t/* nanoseconds */\n"
"};\n"
msgstr ""

# type: Plain text
#: nanosleep.2:77
msgid "The value of the nanoseconds field must be in the range 0 to 999999999."
msgstr ""

# type: Plain text
#: nanosleep.2:86
msgid ""
"Compared to B<sleep>(3)  and B<usleep>(3), B<nanosleep> has the advantage of "
"not affecting any signals, it is standardized by POSIX, it provides higher "
"timing resolution, and it allows to continue a sleep that has been "
"interrupted by a signal more easily."
msgstr ""

# type: SH
#: nanosleep.2:86
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: Plain text
#: nanosleep.2:92
msgid ""
"In case of an error or exception, the B<nanosleep> system call returns -1 "
"instead of 0 and sets I<errno> to one of the following values:"
msgstr ""

# type: TP
#: nanosleep.2:92
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

# type: Plain text
#: nanosleep.2:95
msgid "Problem with copying information from user space."
msgstr ""

# type: TP
#: nanosleep.2:95
#, no-wrap
msgid "B<EINTR>"
msgstr ""

# type: Plain text
#: nanosleep.2:102
msgid ""
"The pause has been interrupted by a non-blocked signal that was delivered to "
"the process. The remaining sleep time has been written into *I<rem> so that "
"the process can easily call B<nanosleep> again and continue with the pause."
msgstr ""

# type: TP
#: nanosleep.2:102
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: nanosleep.2:109
msgid ""
"The value in the I<tv_nsec> field was not in the range 0 to 999999999 or "
"I<tv_sec> was negative."
msgstr ""

# type: SH
#: nanosleep.2:109
#, no-wrap
msgid "BUGS"
msgstr ""

# type: Plain text
#: nanosleep.2:121
msgid ""
"The current implementation of B<nanosleep> is based on the normal kernel "
"timer mechanism, which has a resolution of 1/I<HZ>\\ s (i.e, 10\\ ms on "
"Linux/i386 and 1\\ ms on Linux/Alpha).  Therefore, B<nanosleep> pauses "
"always for at least the specified time, however it can take up to 10 ms "
"longer than specified until the process becomes runnable again. For the same "
"reason, the value returned in case of a delivered signal in *I<rem> is "
"usually rounded to the next larger multiple of 1/I<HZ>\\ s."
msgstr ""

# type: SS
#: nanosleep.2:122
#, no-wrap
msgid "Old behaviour"
msgstr ""

# type: Plain text
#: nanosleep.2:134
msgid ""
"In order to support applications requiring much more precise pauses (e.g., "
"in order to control some time-critical hardware), B<nanosleep> would handle "
"pauses of up to 2\\ ms by busy waiting with microsecond precision when "
"called from a process scheduled under a real-time policy like I<SCHED_FIFO> "
"or I<SCHED_RR>.  This special extension was removed in kernel 2.5.39, hence "
"is still present in current 2.4 kernels, but not in 2.6 kernels."
msgstr ""

# type: SH
#: nanosleep.2:134
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: nanosleep.2:136
msgid "POSIX.1b (formerly POSIX.4)."
msgstr ""

# type: SH
#: nanosleep.2:136
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: nanosleep.2:140
msgid "B<sched_setscheduler>(2), B<timer_create>(2), B<sleep>(3), B<usleep>(3)"
msgstr ""
