# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:04+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: getsid.2:25
#, no-wrap
msgid "GETSID"
msgstr ""

# type: TH
#: getsid.2:25
#, no-wrap
msgid "2001-12-17"
msgstr ""

# type: TH
#: getsid.2:25
#, no-wrap
msgid "Linux 2.5.0"
msgstr ""

# type: TH
#: getsid.2:25
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: getsid.2:26
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: getsid.2:28
msgid "getsid - get session ID"
msgstr ""

# type: SH
#: getsid.2:28
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: getsid.2:30
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr ""

# type: Plain text
#: getsid.2:32
msgid "B<pid_t getsid(pid_t>I< pid>B<);>"
msgstr ""

# type: SH
#: getsid.2:32
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: getsid.2:43
msgid ""
"B<getsid(0)> returns the session ID of the calling process.  "
"B<getsid(>I<p>B<)> returns the session ID of the process with process ID "
"I<p>.  (The session ID of a process is the process group ID of the session "
"leader.)  On error, (pid_t) -1 will be returned, and I<errno> is set "
"appropriately."
msgstr ""

# type: SH
#: getsid.2:43
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: getsid.2:44
#, no-wrap
msgid "B<EPERM>"
msgstr ""

# type: Plain text
#: getsid.2:50
msgid ""
"A process with process ID I<p> exists, but it is not in the same session as "
"the current process, and the implementation considers this an error."
msgstr ""

# type: TP
#: getsid.2:50
#, no-wrap
msgid "B<ESRCH>"
msgstr ""

# type: Plain text
#: getsid.2:55
msgid "No process with process ID I<p> was found."
msgstr ""

# type: SH
#: getsid.2:55
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: getsid.2:57
msgid "SVr4, POSIX 1003.1-2001."
msgstr ""

# type: SH
#: getsid.2:57
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: getsid.2:59
msgid "Linux does not return EPERM."
msgstr ""

# type: Plain text
#: getsid.2:62
msgid ""
"Linux has this system call since Linux 1.3.44.  There is libc support since "
"libc 5.2.19."
msgstr ""

# type: Plain text
#: getsid.2:66
msgid ""
"To get the prototype under glibc, define both _XOPEN_SOURCE and "
"_XOPEN_SOURCE_EXTENDED, or use \"#define _XOPEN_SOURCE I<n>\" for some "
"integer I<n> larger than or equal to 500."
msgstr ""

# type: SH
#: getsid.2:66
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: getsid.2:68
msgid "B<getpgid>(2), B<setsid>(2)"
msgstr ""
