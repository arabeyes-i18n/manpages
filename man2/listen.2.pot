# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:04+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: listen.2:41
#, no-wrap
msgid "LISTEN"
msgstr ""

# type: TH
#: listen.2:41
#, no-wrap
msgid "1993-07-23"
msgstr ""

# type: TH
#: listen.2:41
#, no-wrap
msgid "BSD Man Page"
msgstr ""

# type: TH
#: listen.2:41
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: listen.2:42
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: listen.2:44
msgid "listen - listen for connections on a socket"
msgstr ""

# type: SH
#: listen.2:44
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: listen.2:46
msgid "B<#include E<lt>sys/socket.hE<gt>>"
msgstr ""

# type: Plain text
#: listen.2:48
msgid "B<int listen(int >I<s>B<, int >I<backlog>B<);>"
msgstr ""

# type: SH
#: listen.2:48
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: listen.2:63
msgid ""
"To accept connections, a socket is first created with B<socket>(2), a "
"willingness to accept incoming connections and a queue limit for incoming "
"connections are specified with B<listen>, and then the connections are "
"accepted with B<accept>(2).  The B<listen> call applies only to sockets of "
"type B<SOCK_STREAM> or B<SOCK_SEQPACKET>."
msgstr ""

# type: Plain text
#: listen.2:72
msgid ""
"The I<backlog> parameter defines the maximum length the queue of pending "
"connections may grow to.  If a connection request arrives with the queue "
"full the client may receive an error with an indication of B<ECONNREFUSED> "
"or, if the underlying protocol supports retransmission, the request may be "
"ignored so that retries succeed."
msgstr ""

# type: SH
#: listen.2:72
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: listen.2:88
msgid ""
"The behaviour of the I<backlog> parameter on TCP sockets changed with Linux "
"2.2.  Now it specifies the queue length for I<completely> established "
"sockets waiting to be accepted, instead of the number of incomplete "
"connection requests. The maximum length of the queue for incomplete sockets "
"can be set using the B<tcp_max_syn_backlog> sysctl.  When syncookies are "
"enabled there is no logical maximum length and this sysctl setting is "
"ignored.  See B<tcp>(7)  for more information."
msgstr ""

# type: SH
#: listen.2:89
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: listen.2:93
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""

# type: SH
#: listen.2:93
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: listen.2:94
#, no-wrap
msgid "B<EADDRINUSE>"
msgstr ""

# type: Plain text
#: listen.2:97
msgid "Another socket is already listening on the same port."
msgstr ""

# type: TP
#: listen.2:97
#, no-wrap
msgid "B<EBADF>"
msgstr ""

# type: Plain text
#: listen.2:102
msgid "The argument I<s> is not a valid descriptor."
msgstr ""

# type: TP
#: listen.2:102
#, no-wrap
msgid "B<ENOTSOCK>"
msgstr ""

# type: Plain text
#: listen.2:107
msgid "The argument I<s> is not a socket."
msgstr ""

# type: TP
#: listen.2:107
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr ""

# type: Plain text
#: listen.2:112
msgid "The socket is not of a type that supports the B<listen> operation."
msgstr ""

# type: SH
#: listen.2:112
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: listen.2:116
msgid ""
"Single Unix, 4.4BSD, POSIX 1003.1g draft. The B<listen> function call first "
"appeared in 4.2BSD."
msgstr ""

# type: SH
#: listen.2:116
#, no-wrap
msgid "BUGS"
msgstr ""

# type: Plain text
#: listen.2:129
msgid ""
"If the socket is of type B<AF_INET>, and the I<backlog> argument is greater "
"than the constant B<SOMAXCONN> (128 in Linux 2.0 & 2.2), it is silently "
"truncated to B<SOMAXCONN>.  Don't rely on this value in portable "
"applications since BSD (and some BSD-derived systems) limit the backlog to "
"5."
msgstr ""

# type: SH
#: listen.2:129
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: listen.2:132
msgid "B<accept>(2), B<connect>(2), B<socket>(2)"
msgstr ""
