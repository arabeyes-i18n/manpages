# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: setpgid.2:43
#, no-wrap
msgid "SETPGID"
msgstr ""

# type: TH
#: setpgid.2:43
#, no-wrap
msgid "2003-01-20"
msgstr ""

# type: TH
#: setpgid.2:43
#, no-wrap
msgid "Linux"
msgstr ""

# type: TH
#: setpgid.2:43
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: setpgid.2:44
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: setpgid.2:46
msgid "setpgid, getpgid, setpgrp, getpgrp - set/get process group"
msgstr ""

# type: SH
#: setpgid.2:46
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: setpgid.2:48
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr ""

# type: Plain text
#: setpgid.2:50
msgid "B<int setpgid(pid_t >I<pid>B<, pid_t >I<pgid>B<);>"
msgstr ""

# type: Plain text
#: setpgid.2:52
msgid "B<pid_t getpgid(pid_t >I<pid>B<);>"
msgstr ""

# type: Plain text
#: setpgid.2:54
msgid "B<int setpgrp(void);>"
msgstr ""

# type: Plain text
#: setpgid.2:56
msgid "B<pid_t getpgrp(void);>"
msgstr ""

# type: SH
#: setpgid.2:56
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: setpgid.2:73
msgid ""
"B<setpgid> sets the process group ID of the process specified by I<pid> to "
"I<pgid>.  If I<pid> is zero, the process ID of the current process is used.  "
"If I<pgid> is zero, the process ID of the process specified by I<pid> is "
"used.  If B<setpgid> is used to move a process from one process group to "
"another (as is done by some shells when creating pipelines), both process "
"groups must be part of the same session.  In this case, the I<pgid> "
"specifies an existing process group to be joined and the session ID of that "
"group must match the session ID of the joining process."
msgstr ""

# type: Plain text
#: setpgid.2:80
msgid ""
"B<getpgid> returns the process group ID of the process specified by I<pid>.  "
"If I<pid> is zero, the process ID of the current process is used."
msgstr ""

# type: Plain text
#: setpgid.2:85
msgid "The call B<setpgrp()> is equivalent to B<setpgid(0,0)>."
msgstr ""

# type: Plain text
#: setpgid.2:92
msgid ""
"Similarly, B<getpgrp()> is equivalent to B<getpgid(0)>.  Each process group "
"is a member of a session and each process is a member of the session of "
"which its process group is a member."
msgstr ""

# type: Plain text
#: setpgid.2:106
msgid ""
"Process groups are used for distribution of signals, and by terminals to "
"arbitrate requests for their input: Processes that have the same process "
"group as the terminal are foreground and may read, while others will block "
"with a signal if they attempt to read.  These calls are thus used by "
"programs such as B<csh>(1)  to create process groups in implementing job "
"control.  The B<TIOCGPGRP> and B<TIOCSPGRP> calls described in B<termios>(3)  "
"are used to get/set the process group of the control terminal."
msgstr ""

# type: Plain text
#: setpgid.2:111
msgid ""
"If a session has a controlling terminal, CLOCAL is not set and a hangup "
"occurs, then the session leader is sent a SIGHUP.  If the session leader "
"exits, the SIGHUP signal will be sent to each process in the foreground "
"process group of the controlling terminal."
msgstr ""

# type: Plain text
#: setpgid.2:116
msgid ""
"If the exit of the process causes a process group to become orphaned, and if "
"any member of the newly-orphaned process group is stopped, then a SIGHUP "
"signal followed by a SIGCONT signal will be sent to each process in the "
"newly-orphaned process group."
msgstr ""

# type: SH
#: setpgid.2:117
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: setpgid.2:123
msgid ""
"On success, B<setpgid> and B<setpgrp> return zero.  On error, -1 is "
"returned, and I<errno> is set appropriately."
msgstr ""

# type: Plain text
#: setpgid.2:129
msgid ""
"B<getpgid> returns a process group on success.  On error, -1 is returned, "
"and I<errno> is set appropriately."
msgstr ""

# type: Plain text
#: setpgid.2:132
msgid "B<getpgrp> always returns the current process group."
msgstr ""

# type: SH
#: setpgid.2:132
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: setpgid.2:133
#, no-wrap
msgid "B<EACCES>"
msgstr ""

# type: Plain text
#: setpgid.2:139
msgid ""
"An attempt was made to change the process group ID of one of the children of "
"the calling process and the child had already performed an B<execve> "
"(B<setpgid>, B<setpgrp>)."
msgstr ""

# type: TP
#: setpgid.2:139
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: setpgid.2:144
msgid "I<pgid> is less than 0 (B<setpgid>, B<setpgrp>)."
msgstr ""

# type: TP
#: setpgid.2:144
#, no-wrap
msgid "B<EPERM>"
msgstr ""

# type: Plain text
#: setpgid.2:152
msgid ""
"An attempt was made to move a process into a process group in a different "
"session, or to change the process group ID of one of the children of the "
"calling process and the child was in a different session, or to change the "
"process group ID of a session leader (B<setpgid>, B<setpgrp>)."
msgstr ""

# type: TP
#: setpgid.2:152
#, no-wrap
msgid "B<ESRCH>"
msgstr ""

# type: Plain text
#: setpgid.2:162
msgid ""
"For B<getpgid>: I<pid> does not match any process.  For B<setpgid>: I<pid> "
"is not the current process and not a child of the current process."
msgstr ""

# type: SH
#: setpgid.2:162
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: setpgid.2:174
msgid ""
"The functions B<setpgid> and B<getpgrp> conform to POSIX.1.  The function "
"B<setpgrp> is from BSD 4.2.  The function B<getpgid> conforms to SVr4."
msgstr ""

# type: SH
#: setpgid.2:174
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: setpgid.2:181
msgid ""
"POSIX took B<setpgid> from the BSD function B<setpgrp>.  Also SysV has a "
"function with the same name, but it is identical to B<setsid>(2)."
msgstr ""

# type: Plain text
#: setpgid.2:185
msgid ""
"To get the prototypes under glibc, define both _XOPEN_SOURCE and "
"_XOPEN_SOURCE_EXTENDED, or use \"#define _XOPEN_SOURCE I<n>\" for some "
"integer I<n> larger than or equal to 500."
msgstr ""

# type: SH
#: setpgid.2:185
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: setpgid.2:190
msgid "B<getuid>(2), B<setsid>(2), B<tcgetpgrp>(3), B<tcsetpgrp>(3), B<termios>(3)"
msgstr ""
