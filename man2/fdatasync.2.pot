# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:04+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: fdatasync.2:31
#, no-wrap
msgid "FDATASYNC"
msgstr ""

# type: TH
#: fdatasync.2:31
#, no-wrap
msgid "1996-04-13"
msgstr ""

# type: TH
#: fdatasync.2:31
#, no-wrap
msgid "Linux 1.3.86"
msgstr ""

# type: TH
#: fdatasync.2:31
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: fdatasync.2:32
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: fdatasync.2:34
msgid "fdatasync - synchronize a file's in-core data with that on disk"
msgstr ""

# type: SH
#: fdatasync.2:34
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: fdatasync.2:36
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr ""

# type: Plain text
#: fdatasync.2:38
msgid "B<int fdatasync(int >I<fd>B<);>"
msgstr ""

# type: SH
#: fdatasync.2:38
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: fdatasync.2:44
msgid ""
"B<fdatasync> flushes all data buffers of a file to disk (before the system "
"call returns).  It resembles B<fsync> but is not required to update the "
"metadata such as access time."
msgstr ""

# type: Plain text
#: fdatasync.2:57
msgid ""
"Applications that access databases or log files often write a tiny data "
"fragment (e.g., one line in a log file) and then call B<fsync> immediately "
"in order to ensure that the written data is physically stored on the "
"harddisk. Unfortunately, B<fsync> will always initiate two write operations: "
"one for the newly written data and another one in order to update the "
"modification time stored in the inode. If the modification time is not a "
"part of the transaction concept B<fdatasync> can be used to avoid "
"unnecessary inode disk write operations."
msgstr ""

# type: SH
#: fdatasync.2:57
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: fdatasync.2:61
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""

# type: SH
#: fdatasync.2:61
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: fdatasync.2:62
#, no-wrap
msgid "B<EBADF>"
msgstr ""

# type: Plain text
#: fdatasync.2:66
msgid "I<fd> is not a valid file descriptor open for writing."
msgstr ""

# type: TP
#: fdatasync.2:66
#, no-wrap
msgid "B<EIO>"
msgstr ""

# type: Plain text
#: fdatasync.2:69
msgid "An error occurred during synchronization."
msgstr ""

# type: TP
#: fdatasync.2:69
#, no-wrap
msgid "B<EROFS>, B<EINVAL>"
msgstr ""

# type: Plain text
#: fdatasync.2:73
msgid "I<fd> is bound to a special file which does not support synchronization."
msgstr ""

# type: SH
#: fdatasync.2:73
#, no-wrap
msgid "BUGS"
msgstr ""

# type: Plain text
#: fdatasync.2:78
msgid "Currently (Linux 2.2)  B<fdatasync> is equivalent to B<fsync>."
msgstr ""

# type: SH
#: fdatasync.2:78
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

# type: Plain text
#: fdatasync.2:85
msgid ""
"On POSIX systems on which B<fdatasync> is available, "
"B<_POSIX_SYNCHRONIZED_IO> is defined in E<lt>unistd.hE<gt> to a value "
"greater than 0. (See also B<sysconf>(3).)"
msgstr ""

# type: SH
#: fdatasync.2:88
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: fdatasync.2:90
msgid "POSIX1b (formerly POSIX.4)"
msgstr ""

# type: SH
#: fdatasync.2:90
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: fdatasync.2:92
msgid "B<fsync>(2)"
msgstr ""

# type: Plain text
#: fdatasync.2:93
msgid "B.O. Gallmeister, POSIX.4, O'Reilly, pp. 220-223 and 343."
msgstr ""
