# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: mlock.2:27
#, no-wrap
msgid "MLOCK"
msgstr ""

# type: TH
#: mlock.2:27
#, no-wrap
msgid "2004-11-25"
msgstr ""

# type: TH
#: mlock.2:27
#, no-wrap
msgid "Linux 2.6.9"
msgstr ""

# type: TH
#: mlock.2:27
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: mlock.2:28
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: mlock.2:30
msgid "mlock, munlock, mlockall, munlockall - lock and unlock memory"
msgstr ""

# type: SH
#: mlock.2:30
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: mlock.2:33
#, no-wrap
msgid "B<#include E<lt>sys/mman.hE<gt>>\n"
msgstr ""

# type: Plain text
#: mlock.2:35
#, no-wrap
msgid "B<int mlock(const void *>I<addr>B<, size_t >I<len>B<);>\n"
msgstr ""

# type: Plain text
#: mlock.2:37
#, no-wrap
msgid "B<int munlock(const void *>I<addr>B<, size_t >I<len>B<);>\n"
msgstr ""

# type: Plain text
#: mlock.2:39
#, no-wrap
msgid "B<int mlockall(int >I<flags>B<);>\n"
msgstr ""

# type: Plain text
#: mlock.2:41
#, no-wrap
msgid "B<int munlockall(void);>\n"
msgstr ""

# type: SH
#: mlock.2:42
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: mlock.2:57
msgid ""
"B<mlock>()  and B<mlockall>()  respectively lock part or all of the calling "
"process's virtual address space into RAM, preventing that memory from being "
"paged to the swap area.  B<munlock>()  and B<munlockall>()  perform the "
"converse operation, respectively unlocking part or all of the calling "
"process's virtual address space, so that pages in the specified virtual "
"address range may once more to be swapped out if required by the kernel "
"memory manager.  Memory locking and unlocking are performed in units of "
"whole pages."
msgstr ""

# type: SS
#: mlock.2:57
#, no-wrap
msgid "mlock() and munlock()"
msgstr ""

# type: Plain text
#: mlock.2:67
msgid ""
"B<mlock>()  locks pages in the address range starting at I<addr> and "
"continuing for I<len> bytes.  All pages that contain a part of the specified "
"address range are guaranteed to be resident in RAM when the call returns "
"successfully; the pages are guaranteed to stay in RAM until later unlocked."
msgstr ""

# type: Plain text
#: mlock.2:76
msgid ""
"B<munlock>()  unlocks pages in the address range starting at I<addr> and "
"continuing for I<len> bytes.  After this call, all pages that contain a part "
"of the specified memory range can be moved to external swap space again by "
"the kernel."
msgstr ""

# type: SS
#: mlock.2:76
#, no-wrap
msgid "mlockall() and munlockall()"
msgstr ""

# type: Plain text
#: mlock.2:84
msgid ""
"B<mlockall>()  locks all pages mapped into the address space of the calling "
"process. This includes the pages of the code, data and stack segment, as "
"well as shared libraries, user space kernel data, shared memory, and "
"memory-mapped files. All mapped pages are guaranteed to be resident in RAM "
"when the call returns successfully; the pages are guaranteed to stay in RAM "
"until later unlocked."
msgstr ""

# type: Plain text
#: mlock.2:89
msgid ""
"The I<flags> argument is constructed as the bitwise OR of one or more of the "
"following constants:"
msgstr ""

# type: TP
#: mlock.2:89
#, no-wrap
msgid "B<MCL_CURRENT>"
msgstr ""

# type: Plain text
#: mlock.2:93
msgid ""
"Lock all pages which are currently mapped into the address space of the "
"process."
msgstr ""

# type: TP
#: mlock.2:93
#, no-wrap
msgid "B<MCL_FUTURE>"
msgstr ""

# type: Plain text
#: mlock.2:99
msgid ""
"Lock all pages which will become mapped into the address space of the "
"process in the future. These could be for instance new pages required by a "
"growing heap and stack as well as new memory mapped files or shared memory "
"regions."
msgstr ""

# type: Plain text
#: mlock.2:112
msgid ""
"If B<MCL_FUTURE> has been specified, then a later system call (e.g., "
"B<mmap>(2), B<sbrk>(2), B<malloc>(3)), may fail if it would cause the number "
"of locked bytes to exceed the permitted maximum (see below).  In the same "
"circumstances, stack growth may likewise fail: the kernel will deny stack "
"expansion and deliver a B<SIGSEGV> signal to the process."
msgstr ""

# type: Plain text
#: mlock.2:116
msgid ""
"B<munlockall>()  unlocks all pages mapped into the address space of the "
"calling process."
msgstr ""

# type: SH
#: mlock.2:116
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: mlock.2:131
msgid ""
"Memory locking has two main applications: real-time algorithms and "
"high-security data processing. Real-time applications require deterministic "
"timing, and, like scheduling, paging is one major cause of unexpected "
"program execution delays. Real-time applications will usually also switch to "
"a real-time scheduler with B<sched_setscheduler>(2).  Cryptographic security "
"software often handles critical bytes like passwords or secret keys as data "
"structures. As a result of paging, these secrets could be transferred onto a "
"persistent swap store medium, where they might be accessible to the enemy "
"long after the security software has erased the secrets in RAM and "
"terminated.  (But be aware that the suspend mode on laptops and some desktop "
"computers will save a copy of the system's RAM to disk, regardless of memory "
"locks.)"
msgstr ""

# type: Plain text
#: mlock.2:143
msgid ""
"Real-time processes that are using B<mlockall>()  to prevent delays on page "
"faults should reserve enough locked stack pages before entering the "
"time-critical section, so that no page fault can be caused by function "
"calls.  This can be achieved by calling a function that allocates a "
"sufficiently large automatic variable (an array) and writes to the memory "
"occupied by this array in order to touch these stack pages.  This way, "
"enough pages will be mapped for the stack and can be locked into RAM. The "
"dummy writes ensure that not even copy-on-write page faults can occur in the "
"critical section."
msgstr ""

# type: Plain text
#: mlock.2:149
msgid ""
"Memory locks are not inherited by a child created via B<fork>(2)  and are "
"automatically removed (unlocked) during an B<execve>(2)  or when the process "
"terminates."
msgstr ""

# type: Plain text
#: mlock.2:153
msgid ""
"The memory lock on an address range is automatically removed if the address "
"range is unmapped via B<munmap>(2)."
msgstr ""

# type: Plain text
#: mlock.2:166
msgid ""
"Memory locks do not stack, i.e., pages which have been locked several times "
"by calls to B<mlock>()  or B<mlockall>()  will be unlocked by a single call "
"to B<munlock>()  for the corresponding range or by B<munlockall>.  Pages "
"which are mapped to several locations or by several processes stay locked "
"into RAM as long as they are locked at least at one location or by at least "
"one process."
msgstr ""

# type: SH
#: mlock.2:166
#, no-wrap
msgid "LINUX NOTES"
msgstr ""

# type: Plain text
#: mlock.2:177
msgid ""
"Under Linux, B<mlock>()  and B<munlock>()  automatically round I<addr> down "
"to the nearest page boundary.  However, POSIX 1003.1-2001 allows an "
"implementation to require that I<addr> is page aligned, so portable "
"applications should ensure this."
msgstr ""

# type: SS
#: mlock.2:177
#, no-wrap
msgid "Limits and permissions"
msgstr ""

# type: Plain text
#: mlock.2:184
msgid ""
"In Linux 2.6.8 and earlier, a process must be privileged (B<CAP_IPC_LOCK>)  "
"in order to lock memory and the B<RLIMIT_MEMLOCK> soft resource limit "
"defines a limit on how much memory the process may lock."
msgstr ""

# type: Plain text
#: mlock.2:190
msgid ""
"Since Linux 2.6.9, no limits are placed on the amount of memory that a "
"privileged process can lock and the B<RLIMIT_MEMLOCK> soft resource limit "
"instead defines a limit on how much memory an unprivileged process may lock."
msgstr ""

# type: SH
#: mlock.2:190
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: mlock.2:196
msgid ""
"On success these system calls return 0.  On error, -1 is returned, I<errno> "
"is set appropriately, and no changes are made to any locks in the address "
"space of the process."
msgstr ""

# type: SH
#: mlock.2:196
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: mlock.2:197 mlock.2:205 mlock.2:247
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

# type: Plain text
#: mlock.2:205
msgid ""
"(Linux 2.6.9 and later) the caller had a non-zero B<RLIMIT_MEMLOCK> soft "
"resource limit, but tried to lock more memory than the limit permitted.  "
"This limit is not enforced if the process is privileged (B<CAP_IPC_LOCK>)."
msgstr ""

# type: Plain text
#: mlock.2:209
msgid ""
"(Linux 2.4 and earlier) the calling process tried to lock more than half of "
"RAM."
msgstr ""

# type: TP
#: mlock.2:217 mlock.2:224 mlock.2:260
#, no-wrap
msgid "B<EPERM>"
msgstr ""

# type: Plain text
#: mlock.2:224
msgid ""
"(Linux 2.6.9 and later) the caller was not privileged (B<CAP_IPC_LOCK>)  and "
"its B<RLIMIT_MEMLOCK> soft resource limit was 0."
msgstr ""

# type: Plain text
#: mlock.2:232
msgid ""
"(Linux 2.6.8 and earlier)  The calling process has insufficient privilege to "
"call B<munlockall>.  Under Linux the B<CAP_IPC_LOCK> capability is required."
msgstr ""

# type: Plain text
#: mlock.2:238
msgid "For B<mlock>()  and B<munlock>():"
msgstr ""

# type: TP
#: mlock.2:238 mlock.2:242 mlock.2:254
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: mlock.2:242
msgid "I<len> was negative."
msgstr ""

# type: Plain text
#: mlock.2:247
msgid "(Not on Linux)  I<addr> was not a multiple of the page size."
msgstr ""

# type: Plain text
#: mlock.2:251
msgid ""
"Some of the specified address range does not correspond to mapped pages in "
"the address space of the process."
msgstr ""

# type: Plain text
#: mlock.2:254
msgid "For B<mlockall>():"
msgstr ""

# type: Plain text
#: mlock.2:257
msgid "Unknown I<flags> were specified."
msgstr ""

# type: Plain text
#: mlock.2:260
msgid "For B<munlockall>():"
msgstr ""

# type: Plain text
#: mlock.2:264
msgid "(Linux 2.6.8 and earlier) The caller was not privileged (B<CAP_IPC_LOCK>)."
msgstr ""

# type: SH
#: mlock.2:264
#, no-wrap
msgid "BUGS"
msgstr ""

# type: Plain text
#: mlock.2:272
msgid ""
"In the 2.4 series Linux kernels up to and including 2.4.17, a bug caused the "
"B<mlockall>()  B<MCL_FUTURE> flag to be inherited across a B<fork>(2).  This "
"was rectified in kernel 2.4.18."
msgstr ""

# type: SH
#: mlock.2:272
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

# type: Plain text
#: mlock.2:282
msgid ""
"On POSIX systems on which B<mlock>()  and B<munlock>()  are available, "
"B<_POSIX_MEMLOCK_RANGE> is defined in E<lt>unistd.hE<gt> and the value "
"B<PAGESIZE> from E<lt>limits.hE<gt> indicates the number of bytes per page."
msgstr ""

# type: Plain text
#: mlock.2:291
msgid ""
"On POSIX systems on which B<mlockall>()  and B<munlockall>()  are available, "
"B<_POSIX_MEMLOCK> is defined in E<lt>unistd.hE<gt> to a value greater than "
"0. (See also B<sysconf>(3).)"
msgstr ""

# type: SH
#: mlock.2:294
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: mlock.2:296
msgid "POSIX.1-2001, SVr4"
msgstr ""

# type: SH
#: mlock.2:296
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: mlock.2:301
msgid "B<mmap>(2), B<shmctl>(2), B<setrlimit>(2), B<sysconf>(3), B<capabilities>(7)"
msgstr ""
