# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: sigaction.2:40
#, no-wrap
msgid "SIGACTION"
msgstr ""

# type: TH
#: sigaction.2:40
#, no-wrap
msgid "2004-11-11"
msgstr ""

# type: TH
#: sigaction.2:40
#, no-wrap
msgid "Linux 2.6.9"
msgstr ""

# type: TH
#: sigaction.2:40
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: sigaction.2:41
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: sigaction.2:44
msgid "sigaction, sigprocmask, sigpending, sigsuspend - POSIX signal handling API"
msgstr ""

# type: SH
#: sigaction.2:44
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: sigaction.2:46
msgid "B<#include E<lt>signal.hE<gt>>"
msgstr ""

# type: Plain text
#: sigaction.2:49
msgid ""
"B<int sigaction(int >I<signum>B<, const struct sigaction *>I<act>B<,> "
"B<struct sigaction *>I<oldact>B<);>"
msgstr ""

# type: Plain text
#: sigaction.2:52
msgid ""
"B<int sigprocmask(int >I<how>B<, const sigset_t *>I<set>B<,> B<sigset_t "
"*>I<oldset>B<);>"
msgstr ""

# type: Plain text
#: sigaction.2:54
msgid "B<int sigpending(sigset_t *>I<set>B<);>"
msgstr ""

# type: Plain text
#: sigaction.2:56
msgid "B<int sigsuspend(const sigset_t *>I<mask>B<);>"
msgstr ""

# type: SH
#: sigaction.2:56
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: sigaction.2:61
msgid ""
"The B<sigaction>()  system call is used to change the action taken by a "
"process on receipt of a specific signal."
msgstr ""

# type: Plain text
#: sigaction.2:67
msgid ""
"I<signum> specifies the signal and can be any valid signal except B<SIGKILL> "
"and B<SIGSTOP>."
msgstr ""

# type: Plain text
#: sigaction.2:78
msgid ""
"If I<act> is non-null, the new action for signal I<signum> is installed from "
"I<act>.  If I<oldact> is non-null, the previous action is saved in "
"I<oldact>."
msgstr ""

# type: Plain text
#: sigaction.2:82
msgid "The I<sigaction> structure is defined as something like"
msgstr ""

# type: Plain text
#: sigaction.2:92
#, no-wrap
msgid ""
"struct sigaction {\n"
"    void (*sa_handler)(int);\n"
"    void (*sa_sigaction)(int, siginfo_t *, void *);\n"
"    sigset_t sa_mask;\n"
"    int sa_flags;\n"
"    void (*sa_restorer)(void);\n"
"}\n"
msgstr ""

# type: Plain text
#: sigaction.2:99
msgid ""
"On some architectures a union is involved - do not assign to both "
"I<sa_handler> and I<sa_sigaction>."
msgstr ""

# type: Plain text
#: sigaction.2:106
msgid ""
"The I<sa_restorer> element is obsolete and should not be used.  POSIX does "
"not specify a I<sa_restorer> element."
msgstr ""

# type: Plain text
#: sigaction.2:116
msgid ""
"I<sa_handler> specifies the action to be associated with I<signum> and may "
"be B<SIG_DFL> for the default action, B<SIG_IGN> to ignore this signal, or a "
"pointer to a signal handling function.  This function receives the signal "
"number as its only argument."
msgstr ""

# type: Plain text
#: sigaction.2:126
msgid ""
"I<sa_sigaction> also specifies the action to be associated with I<signum>.  "
"This function receives the signal number as its first argument, a pointer to "
"a I<siginfo_t> as its second argument and a pointer to a I<ucontext_t> (cast "
"to void *) as its third argument."
msgstr ""

# type: Plain text
#: sigaction.2:133
msgid ""
"I<sa_mask> gives a mask of signals which should be blocked during execution "
"of the signal handler.  In addition, the signal which triggered the handler "
"will be blocked, unless the B<SA_NODEFER> flag is used."
msgstr ""

# type: Plain text
#: sigaction.2:137
msgid ""
"I<sa_flags> specifies a set of flags which modify the behaviour of the "
"signal handling process. It is formed by the bitwise OR of zero or more of "
"the following:"
msgstr ""

# type: TP
#: sigaction.2:138
#, no-wrap
msgid "B<SA_NOCLDSTOP>"
msgstr ""

# type: Plain text
#: sigaction.2:153
msgid ""
"If I<signum> is B<SIGCHLD>, do not receive notification when child processes "
"stop (i.e., when they receive one of B<SIGSTOP>, B<SIGTSTP>, B<SIGTTIN> or "
"B<SIGTTOU>)  or resume (i.e., they receive B<SIGCONT>)  (see B<wait>(2))."
msgstr ""

# type: TP
#: sigaction.2:153
#, no-wrap
msgid "B<SA_NOCLDWAIT>"
msgstr ""

# type: Plain text
#: sigaction.2:156
msgid "(Linux 2.6 and later)"
msgstr ""

# type: Plain text
#: sigaction.2:164
msgid ""
"If I<signum> is B<SIGCHLD>, do not transform children into zombies when they "
"terminate.  See also B<waitpid>(2)."
msgstr ""

# type: TP
#: sigaction.2:164
#, no-wrap
msgid "B<SA_RESETHAND>"
msgstr ""

# type: Plain text
#: sigaction.2:170
msgid ""
"Restore the signal action to the default state once the signal handler has "
"been called.  B<SA_ONESHOT> is an obsolete, non-standard synonym for this "
"flag."
msgstr ""

# type: TP
#: sigaction.2:170
#, no-wrap
msgid "B<SA_ONSTACK>"
msgstr ""

# type: Plain text
#: sigaction.2:175
msgid ""
"Call the signal handler on an alternate signal stack provided by "
"B<sigaltstack>(2).  If an alternate stack is not available, the default "
"stack will be used."
msgstr ""

# type: TP
#: sigaction.2:175
#, no-wrap
msgid "B<SA_RESTART>"
msgstr ""

# type: Plain text
#: sigaction.2:179
msgid ""
"Provide behaviour compatible with BSD signal semantics by making certain "
"system calls restartable across signals."
msgstr ""

# type: TP
#: sigaction.2:179
#, no-wrap
msgid "B<SA_NODEFER>"
msgstr ""

# type: Plain text
#: sigaction.2:185
msgid ""
"Do not prevent the signal from being received from within its own signal "
"handler.  B<SA_NOMASK> is an obsolete, non-standard synonym for this flag."
msgstr ""

# type: TP
#: sigaction.2:185
#, no-wrap
msgid "B<SA_SIGINFO>"
msgstr ""

# type: Plain text
#: sigaction.2:194
msgid ""
"The signal handler takes 3 arguments, not one.  In this case, "
"I<sa_sigaction> should be set instead of I<sa_handler>.  (The "
"I<sa_sigaction> field was added in Linux 2.1.86.)"
msgstr ""

# type: Plain text
#: sigaction.2:201
msgid ""
"The I<siginfo_t> parameter to I<sa_sigaction> is a struct with the following "
"elements"
msgstr ""

# type: Plain text
#: sigaction.2:221
#, no-wrap
msgid ""
"siginfo_t {\n"
"\tint\tsi_signo;\t/* Signal number */\n"
"\tint\tsi_errno;\t/* An errno value */\n"
"\tint\tsi_code;\t/* Signal code */\n"
"\tpid_t\tsi_pid;\t/* Sending process ID */\n"
"\tuid_t\tsi_uid;\t/* Real user ID of sending process */\n"
"\tint\tsi_status;\t/* Exit value or signal */\n"
"\tclock_t\tsi_utime;\t/* User time consumed */\n"
"\tclock_t\tsi_stime;\t/* System time consumed */\n"
"\tsigval_t\tsi_value;\t/* Signal value */\n"
"\tint\tsi_int;\t/* POSIX.1b signal */\n"
"\tvoid *\tsi_ptr;\t/* POSIX.1b signal */\n"
"\tvoid *\tsi_addr;\t/* Memory location which caused fault */\n"
"\tint\tsi_band;\t/* Band event */\n"
"\tint\tsi_fd;\t/* File descriptor */\n"
"}\n"
msgstr ""

# type: Plain text
#: sigaction.2:238
msgid ""
"I<si_signo>, I<si_errno> and I<si_code> are defined for all signals.  The "
"rest of the struct may be a union, so that one should only read the fields "
"that are meaningful for the given signal.  POSIX.1b signals and B<SIGCHLD> "
"fill in I<si_pid> and I<si_uid>.  B<SIGCHLD> also fills in I<si_status>, "
"I<si_utime> and I<si_stime>.  I<si_int> and I<si_ptr> are specified by the "
"sender of the POSIX.1b signal."
msgstr ""

# type: Plain text
#: sigaction.2:252
msgid ""
"B<SIGILL>, B<SIGFPE>, B<SIGSEGV>, and B<SIGBUS> fill in I<si_addr> with the "
"address of the fault.  B<SIGPOLL> fills in I<si_band> and I<si_fd>."
msgstr ""

# type: Plain text
#: sigaction.2:256
msgid ""
"I<si_code> indicates why this signal was sent.  It is a value, not a "
"bitmask.  The values which are possible for any signal are listed in this "
"table:"
msgstr ""

# type: tbl table
#: sigaction.2:260
#, no-wrap
msgid "I<si_code>\n"
msgstr ""

# type: tbl table
#: sigaction.2:261
#, no-wrap
msgid "Value:Signal origin\n"
msgstr ""

# type: tbl table
#: sigaction.2:262
#, no-wrap
msgid "SI_USER:kill(), sigsend(), or raise()\n"
msgstr ""

# type: tbl table
#: sigaction.2:263
#, no-wrap
msgid "SI_KERNEL:The kernel\n"
msgstr ""

# type: tbl table
#: sigaction.2:264
#, no-wrap
msgid "SI_QUEUE:sigqueue()\n"
msgstr ""

# type: tbl table
#: sigaction.2:265
#, no-wrap
msgid "SI_TIMER:POSIX timer expired\n"
msgstr ""

# type: tbl table
#: sigaction.2:266
#, no-wrap
msgid "SI_MESGQ:POSIX message queue state changed (since Linux 2.6.6)\n"
msgstr ""

# type: tbl table
#: sigaction.2:267
#, no-wrap
msgid "SI_ASYNCIO:AIO completed\n"
msgstr ""

# type: tbl table
#: sigaction.2:268
#, no-wrap
msgid "SI_SIGIO:queued SIGIO\n"
msgstr ""

# type: tbl table
#: sigaction.2:269
#, no-wrap
msgid "SI_TKILL:tkill() or tgkill() (since Linux 2.4.19)\n"
msgstr ""

# type: tbl table
#: sigaction.2:270
#, no-wrap
msgid ".\\\" FIXME? SI_DETHREAD is defined in 2.6.9 sources, but doesn't yet\n"
msgstr ""

# type: tbl table
#: sigaction.2:271
#, no-wrap
msgid ".\\\" seem to be implemented.  Maybe it will be later?  Dec 04, MTK\n"
msgstr ""

# type: tbl table
#: sigaction.2:278
#, no-wrap
msgid "SIGILL\n"
msgstr ""

# type: tbl table
#: sigaction.2:279
#, no-wrap
msgid "ILL_ILLOPC:illegal opcode\n"
msgstr ""

# type: tbl table
#: sigaction.2:280
#, no-wrap
msgid "ILL_ILLOPN:illegal operand\n"
msgstr ""

# type: tbl table
#: sigaction.2:281
#, no-wrap
msgid "ILL_ILLADR:illegal addressing mode\n"
msgstr ""

# type: tbl table
#: sigaction.2:282
#, no-wrap
msgid "ILL_ILLTRP:illegal trap\n"
msgstr ""

# type: tbl table
#: sigaction.2:283
#, no-wrap
msgid "ILL_PRVOPC:privileged opcode\n"
msgstr ""

# type: tbl table
#: sigaction.2:284
#, no-wrap
msgid "ILL_PRVREG:privileged register\n"
msgstr ""

# type: tbl table
#: sigaction.2:285
#, no-wrap
msgid "ILL_COPROC:coprocessor error\n"
msgstr ""

# type: tbl table
#: sigaction.2:286
#, no-wrap
msgid "ILL_BADSTK:internal stack error\n"
msgstr ""

# type: tbl table
#: sigaction.2:293
#, no-wrap
msgid "SIGFPE\n"
msgstr ""

# type: tbl table
#: sigaction.2:294
#, no-wrap
msgid "FPE_INTDIV:integer divide by zero\n"
msgstr ""

# type: tbl table
#: sigaction.2:295
#, no-wrap
msgid "FPE_INTOVF:integer overflow\n"
msgstr ""

# type: tbl table
#: sigaction.2:296
#, no-wrap
msgid "FPE_FLTDIV:floating point divide by zero\n"
msgstr ""

# type: tbl table
#: sigaction.2:297
#, no-wrap
msgid "FPE_FLTOVF:floating point overflow\n"
msgstr ""

# type: tbl table
#: sigaction.2:298
#, no-wrap
msgid "FPE_FLTUND:floating point underflow\n"
msgstr ""

# type: tbl table
#: sigaction.2:299
#, no-wrap
msgid "FPE_FLTRES:floating point inexact result\n"
msgstr ""

# type: tbl table
#: sigaction.2:300
#, no-wrap
msgid "FPE_FLTINV:floating point invalid operation\n"
msgstr ""

# type: tbl table
#: sigaction.2:301
#, no-wrap
msgid "FPE_FLTSUB:subscript out of range\n"
msgstr ""

# type: tbl table
#: sigaction.2:308
#, no-wrap
msgid "SIGSEGV\n"
msgstr ""

# type: tbl table
#: sigaction.2:309
#, no-wrap
msgid "SEGV_MAPERR:address not mapped to object\n"
msgstr ""

# type: tbl table
#: sigaction.2:310
#, no-wrap
msgid "SEGV_ACCERR:invalid permissions for mapped object\n"
msgstr ""

# type: tbl table
#: sigaction.2:317
#, no-wrap
msgid "SIGBUS\n"
msgstr ""

# type: tbl table
#: sigaction.2:318
#, no-wrap
msgid "BUS_ADRALN:invalid address alignment\n"
msgstr ""

# type: tbl table
#: sigaction.2:319
#, no-wrap
msgid "BUS_ADRERR:non-existent physical address\n"
msgstr ""

# type: tbl table
#: sigaction.2:320
#, no-wrap
msgid "BUS_OBJERR:object specific hardware error\n"
msgstr ""

# type: tbl table
#: sigaction.2:327
#, no-wrap
msgid "SIGTRAP\n"
msgstr ""

# type: tbl table
#: sigaction.2:328
#, no-wrap
msgid "TRAP_BRKPT:process breakpoint\n"
msgstr ""

# type: tbl table
#: sigaction.2:329
#, no-wrap
msgid "TRAP_TRACE:process trace trap\n"
msgstr ""

# type: tbl table
#: sigaction.2:336
#, no-wrap
msgid "SIGCHLD\n"
msgstr ""

# type: tbl table
#: sigaction.2:337
#, no-wrap
msgid "CLD_EXITED:child has exited\n"
msgstr ""

# type: tbl table
#: sigaction.2:338
#, no-wrap
msgid "CLD_KILLED:child was killed\n"
msgstr ""

# type: tbl table
#: sigaction.2:339
#, no-wrap
msgid "CLD_DUMPED:child terminated abnormally\n"
msgstr ""

# type: tbl table
#: sigaction.2:340
#, no-wrap
msgid "CLD_TRAPPED:traced child has trapped\n"
msgstr ""

# type: tbl table
#: sigaction.2:341
#, no-wrap
msgid "CLD_STOPPED:child has stopped\n"
msgstr ""

# type: tbl table
#: sigaction.2:342
#, no-wrap
msgid "CLD_CONTINUED:stopped child has continued (since Linux 2.6.9)\n"
msgstr ""

# type: tbl table
#: sigaction.2:349
#, no-wrap
msgid "SIGPOLL\n"
msgstr ""

# type: tbl table
#: sigaction.2:350
#, no-wrap
msgid "POLL_IN:data input available\n"
msgstr ""

# type: tbl table
#: sigaction.2:351
#, no-wrap
msgid "POLL_OUT:output buffers available\n"
msgstr ""

# type: tbl table
#: sigaction.2:352
#, no-wrap
msgid "POLL_MSG:input message available\n"
msgstr ""

# type: tbl table
#: sigaction.2:353
#, no-wrap
msgid "POLL_ERR:i/o error\n"
msgstr ""

# type: tbl table
#: sigaction.2:354
#, no-wrap
msgid "POLL_PRI:high priority input available\n"
msgstr ""

# type: tbl table
#: sigaction.2:355
#, no-wrap
msgid "POLL_HUP:device disconnected\n"
msgstr ""

# type: Plain text
#: sigaction.2:365
msgid ""
"The B<sigprocmask>()  call is used to change the list of currently blocked "
"signals. The behaviour of the call is dependent on the value of I<how>, as "
"follows."
msgstr ""

# type: TP
#: sigaction.2:366
#, no-wrap
msgid "B<SIG_BLOCK>"
msgstr ""

# type: Plain text
#: sigaction.2:371
msgid ""
"The set of blocked signals is the union of the current set and the I<set> "
"argument."
msgstr ""

# type: TP
#: sigaction.2:371
#, no-wrap
msgid "B<SIG_UNBLOCK>"
msgstr ""

# type: Plain text
#: sigaction.2:377
msgid ""
"The signals in I<set> are removed from the current set of blocked signals.  "
"It is legal to attempt to unblock a signal which is not blocked."
msgstr ""

# type: TP
#: sigaction.2:377
#, no-wrap
msgid "B<SIG_SETMASK>"
msgstr ""

# type: Plain text
#: sigaction.2:381
msgid "The set of blocked signals is set to the argument I<set>."
msgstr ""

# type: Plain text
#: sigaction.2:387
msgid ""
"If I<oldset> is non-null, the previous value of the signal mask is stored in "
"I<oldset>."
msgstr ""

# type: Plain text
#: sigaction.2:394
msgid ""
"The B<sigpending>()  call allows the examination of pending signals (ones "
"which have been raised while blocked).  The signal mask of pending signals "
"is stored in I<set>."
msgstr ""

# type: Plain text
#: sigaction.2:401
msgid ""
"The B<sigsuspend>()  call temporarily replaces the signal mask for the "
"process with that given by I<mask> and then suspends the process until a "
"signal is received."
msgstr ""

# type: SH
#: sigaction.2:402
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: sigaction.2:411
msgid ""
"B<sigaction>(), B<sigprocmask>(), and B<sigpending>()  return 0 on success "
"and -1 on error.  B<sigsuspend>()  always returns -1, normally with the "
"error B<EINTR>."
msgstr ""

# type: SH
#: sigaction.2:412
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: sigaction.2:413
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

# type: Plain text
#: sigaction.2:419
msgid ""
"I<act>, I<oldact>, I<set>, I<oldset> or I<mask> point to memory which is not "
"a valid part of the process address space."
msgstr ""

# type: TP
#: sigaction.2:419
#, no-wrap
msgid "B<EINTR>"
msgstr ""

# type: Plain text
#: sigaction.2:422
msgid "System call was interrupted."
msgstr ""

# type: TP
#: sigaction.2:422
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: sigaction.2:428
msgid ""
"An invalid signal was specified.  This will also be generated if an attempt "
"is made to change the action for B<SIGKILL> or B<SIGSTOP>, which cannot be "
"caught."
msgstr ""

# type: SH
#: sigaction.2:428
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: sigaction.2:432
msgid ""
"It is not possible to block B<SIGKILL> or B<SIGSTOP> with the sigprocmask "
"call.  Attempts to do so will be silently ignored."
msgstr ""

# type: Plain text
#: sigaction.2:449
msgid ""
"According to POSIX, the behaviour of a process is undefined after it ignores "
"a B<SIGFPE>, B<SIGILL>, or B<SIGSEGV> signal that was not generated by "
"B<kill>()  or B<raise>().  Integer division by zero has undefined result.  "
"On some architectures it will generate a B<SIGFPE> signal.  (Also dividing "
"the most negative integer by -1 may generate SIGFPE.)  Ignoring this signal "
"might lead to an endless loop."
msgstr ""

# type: Plain text
#: sigaction.2:466
msgid ""
"POSIX.1-1990 disallowed setting the action for B<SIGCHLD> to B<SIG_IGN>.  "
"POSIX.1-2001 allows this possibility, so that ignoring B<SIGCHLD> can be "
"used to prevent the creation of zombies (see B<wait>(2)).  Nevertheless, the "
"historical BSD and System V behaviours for ignoring B<SIGCHLD> differ, so "
"that the only completely portable method of ensuring that terminated "
"children do not become zombies is to catch the B<SIGCHLD> signal and perform "
"a B<wait>(2)  or similar."
msgstr ""

# type: Plain text
#: sigaction.2:479
msgid ""
"POSIX.1-1990 only specified B<SA_NOCLDSTOP>.  POSIX.1-2001 added "
"B<SA_NOCLDWAIT>, B<SA_RESETHAND>, B<SA_NODEFER>, and B<SA_SIGINFO>.  Use of "
"these latter values in I<sa_flags> may be less portable in applications "
"intended for older Unix implementations."
msgstr ""

# type: Plain text
#: sigaction.2:483
msgid "Support for B<SA_SIGINFO> was added in Linux 2.2."
msgstr ""

# type: Plain text
#: sigaction.2:487
msgid "The B<SA_RESETHAND> flag is compatible with the SVr4 flag of the same name."
msgstr ""

# type: Plain text
#: sigaction.2:496
msgid ""
"The B<SA_NODEFER> flag is compatible with the SVr4 flag of the same name "
"under kernels 1.3.9 and newer.  On older kernels the Linux implementation "
"allowed the receipt of any signal, not just the one we are installing "
"(effectively overriding any I<sa_mask> settings)."
msgstr ""

# type: Plain text
#: sigaction.2:502
msgid ""
"The B<SA_RESETHAND> and B<SA_NODEFER> names for SVr4 compatibility are "
"present only in library versions 3.0.9 and greater."
msgstr ""

# type: Plain text
#: sigaction.2:507
msgid ""
"B<sigaction>()  can be called with a null second argument to query the "
"current signal handler. It can also be used to check whether a given signal "
"is valid for the current machine by calling it with null second and third "
"arguments."
msgstr ""

# type: Plain text
#: sigaction.2:511
msgid "See B<sigsetops>(3)  for details on manipulating signal sets."
msgstr ""

# type: SH
#: sigaction.2:511
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: sigaction.2:513
msgid "POSIX, SVr4.  SVr4 does not document the EINTR condition."
msgstr ""

# type: SH
#: sigaction.2:514
#, no-wrap
msgid "UNDOCUMENTED"
msgstr ""

# type: Plain text
#: sigaction.2:524
msgid ""
"Before the introduction of B<SA_SIGINFO> it was also possible to get some "
"additional information, namely by using a I<sa_handler> with second argument "
"of type I<struct sigcontext>.  See the relevant kernel sources for details.  "
"This use is obsolete now."
msgstr ""

# type: SH
#: sigaction.2:525
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: sigaction.2:538
msgid ""
"B<kill>(1), B<kill>(2), B<pause>(2), B<sigaltstack>(2), B<signal>(2), "
"B<sigqueue>(2), B<sigvec>(2), B<wait>(2), B<killpg>(3), B<raise>(3), "
"B<siginterrupt>(3), B<sigsetops>(3), B<signal>(7)"
msgstr ""
