# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: semget.2:34
#, no-wrap
msgid "SEMGET"
msgstr ""

# type: TH
#: semget.2:34
#, no-wrap
msgid "2004-05-27"
msgstr ""

# type: TH
#: semget.2:34
#, no-wrap
msgid "Linux 2.6.6"
msgstr ""

# type: TH
#: semget.2:34
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: semget.2:35
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: semget.2:37
msgid "semget - get a semaphore set identifier"
msgstr ""

# type: SH
#: semget.2:37
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: semget.2:45
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/ipc.hE<gt>>\n"
"B<#include E<lt>sys/sem.hE<gt>>\n"
msgstr ""

# type: Plain text
#: semget.2:50
msgid "B<int semget(key_t >I<key>B<,> B<int >I<nsems>B<,> B<int >I<semflg>B<);>"
msgstr ""

# type: SH
#: semget.2:50
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: semget.2:68
msgid ""
"The B<semget>()  system call returns the semaphore set identifier associated "
"with the argument I<key>.  A new set of I<nsems> semaphores is created if "
"I<key> has the value B<IPC_PRIVATE> or if no existing semaphore set is "
"associated with I<key> and B<IPC_CREAT> is specified in I<semflg>."
msgstr ""

# type: Plain text
#: semget.2:87
msgid ""
"If I<semflg> specifies both B<IPC_CREAT> and B<IPC_EXCL> and a semaphore set "
"already exists for I<key>, then B<semget>()  fails with I<errno> set to "
"B<EEXIST>.  (This is analogous to the effect of the combination B<O_CREAT | "
"O_EXCL> for B<open>(2).)"
msgstr ""

# type: Plain text
#: semget.2:100
msgid ""
"Upon creation, the least significant 9 bits of the argument I<semflg> define "
"the permissions (for owner, group and others)  for the semaphore set.  These "
"bits have the same format, and the same meaning, as the I<mode> argument of "
"B<open>(2)  (though the execute permissions are not meaningful for "
"semaphores, and write permissions mean permission to alter semaphore "
"values)."
msgstr ""

# type: Plain text
#: semget.2:108
msgid ""
"When creating a new semaphore set, B<semget>()  initialises the set's "
"associated data structure I<semid_ds> (see B<semctl>(2))  as follows:"
msgstr ""

# type: Plain text
#: semget.2:113
msgid ""
"I<sem_perm.cuid> and I<sem_perm.uid> are set to the effective user-ID of the "
"calling process."
msgstr ""

# type: Plain text
#: semget.2:118
msgid ""
"I<sem_perm.cgid> and I<sem_perm.gid> are set to the effective group-ID of "
"the calling process."
msgstr ""

# type: Plain text
#: semget.2:123
msgid ""
"The least significant 9 bits of I<sem_perm.mode> are set to the least "
"significant 9 bits of I<semflg>."
msgstr ""

# type: Plain text
#: semget.2:127
msgid "I<sem_nsems> is set to the value of I<nsems>."
msgstr ""

# type: Plain text
#: semget.2:130
msgid "I<sem_otime> is set to 0."
msgstr ""

# type: Plain text
#: semget.2:133
msgid "I<sem_ctime> is set to the current time."
msgstr ""

# type: Plain text
#: semget.2:144
msgid ""
"The argument I<nsems> can be 0 (a don't care)  when a semaphore set is not "
"being created.  Otherwise I<nsems> must be greater than 0 and less than or "
"equal to the maximum number of semaphores per semaphore set (B<SEMMSL>)."
msgstr ""

# type: Plain text
#: semget.2:147
msgid "If the semaphore set already exists, the permissions are verified."
msgstr ""

# type: SH
#: semget.2:148
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: semget.2:154
msgid ""
"If successful, the return value will be the semaphore set identifier (a "
"nonnegative integer), otherwise -1 is returned, with I<errno> indicating the "
"error."
msgstr ""

# type: SH
#: semget.2:154
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: Plain text
#: semget.2:158
msgid "On failure I<errno> will be set to one of the following:"
msgstr ""

# type: TP
#: semget.2:158
#, no-wrap
msgid "B<EACCES>"
msgstr ""

# type: Plain text
#: semget.2:166
msgid ""
"A semaphore set exists for I<key>, but the calling process does not have "
"permission to access the set, and does not have the B<CAP_IPC_OWNER> "
"capability."
msgstr ""

# type: TP
#: semget.2:166
#, no-wrap
msgid "B<EEXIST>"
msgstr ""

# type: Plain text
#: semget.2:176
msgid ""
"A semaphore set exists for B<key> and I<semflg> specified both B<IPC_CREAT> "
"and B<IPC_EXCL>."
msgstr ""

# type: TP
#: semget.2:179
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: semget.2:190
msgid ""
"I<nsems> is less than 0 or greater than the limit on the number of "
"semaphores per semaphore set (B<SEMMSL>), or a semaphore set corresponding "
"to I<key> already exists, and I<nsems> is larger than the number of "
"semaphores in that set."
msgstr ""

# type: TP
#: semget.2:190
#, no-wrap
msgid "B<ENOENT>"
msgstr ""

# type: Plain text
#: semget.2:198
msgid ""
"No semaphore set exists for I<key> and I<semflg> did not specify "
"B<IPC_CREAT>."
msgstr ""

# type: TP
#: semget.2:198
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

# type: Plain text
#: semget.2:202
msgid ""
"A semaphore set has to be created but the system does not have enough memory "
"for the new data structure."
msgstr ""

# type: TP
#: semget.2:202
#, no-wrap
msgid "B<ENOSPC>"
msgstr ""

# type: Plain text
#: semget.2:210
msgid ""
"A semaphore set has to be created but the system limit for the maximum "
"number of semaphore sets (B<SEMMNI>), or the system wide maximum number of "
"semaphores (B<SEMMNS>), would be exceeded."
msgstr ""

# type: SH
#: semget.2:210
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: semget.2:220
msgid ""
"B<IPC_PRIVATE> isn't a flag field but a I<key_t> type.  If this special "
"value is used for I<key>, the system call ignores everything but the least "
"significant 9 bits of I<semflg> and creates a new semaphore set (on "
"success)."
msgstr ""

# type: Plain text
#: semget.2:224
msgid ""
"The following limits on semaphore set resources affect the B<semget>()  "
"call:"
msgstr ""

# type: TP
#: semget.2:224
#, no-wrap
msgid "B<SEMMNI>"
msgstr ""

# type: Plain text
#: semget.2:229
msgid ""
"System wide maximum number of semaphore sets: policy dependent (on Linux, "
"this limit can be read and modified via the fourth field of "
"I</proc/sys/kernel/sem>)."
msgstr ""

# type: TP
#: semget.2:230
#, no-wrap
msgid "B<SEMMSL>"
msgstr ""

# type: Plain text
#: semget.2:235
msgid ""
"Maximum number of semaphores per semid: implementation dependent (on Linux, "
"this limit can be read and modified via the first field of "
"I</proc/sys/kernel/sem>)."
msgstr ""

# type: TP
#: semget.2:235
#, no-wrap
msgid "B<SEMMNS>"
msgstr ""

# type: Plain text
#: semget.2:243
msgid ""
"System wide maximum number of semaphores: policy dependent (on Linux, this "
"limit can be read and modified via the second field of "
"I</proc/sys/kernel/sem>).  Values greater than B<SEMMSL * SEMMNI> makes it "
"irrelevant."
msgstr ""

# type: SH
#: semget.2:243
#, no-wrap
msgid "BUGS"
msgstr ""

# type: Plain text
#: semget.2:246
msgid ""
"The name choice IPC_PRIVATE was perhaps unfortunate, IPC_NEW would more "
"clearly show its function."
msgstr ""

# type: Plain text
#: semget.2:249
msgid "The semaphores in a set are not initialised by B<semget>()."
msgstr ""

# type: Plain text
#: semget.2:265
msgid ""
"In order to initialise the semaphores, B<semctl>(2)  must be used to perform "
"a B<SETVAL> or a B<SETALL> operation on the semaphore set.  (Where multiple "
"peers do not know who will be the first to initialise the set, checking for "
"a non-zero I<sem_otime> in the associated data structure retrieved by a "
"B<semctl>()  B<IPC_STAT> operation can be used to avoid races.)"
msgstr ""

# type: SH
#: semget.2:265
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: semget.2:269
msgid ""
"SVr4, SVID.  SVr4 documents additional error conditions EFBIG, E2BIG, "
"EAGAIN, ERANGE, EFAULT."
msgstr ""

# type: SH
#: semget.2:269
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: semget.2:274
msgid "B<semctl>(2), B<semop>(2), B<ftok>(3), B<ipc>(5), B<capabilities>(7)"
msgstr ""
