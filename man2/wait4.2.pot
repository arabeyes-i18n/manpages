# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: wait4.2:34
#, no-wrap
msgid "WAIT4"
msgstr ""

# type: TH
#: wait4.2:34
#, no-wrap
msgid "2004-11-11"
msgstr ""

# type: TH
#: wait4.2:34
#, no-wrap
msgid "Linux"
msgstr ""

# type: TH
#: wait4.2:34
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: wait4.2:35
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: wait4.2:37
msgid "wait3, wait4 - wait for process to change state, BSD style"
msgstr ""

# type: SH
#: wait4.2:37
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: wait4.2:43
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/time.hE<gt>>\n"
"B<#include E<lt>sys/resource.hE<gt>>\n"
"B<#include E<lt>sys/wait.hE<gt>>\n"
msgstr ""

# type: Plain text
#: wait4.2:46
#, no-wrap
msgid ""
"B<pid_t wait3(int *>I<status>B<, int >I<options>B<,>\n"
"B< struct rusage *>I<rusage>B<);>\n"
msgstr ""

# type: Plain text
#: wait4.2:49
#, no-wrap
msgid ""
"B<pid_t wait4(pid_t >I<pid>B<, int *>I<status>B<, int >I<options>B<,>\n"
"B< struct rusage *>I<rusage>B<);>\n"
msgstr ""

# type: SH
#: wait4.2:50
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: wait4.2:60
msgid ""
"The B<wait3>()  and B<wait4>()  system calls are similar to B<waitpid>(2), "
"but additionally return resource usage information about the child in the "
"structure pointed to by I<rusage>."
msgstr ""

# type: Plain text
#: wait4.2:66
msgid ""
"Other than the use of the I<rusage> argument, the following B<wait3>()  "
"call:"
msgstr ""

# type: Plain text
#: wait4.2:69
#, no-wrap
msgid "    wait3(status, options, rusage);\n"
msgstr ""

# type: Plain text
#: wait4.2:72 wait4.2:86
msgid "is equivalent to:"
msgstr ""

# type: Plain text
#: wait4.2:75
#, no-wrap
msgid "    waitpid(-1, status, options);\n"
msgstr ""

# type: Plain text
#: wait4.2:80
msgid "Similarly, the following B<wait4>()  call:"
msgstr ""

# type: Plain text
#: wait4.2:83
#, no-wrap
msgid "    wait4(pid, status, options, rusage);\n"
msgstr ""

# type: Plain text
#: wait4.2:89
#, no-wrap
msgid "    waitpid(pid, status, options);\n"
msgstr ""

# type: Plain text
#: wait4.2:99
msgid ""
"In other words, B<wait3>()  waits of any child, while B<wait4>()  can be "
"used to select a specific child, or children, on which to wait.  See "
"B<wait>(2)  for further details."
msgstr ""

# type: Plain text
#: wait4.2:111
msgid ""
"If I<rusage> is not B<NULL>, the I<struct rusage> to which it points will be "
"filled with accounting information about the child.  See B<getrusage>(2)  "
"for details."
msgstr ""

# type: SH
#: wait4.2:111
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: wait4.2:114 wait4.2:117
msgid "As for B<waitpid>(2)."
msgstr ""

# type: SH
#: wait4.2:114
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: SH
#: wait4.2:117
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: wait4.2:129
msgid ""
"Including I<E<lt>sys/time.hE<gt>> is not required these days, but increases "
"portability.  (Indeed, I<E<lt>sys/resource.hE<gt>> defines the I<rusage> "
"structure with fields of type I<struct timeval> defined in "
"I<E<lt>sys/time.hE<gt>>.)"
msgstr ""

# type: Plain text
#: wait4.2:134
msgid ""
"The prototype for these functions is only available if B<_BSD_SOURCE> is "
"defined (either explicitly, or implicitly, by not defining _POSIX_SOURCE or "
"compiling with the -ansi flag)."
msgstr ""

# type: SH
#: wait4.2:134
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: wait4.2:136
msgid "4.3BSD"
msgstr ""

# type: SH
#: wait4.2:136
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: wait4.2:142
msgid ""
"B<fork>(2), B<getrusage>(2), B<sigaction>(2), B<signal>(2), B<wait>(2), "
"B<signal>(7)"
msgstr ""
