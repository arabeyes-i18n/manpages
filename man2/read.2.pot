# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: read.2:35
#, no-wrap
msgid "READ"
msgstr ""

# type: TH
#: read.2:35
#, no-wrap
msgid "1997-07-12"
msgstr ""

# type: TH
#: read.2:35
#, no-wrap
msgid "Linux 2.0.32"
msgstr ""

# type: TH
#: read.2:35
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: read.2:36
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: read.2:38
msgid "read - read from a file descriptor"
msgstr ""

# type: SH
#: read.2:38
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: read.2:41
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr ""

# type: Plain text
#: read.2:43
#, no-wrap
msgid "B<ssize_t read(int >I<fd>B<, void *>I<buf>B<, size_t >I<count>B<);>\n"
msgstr ""

# type: SH
#: read.2:44
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: read.2:52
msgid ""
"B<read()> attempts to read up to I<count> bytes from file descriptor I<fd> "
"into the buffer starting at I<buf>."
msgstr ""

# type: Plain text
#: read.2:59
msgid ""
"If I<count> is zero, B<read()> returns zero and has no other results.  If "
"I<count> is greater than SSIZE_MAX, the result is unspecified."
msgstr ""

# type: SH
#: read.2:60
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: read.2:72
msgid ""
"On success, the number of bytes read is returned (zero indicates end of "
"file), and the file position is advanced by this number.  It is not an error "
"if this number is smaller than the number of bytes requested; this may "
"happen for example because fewer bytes are actually available right now "
"(maybe because we were close to end-of-file, or because we are reading from "
"a pipe, or from a terminal), or because B<read()> was interrupted by a "
"signal.  On error, -1 is returned, and I<errno> is set appropriately. In "
"this case it is left unspecified whether the file position (if any) changes."
msgstr ""

# type: SH
#: read.2:72
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: read.2:73
#, no-wrap
msgid "B<EAGAIN>"
msgstr ""

# type: Plain text
#: read.2:78
msgid ""
"Non-blocking I/O has been selected using B<O_NONBLOCK> and no data was "
"immediately available for reading."
msgstr ""

# type: TP
#: read.2:78
#, no-wrap
msgid "B<EBADF>"
msgstr ""

# type: Plain text
#: read.2:82
msgid "I<fd> is not a valid file descriptor or is not open for reading."
msgstr ""

# type: TP
#: read.2:82
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

# type: Plain text
#: read.2:86
msgid "I<buf> is outside your accessible address space."
msgstr ""

# type: TP
#: read.2:86
#, no-wrap
msgid "B<EINTR>"
msgstr ""

# type: Plain text
#: read.2:89
msgid "The call was interrupted by a signal before any data was read."
msgstr ""

# type: TP
#: read.2:89
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: read.2:93
msgid "I<fd> is attached to an object which is unsuitable for reading."
msgstr ""

# type: TP
#: read.2:93
#, no-wrap
msgid "B<EIO>"
msgstr ""

# type: Plain text
#: read.2:100
msgid ""
"I/O error. This will happen for example when the process is in a background "
"process group, tries to read from its controlling tty, and either it is "
"ignoring or blocking SIGTTIN or its process group is orphaned.  It may also "
"occur when there is a low-level I/O error while reading from a disk or tape."
msgstr ""

# type: TP
#: read.2:100
#, no-wrap
msgid "B<EISDIR>"
msgstr ""

# type: Plain text
#: read.2:104
msgid "I<fd> refers to a directory."
msgstr ""

# type: Plain text
#: read.2:113
msgid ""
"Other errors may occur, depending on the object connected to I<fd>.  POSIX "
"allows a B<read> that is interrupted after reading some data to return -1 "
"(with I<errno> set to EINTR) or to return the number of bytes already read."
msgstr ""

# type: SH
#: read.2:113
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: read.2:115
msgid "SVr4, SVID, AT&T, POSIX, X/OPEN, BSD 4.3"
msgstr ""

# type: SH
#: read.2:115
#, no-wrap
msgid "RESTRICTIONS"
msgstr ""

# type: Plain text
#: read.2:124
msgid ""
"On NFS file systems, reading small amounts of data will only update the time "
"stamp the first time, subsequent calls may not do so.  This is caused by "
"client side attribute caching, because most if not all NFS clients leave "
"atime updates to the server and client side reads satisfied from the "
"client's cache will not cause atime updates on the server as there are no "
"server side reads.  UNIX semantics can be obtained by disabling client side "
"attribute caching, but in most situations this will substantially increase "
"server load and decrease performance."
msgstr ""

# type: Plain text
#: read.2:130
msgid ""
"Many filesystems and disks were considered to be fast enough that the "
"implementation of B<O_NONBLOCK> was deemed unneccesary. So, O_NONBLOCK may "
"not be available on files and/or disks."
msgstr ""

# type: SH
#: read.2:130
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: read.2:140
msgid ""
"B<close>(2), B<fcntl>(2), B<ioctl>(2), B<lseek>(2), B<readdir>(2), "
"B<readlink>(2), B<select>(2), B<write>(2), B<fread>(3), B<readv>(3)"
msgstr ""
