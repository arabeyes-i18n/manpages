# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: unlink.2:32
#, no-wrap
msgid "UNLINK"
msgstr ""

# type: TH
#: unlink.2:32
#, no-wrap
msgid "2004-06-23"
msgstr ""

# type: TH
#: unlink.2:32
#, no-wrap
msgid "Linux 2.6.7"
msgstr ""

# type: TH
#: unlink.2:32
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: unlink.2:33
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: unlink.2:35
msgid "unlink - delete a name and possibly the file it refers to"
msgstr ""

# type: SH
#: unlink.2:35
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: unlink.2:37
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr ""

# type: Plain text
#: unlink.2:39
msgid "B<int unlink(const char *>I<pathname>B<);>"
msgstr ""

# type: SH
#: unlink.2:39
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: unlink.2:44
msgid ""
"B<unlink> deletes a name from the filesystem. If that name was the last link "
"to a file and no processes have the file open the file is deleted and the "
"space it was using is made available for reuse."
msgstr ""

# type: Plain text
#: unlink.2:48
msgid ""
"If the name was the last link to a file but any processes still have the "
"file open the file will remain in existence until the last file descriptor "
"referring to it is closed."
msgstr ""

# type: Plain text
#: unlink.2:50
msgid "If the name referred to a symbolic link the link is removed."
msgstr ""

# type: Plain text
#: unlink.2:54
msgid ""
"If the name referred to a socket, fifo or device the name for it is removed "
"but processes which have the object open may continue to use it."
msgstr ""

# type: SH
#: unlink.2:54
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: unlink.2:58
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""

# type: SH
#: unlink.2:58
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: unlink.2:59
#, no-wrap
msgid "B<EACCES>"
msgstr ""

# type: Plain text
#: unlink.2:69
msgid ""
"Write access to the directory containing I<pathname> is not allowed for the "
"process's effective UID, or one of the directories in I<pathname> did not "
"allow search permission.  (See also B<path_resolution>(2).)"
msgstr ""

# type: TP
#: unlink.2:69
#, no-wrap
msgid "B<EBUSY> (not on Linux)"
msgstr ""

# type: Plain text
#: unlink.2:75
msgid ""
"The file I<pathname> cannot be unlinked because it is being used by the "
"system or another process and the implementation considers this an error."
msgstr ""

# type: TP
#: unlink.2:75
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

# type: Plain text
#: unlink.2:79
msgid "I<pathname> points outside your accessible address space."
msgstr ""

# type: TP
#: unlink.2:79
#, no-wrap
msgid "B<EIO>"
msgstr ""

# type: Plain text
#: unlink.2:82
msgid "An I/O error occurred."
msgstr ""

# type: TP
#: unlink.2:82
#, no-wrap
msgid "B<EISDIR>"
msgstr ""

# type: Plain text
#: unlink.2:87
msgid ""
"I<pathname> refers to a directory.  (This is the non-POSIX value returned by "
"Linux since 2.1.132.)"
msgstr ""

# type: TP
#: unlink.2:87
#, no-wrap
msgid "B<ELOOP>"
msgstr ""

# type: Plain text
#: unlink.2:91
msgid "Too many symbolic links were encountered in translating I<pathname>."
msgstr ""

# type: TP
#: unlink.2:91
#, no-wrap
msgid "B<ENAMETOOLONG>"
msgstr ""

# type: Plain text
#: unlink.2:94
msgid "I<pathname> was too long."
msgstr ""

# type: TP
#: unlink.2:94
#, no-wrap
msgid "B<ENOENT>"
msgstr ""

# type: Plain text
#: unlink.2:101
msgid ""
"A component in I<pathname> does not exist or is a dangling symbolic link, or "
"I<pathname> is empty."
msgstr ""

# type: TP
#: unlink.2:101
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

# type: Plain text
#: unlink.2:104
msgid "Insufficient kernel memory was available."
msgstr ""

# type: TP
#: unlink.2:104
#, no-wrap
msgid "B<ENOTDIR>"
msgstr ""

# type: Plain text
#: unlink.2:109
msgid "A component used as a directory in I<pathname> is not, in fact, a directory."
msgstr ""

# type: TP
#: unlink.2:109
#, no-wrap
msgid "B<EPERM>"
msgstr ""

# type: Plain text
#: unlink.2:115
msgid ""
"The system does not allow unlinking of directories, or unlinking of "
"directories requires privileges that the current process doesn't have.  "
"(This is the POSIX prescribed error return.)"
msgstr ""

# type: TP
#: unlink.2:115
#, no-wrap
msgid "B<EPERM> (Linux only)"
msgstr ""

# type: Plain text
#: unlink.2:118
msgid "The filesystem does not allow unlinking of files."
msgstr ""

# type: TP
#: unlink.2:118
#, no-wrap
msgid "B<EPERM> or B<EACCES>"
msgstr ""

# type: Plain text
#: unlink.2:129
msgid ""
"The directory containing I<pathname> has the sticky bit (B<S_ISVTX>)  set "
"and the process's effective UID is neither the UID of the file to be deleted "
"nor that of the directory containing it, and the process is not privileged "
"(Linux: does not have the B<CAP_FOWNER> capability)."
msgstr ""

# type: TP
#: unlink.2:129
#, no-wrap
msgid "B<EROFS>"
msgstr ""

# type: Plain text
#: unlink.2:133
msgid "I<pathname> refers to a file on a read-only filesystem."
msgstr ""

# type: SH
#: unlink.2:133
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: unlink.2:136
msgid ""
"SVr4, SVID, POSIX, X/OPEN, 4.3BSD.  SVr4 documents additional error "
"conditions EINTR, EMULTIHOP, ETXTBSY, ENOLINK."
msgstr ""

# type: SH
#: unlink.2:136
#, no-wrap
msgid "BUGS"
msgstr ""

# type: Plain text
#: unlink.2:139
msgid ""
"Infelicities in the protocol underlying NFS can cause the unexpected "
"disappearance of files which are still being used."
msgstr ""

# type: SH
#: unlink.2:139
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: unlink.2:149
msgid ""
"B<rm>(1), B<chmod>(2), B<link>(2), B<mknod>(2), B<open>(2), "
"B<path_resolution>(2), B<rename>(2), B<rmdir>(2), B<mkfifo>(3), B<remove>(3)"
msgstr ""
