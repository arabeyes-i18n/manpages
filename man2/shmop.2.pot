# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: shmop.2:37
#, no-wrap
msgid "SHMOP"
msgstr ""

# type: TH
#: shmop.2:37
#, no-wrap
msgid "2004-11-10"
msgstr ""

# type: TH
#: shmop.2:37
#, no-wrap
msgid "Linux 2.6.9"
msgstr ""

# type: TH
#: shmop.2:37
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: shmop.2:38
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: shmop.2:40
msgid "shmop - shared memory operations"
msgstr ""

# type: SH
#: shmop.2:40
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: shmop.2:46
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/shm.hE<gt>>\n"
msgstr ""

# type: Plain text
#: shmop.2:51
msgid ""
"B<void *shmat(int >I<shmid>B<,> B<const void *>I<shmaddr>B<,> B<int "
">I<shmflg>B<);>"
msgstr ""

# type: Plain text
#: shmop.2:53
msgid "B<int shmdt(const void *>I<shmaddr>B<);>"
msgstr ""

# type: SH
#: shmop.2:53
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: shmop.2:61
msgid ""
"B<shmat>()  attaches the shared memory segment identified by I<shmid> to the "
"address space of the calling process.  The attaching address is specified by "
"I<shmaddr> with one of the following criteria:"
msgstr ""

# type: Plain text
#: shmop.2:68
msgid ""
"If I<shmaddr> is B<NULL>, the system chooses a suitable (unused) address at "
"which to attach the segment."
msgstr ""

# type: Plain text
#: shmop.2:84
msgid ""
"If I<shmaddr> isn't B<NULL> and B<SHM_RND> is specified in I<shmflg>, the "
"attach occurs at the address equal to I<shmaddr> rounded down to the nearest "
"multiple of B<SHMLBA>.  Otherwise I<shmaddr> must be a page-aligned address "
"at which the attach occurs."
msgstr ""

# type: Plain text
#: shmop.2:94
msgid ""
"If B<SHM_RDONLY> is specified in I<shmflg>, the segment is attached for "
"reading and the process must have read permission for the segment.  "
"Otherwise the segment is attached for read and write and the process must "
"have read and write permission for the segment.  There is no notion of a "
"write-only shared memory segment."
msgstr ""

# type: Plain text
#: shmop.2:110
msgid ""
"The (Linux-specific)  B<SHM_REMAP> flag may be specified in I<shmflg> to "
"indicate that the mapping of the segment should replace any existing mapping "
"in the range starting at I<shmaddr> and continuing for the size of the "
"segment.  (Normally an B<EINVAL> error would result if a mapping already "
"exists in this address range.)  In this case, I<shmaddr> must not be "
"B<NULL>."
msgstr ""

# type: Plain text
#: shmop.2:117
msgid ""
"The B<brk>(2)  value of the calling process is not altered by the attach.  "
"The segment will automatically be detached at process exit.  The same "
"segment may be attached as a read and as a read-write one, and more than "
"once, in the process's address space."
msgstr ""

# type: Plain text
#: shmop.2:125
msgid ""
"A successful B<shmat>()  call updates the members of the B<shmid_ds> "
"structure (see B<shmctl>(2))  associated with the shared memory segment as "
"follows:"
msgstr ""

# type: Plain text
#: shmop.2:128
msgid "I<shm_atime> is set to the current time."
msgstr ""

# type: Plain text
#: shmop.2:131 shmop.2:157
msgid "I<shm_lpid> is set to the process-ID of the calling process."
msgstr ""

# type: Plain text
#: shmop.2:134
msgid "I<shm_nattch> is incremented by one."
msgstr ""

# type: Plain text
#: shmop.2:145
msgid ""
"B<shmdt>()  detaches the shared memory segment located at the address "
"specified by I<shmaddr> from the address space of the calling process.  The "
"to-be-detached segment must be currently attached with I<shmaddr> equal to "
"the value returned by the attaching B<shmat>()  call."
msgstr ""

# type: Plain text
#: shmop.2:151
msgid ""
"On a successful B<shmdt>()  call the system updates the members of the "
"I<shmid_ds> structure associated with the shared memory segment as follows:"
msgstr ""

# type: Plain text
#: shmop.2:154
msgid "I<shm_dtime> is set to the current time."
msgstr ""

# type: Plain text
#: shmop.2:162
msgid ""
"I<shm_nattch> is decremented by one.  If it becomes 0 and the segment is "
"marked for deletion, the segment is deleted."
msgstr ""

# type: SH
#: shmop.2:162
#, no-wrap
msgid "SYSTEM CALLS"
msgstr ""

# type: TP
#: shmop.2:163
#, no-wrap
msgid "B<fork>()"
msgstr ""

# type: Plain text
#: shmop.2:168
msgid "After a B<fork>()  the child inherits the attached shared memory segments."
msgstr ""

# type: TP
#: shmop.2:168
#, no-wrap
msgid "B<exec>()"
msgstr ""

# type: Plain text
#: shmop.2:173
msgid ""
"After an B<exec>()  all attached shared memory segments are detached from "
"the process."
msgstr ""

# type: TP
#: shmop.2:173
#, no-wrap
msgid "B<exit>()"
msgstr ""

# type: Plain text
#: shmop.2:178
msgid ""
"Upon B<exit>()  all attached shared memory segments are detached from the "
"process."
msgstr ""

# type: SH
#: shmop.2:178
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: shmop.2:187
msgid ""
"On success B<shmat>()  returns the address of the attached shared memory "
"segment, and B<shmdt>()  returns 0.  On failure both functions return -1 "
"with I<errno> indicating the error."
msgstr ""

# type: SH
#: shmop.2:187
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: Plain text
#: shmop.2:193
msgid "When B<shmat>()  fails, I<errno> is set to one of the following:"
msgstr ""

# type: TP
#: shmop.2:193
#, no-wrap
msgid "B<EACCES>"
msgstr ""

# type: Plain text
#: shmop.2:199
msgid ""
"The calling process does not have the required permissions for the requested "
"attach type, and does not have the B<CAP_IPC_OWNER> capability."
msgstr ""

# type: TP
#: shmop.2:199
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: shmop.2:208
msgid ""
"Invalid I<shmid> value, unaligned (i.e., not page-aligned and B<SHM_RND> was "
"not specified) or invalid I<shmaddr> value, or failing attach at B<brk>,"
msgstr ""

# type: Plain text
#: shmop.2:215
msgid "or B<SHM_REMAP> was specified and I<shmaddr> was B<NULL>."
msgstr ""

# type: TP
#: shmop.2:215
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

# type: Plain text
#: shmop.2:218
msgid "Could not allocate memory for the descriptor or for the page tables."
msgstr ""

# type: Plain text
#: shmop.2:226
msgid ""
"B<shmdt>()  can fail only if there is no shared memory segment attached at "
"I<shmaddr>; in this case, I<errno> will be set to B<EINVAL>."
msgstr ""

# type: SH
#: shmop.2:226
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: shmop.2:239
msgid ""
"Using B<shmat>()  with I<shmaddr> equal to B<NULL> is the preferred, "
"portable way of attaching a shared memory segment.  Be aware that the shared "
"memory segment attached in this way may be attached at different addresses "
"in different processes.  Therefore, any pointers maintained within the "
"shared memory must be made relative (typically to the starting address of "
"the segment), rather than absolute."
msgstr ""

# type: Plain text
#: shmop.2:244
msgid ""
"On Linux, it is possible to attach a shared memory segment even if it "
"already marked to be deleted.  However, POSIX.1-2001 does not specify this "
"behaviour and many other implementations do not support it."
msgstr ""

# type: Plain text
#: shmop.2:247
msgid "The following system parameter affects B<shmat>():"
msgstr ""

# type: Plain text
#: shmop.2:256
msgid ""
"B<SHMLBA> Segment low boundary address multiple.  Must be page aligned.  For "
"the current implementation the B<SHMLBA> value is B<PAGE_SIZE>."
msgstr ""

# type: Plain text
#: shmop.2:262
msgid ""
"The implementation places no intrinsic limit on the per-process maximum "
"number of shared memory segments (B<SHMSEG>)."
msgstr ""

# type: SH
#: shmop.2:262
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: shmop.2:276
msgid ""
"SVr4, SVID.  SVr4 documents an additional error condition EMFILE.  In "
"SVID-v4 the type of the I<shmaddr> argument was changed from B<char *> into "
"B<const void *>, and the returned type of I<shmat>() from B<char *> into "
"B<void *>.  (Linux libc4 and libc5 have the B<char *> prototypes; glibc2 has "
"B<void *>.)"
msgstr ""

# type: SH
#: shmop.2:276
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: shmop.2:282
msgid ""
"B<brk>(2), B<mmap>(2), B<shmctl>(2), B<shmget>(2), B<ipc>(5), "
"B<capabilities>(7)"
msgstr ""
