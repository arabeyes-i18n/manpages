# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:04+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: bdflush.2:28
#, no-wrap
msgid "BDFLUSH"
msgstr ""

# type: TH
#: bdflush.2:28
#, no-wrap
msgid "2004-06-17"
msgstr ""

# type: TH
#: bdflush.2:28
#, no-wrap
msgid "Linux 2.6.7"
msgstr ""

# type: TH
#: bdflush.2:28
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: bdflush.2:29
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: bdflush.2:31
msgid "bdflush - start, flush, or tune buffer-dirty-flush daemon"
msgstr ""

# type: SH
#: bdflush.2:31
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: bdflush.2:35
#, no-wrap
msgid ""
"B<int bdflush(int >I<func>B<, long *>I<address>B<);>\n"
"B<int bdflush(int >I<func>B<, long >I<data>B<);>\n"
msgstr ""

# type: SH
#: bdflush.2:36
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: bdflush.2:43
msgid ""
"B<bdflush> starts, flushes, or tunes the buffer-dirty-flush daemon.  Only a "
"privileged process (one with the B<CAP_SYS_ADMIN> capability) may call "
"B<bdflush>."
msgstr ""

# type: Plain text
#: bdflush.2:49
msgid ""
"If I<func> is negative or 0, and no daemon has been started, then B<bdflush> "
"enters the daemon code and never returns."
msgstr ""

# type: Plain text
#: bdflush.2:54
msgid "If I<func> is 1, some dirty buffers are written to disk."
msgstr ""

# type: Plain text
#: bdflush.2:63
msgid ""
"If I<func> is 2 or more and is even (low bit is 0), then I<address> is the "
"address of a long word, and the tuning parameter numbered (I<func>-2)/2 is "
"returned to the caller in that address."
msgstr ""

# type: Plain text
#: bdflush.2:72
msgid ""
"If I<func> is 3 or more and is odd (low bit is 1), then I<data> is a long "
"word, and the kernel sets tuning parameter numbered (I<func>-3)/2 to that "
"value."
msgstr ""

# type: Plain text
#: bdflush.2:76
msgid ""
"The set of parameters, their values, and their legal ranges are defined in "
"the kernel source file I<fs/buffer.c>."
msgstr ""

# type: SH
#: bdflush.2:76
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: bdflush.2:85
msgid ""
"If I<func> is negative or 0 and the daemon successfully starts, B<bdflush> "
"never returns.  Otherwise, the return value is 0 on success and -1 on "
"failure, with I<errno> set to indicate the error."
msgstr ""

# type: SH
#: bdflush.2:85
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: bdflush.2:86
#, no-wrap
msgid "B<EBUSY>"
msgstr ""

# type: Plain text
#: bdflush.2:90
msgid ""
"An attempt was made to enter the daemon code after another process has "
"already entered."
msgstr ""

# type: TP
#: bdflush.2:90
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

# type: Plain text
#: bdflush.2:94
msgid "I<address> points outside your accessible address space."
msgstr ""

# type: TP
#: bdflush.2:94
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: bdflush.2:98
msgid ""
"An attempt was made to read or write an invalid parameter number, or to "
"write an invalid value to a parameter."
msgstr ""

# type: TP
#: bdflush.2:98
#, no-wrap
msgid "B<EPERM>"
msgstr ""

# type: Plain text
#: bdflush.2:103
msgid "Caller does not have the B<CAP_SYS_ADMIN> capability."
msgstr ""

# type: SH
#: bdflush.2:103
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: bdflush.2:106
msgid ""
"B<bdflush> is Linux specific and should not be used in programs intended to "
"be portable."
msgstr ""

# type: SH
#: bdflush.2:106
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: bdflush.2:110
msgid "B<fsync>(2), B<sync>(2), B<sync>(8), B<update>(8)"
msgstr ""
