# Arabic translation of intro.2 manpage
# Copyright (C) 2005 Free Software Foundation, Inc.
# Khaled Hosny <dr.khaled.hosny@gmail.com>, 2005.
# , fuzzy
# 
# 
msgid ""
msgstr ""
"Project-Id-Version: intro.2 2.02\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-11-20 10:04+0200\n"
"PO-Revision-Date: 2005-12-01 07:06+0200\n"
"Last-Translator: Khaled Hosny <dr.khaled.hosny@gmail.com>\n"
"Language-Team: Arabic\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit"

# type: TH
#: intro.2:35
#, no-wrap
msgid "INTRO"
msgstr "مقدمة"

# type: TH
#: intro.2:35
#, no-wrap
msgid "1996-05-22"
msgstr "1996-05-22"

# type: TH
#: intro.2:35
#, no-wrap
msgid "Linux 1.2.13"
msgstr "لينكس 1.2.13"

# type: TH
#: intro.2:35
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "دليل مبرمجى لينكس"

# type: SH
#: intro.2:36
#, no-wrap
msgid "NAME"
msgstr "اﻹسم"

# type: Plain text
#: intro.2:38
msgid "intro, _syscall - Introduction to system calls"
msgstr "مقدمة, _syscall - مقدمة الى نداءات النظام"

# type: SH
#: intro.2:38
#, no-wrap
msgid "DESCRIPTION"
msgstr "الوصف"

# type: Plain text
#: intro.2:41
msgid ""
"This chapter describes the Linux system calls.  For a list of the 164 "
"syscalls present in Linux 2.0, see syscalls(2)."
msgstr "يصف هذا الفصل نداءات النظام فى لينكس.  للحصول على قائمة بالـ 164 syscalls الموجودة فى لينكس, انظر see·syscalls(2)."

# type: SS
#: intro.2:41
#, no-wrap
msgid "Calling Directly"
msgstr "النداء مباشرة"

# type: Plain text
#: intro.2:45
msgid ""
"In most cases, it is unnecessary to invoke a system call directly, but there "
"are times when the Standard C library does not implement a nice function "
"call for you."
msgstr "من غير الضرورى - فى أغلب الحالات - بدأ نداء نظام مباشرة, لكن أ حيانا لا تؤدى مكتبة C القياسية وظيفة نداء جيدة بالنسبة لك."

# type: SS
#: intro.2:45
#, no-wrap
msgid "Synopsis"
msgstr "الخلاصة"

# type: Plain text
#: intro.2:47
msgid "B<#include E<lt>linux/unistd.hE<gt>>"
msgstr "B<#include E<lt>linux/unistd.hE<gt>>"

# type: Plain text
#: intro.2:49
msgid "A _syscall macro"
msgstr "ماكر _syscall"

# type: Plain text
#: intro.2:51
msgid "desired system call"
msgstr "نداء النظام المطلوب"

# type: SS
#: intro.2:52
#, no-wrap
msgid "Setup"
msgstr "اﻹعداد"

# type: Plain text
#: intro.2:57
msgid ""
"The important thing to know about a system call is its prototype.  You need "
"to know how many arguments, their types, and the function return type.  "
"There are six macros that make the actual call into the system easier.  They "
"have the form:"
msgstr "الشئ الذى تهم معرفته عن نداء النظام هو نموذجه.  تحتاج ﻷن تعرف عدد المعاملات وأنواعها و أنواع رجع الوظائف.  هناك ست ماكرو تجعل النداء الحقيقى الى النظام أسهل.  ولهم النسق التالى:"

# type: Plain text
#: intro.2:60
msgid "_syscallI<X>(I<type>,I<name>,I<type1>,I<arg1>,I<type2>,I<arg2>,...)"
msgstr "_syscallI<X>(I<type>,I<name>,I<type1>,I<arg1>,I<type2>,I<arg2>,...)"

# type: Plain text
#: intro.2:64
msgid ""
"where I<X> is 0\\(en5, which are the number of arguments taken by the system "
"call"
msgstr ""
"where I<X> is 0\\(en5, which are the number of arguments taken by the system "
"call"

# type: Plain text
#: intro.2:66
msgid "I<type> is the return type of the system call"
msgstr "I<type> هو نوع المرتجع لنداء النظام"

# type: Plain text
#: intro.2:68
msgid "I<name> is the name of the system call"
msgstr "I<name> هو اسم نداء النظام"

# type: Plain text
#: intro.2:70
msgid "I<typeN> is the Nth argument's type"
msgstr "I<typeN> هو نوع المعامل رقم N"

# type: Plain text
#: intro.2:72
msgid "I<argN> is the name of the Nth argument"
msgstr "I<argN> هو اسم المعمل رقم N"

# type: Plain text
#: intro.2:78
msgid ""
"These macros create a function called I<name> with the arguments you "
"specify.  Once you include the _syscall() in your source file, you call the "
"system call by I<name>."
msgstr "هذه الماكروز تنشئ وظيفة تسمى I<name> ولها المعاملات التى تحددها.  بمجرد أن تدرج الـ _syscall() فى ملفك المصدرى, يمكنك أن تستدعى نداء النظام بواسطة I<name>."

# type: SH
#: intro.2:78
#, no-wrap
msgid "EXAMPLE"
msgstr "مثال"

# type: Plain text
#: intro.2:85
#, no-wrap
msgid ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>linux/unistd.hE<gt>\t/* for _syscallX macros/related stuff "
"*/\n"
"#include E<lt>linux/kernel.hE<gt>\t/* for struct sysinfo */\n"
msgstr ""
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>errno.hE<gt>\n"
"#include E<lt>linux/unistd.hE<gt>\t/* for _syscallX macros/related stuff "
"*/\n"
"#include E<lt>linux/kernel.hE<gt>\t/* for struct sysinfo */\n"

# type: Plain text
#: intro.2:87
#, no-wrap
msgid "_syscall1(int, sysinfo, struct sysinfo *, info);\n"
msgstr "_syscall1(int, sysinfo, struct sysinfo *, info);\n"

# type: Plain text
#: intro.2:90
#, no-wrap
msgid ""
"/* Note: if you copy directly from the nroff source, remember to\n"
"REMOVE the extra backslashes in the printf statement. */\n"
msgstr "/* لاحظ: اذا نسخت مباشرة من مصدر nroff, تذكر أن\n"
"تحذف الشرطات الزائدة فى جملة printf. */\n"

# type: Plain text
#: intro.2:95
#, no-wrap
msgid ""
"int main(void)\n"
"{\n"
"\tstruct sysinfo s_info;\n"
"\tint error;\n"
msgstr ""
"int main(void)\n"
"{\n"
"\tstruct sysinfo s_info;\n"
"\tint error;\n"

# type: Plain text
#: intro.2:110
#, no-wrap
msgid ""
"\terror = sysinfo(&s_info);\n"
"\tprintf(\"code error = %d\\en\", error);\n"
"        printf(\"Uptime = %lds\\enLoad: 1 min %lu / 5 min %lu / 15 min "
"%lu\\en\"\n"
"                \"RAM: total %lu / free %lu / shared %lu\\en\"\n"
"                \"Memory in buffers = %lu\\enSwap: total %lu / free "
"%lu\\en\"\n"
"                \"Number of processes = %d\\en\",\n"
"\t\ts_info.uptime, s_info.loads[0],\n"
"\t\ts_info.loads[1], s_info.loads[2],\n"
"\t\ts_info.totalram, s_info.freeram,\n"
"\t\ts_info.sharedram, s_info.bufferram,\n"
"\t\ts_info.totalswap, s_info.freeswap,\n"
"\t\ts_info.procs);\n"
"\treturn(0);\n"
"}\n"
msgstr ""
"\terror = sysinfo(&s_info);\n"
"\tprintf(\"code error = %d\\en\", error);\n"
"        printf(\"Uptime = %lds\\enLoad: 1 min %lu / 5 min %lu / 15 min "
"%lu\\en\"\n"
"                \"RAM: total %lu / free %lu / shared %lu\\en\"\n"
"                \"Memory in buffers = %lu\\enSwap: total %lu / free "
"%lu\\en\"\n"
"                \"Number of processes = %d\\en\",\n"
"\t\ts_info.uptime, s_info.loads[0],\n"
"\t\ts_info.loads[1], s_info.loads[2],\n"
"\t\ts_info.totalram, s_info.freeram,\n"
"\t\ts_info.sharedram, s_info.bufferram,\n"
"\t\ts_info.totalswap, s_info.freeswap,\n"
"\t\ts_info.procs);\n"
"\treturn(0);\n"
"}\n"

# type: SH
#: intro.2:111
#, no-wrap
msgid "Sample Output"
msgstr "عينة خرج"

# type: Plain text
#: intro.2:120
#, no-wrap
msgid ""
"code error = 0\n"
"uptime = 502034s\n"
"Load: 1 min 13376 / 5 min 5504 / 15 min 1152\n"
"RAM: total 15343616 / free 827392 / shared 8237056\n"
"Memory in buffers = 5066752\n"
"Swap: total 27881472 / free 24698880\n"
"Number of processes = 40\n"
msgstr ""
"code error = 0\n"
"uptime = 502034s\n"
"Load: 1 min 13376 / 5 min 5504 / 15 min 1152\n"
"RAM: total 15343616 / free 827392 / shared 8237056\n"
"Memory in buffers = 5066752\n"
"Swap: total 27881472 / free 24698880\n"
"Number of processes = 40\n"

# type: SH
#: intro.2:121
#, no-wrap
msgid "NOTES"
msgstr "ملاحظات"

# type: Plain text
#: intro.2:124
msgid ""
"The _syscall() macros DO NOT produce a prototype.  You may have to create "
"one, especially for C++ users."
msgstr "ماكروز الـ _syscall() لا تنتج نموذج. ربما تحتاج ﻹنشاء واحد, خاصة مستخدمى C++."

# type: Plain text
#: intro.2:134
msgid ""
"System calls are not required to return only positive or negative error "
"codes.  You need to read the source to be sure how it will return errors.  "
"Usually, it is the negative of a standard error code, e.g., -B<EPERM>.  The "
"_syscall() macros will return the result I<r> of the system call when I<r> "
"is nonnegative, but will return -1 and set the variable I<errno> to -I<r> "
"when I<r> is negative.  For the error codes, see B<errno>(3)."
msgstr "نداءات النظام ليست مطلوبة لمجرد ارتجاع رموز خطأ موجبة أو سالبة.  عليك أن تقرأ المصدر لتعرف كيف سترتجع اﻷخطاء."

# type: Plain text
#: intro.2:139
msgid ""
"Some system calls, such as B<mmap>, require more than five arguments.  These "
"are handled by pushing the arguments on the stack and passing a pointer to "
"the block of arguments."
msgstr ""

# type: Plain text
#: intro.2:142
msgid ""
"When defining a system call, the argument types MUST be passed by-value or "
"by-pointer (for aggregates like structs)."
msgstr ""

# type: Plain text
#: intro.2:146
msgid ""
"The preferred way to invoke system calls that glibc does not know about yet, "
"is via B<syscall>(2)."
msgstr ""

# type: SH
#: intro.2:146
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: intro.2:149
msgid ""
"Certain codes are used to indicate Unix variants and standards to which "
"calls in the section conform.  These are:"
msgstr ""

# type: TP
#: intro.2:149
#, no-wrap
msgid "SVr4"
msgstr ""

# type: Plain text
#: intro.2:154
msgid ""
"System V Release 4 Unix, as described in the \"Programmer's Reference "
"Manual: Operating System API (Intel processors)\" (Prentice-Hall 1992, ISBN "
"0-13-951294-2)"
msgstr ""

# type: TP
#: intro.2:154
#, no-wrap
msgid "SVID"
msgstr ""

# type: Plain text
#: intro.2:158
msgid ""
"System V Interface Definition, as described in \"The System V Interface "
"Definition, Fourth Edition\"."
msgstr ""

# type: TP
#: intro.2:158
#, no-wrap
msgid "POSIX.1"
msgstr ""

# type: Plain text
#: intro.2:164
msgid ""
"IEEE 1003.1-1990 part 1, aka ISO/IEC 9945-1:1990s, aka \"IEEE Portable "
"Operating System Interface for Computing Environments\", as elucidated in "
"Donald Lewine's \"POSIX Programmer's Guide\" (O'Reilly & Associates, Inc., "
"1991, ISBN 0-937175-73-0."
msgstr ""

# type: TP
#: intro.2:164
#, no-wrap
msgid "POSIX.1b"
msgstr ""

# type: Plain text
#: intro.2:170
msgid ""
"IEEE Std 1003.1b-1993 (POSIX.1b standard) describing real-time facilities "
"for portable operating systems, aka ISO/IEC 9945-1:1996, as elucidated in "
"\"Programming for the real world - POSIX.4\" by Bill O. Gallmeister "
"(O'Reilly & Associates, Inc. ISBN 1-56592-074-0)."
msgstr ""

# type: TP
#: intro.2:170
#, no-wrap
msgid "SUS, SUSv2"
msgstr ""

# type: Plain text
#: intro.2:175
msgid ""
"Single Unix Specification.  (Developed by X/Open and The Open Group. See "
"also http://www.UNIX-systems.org/version2/ .)"
msgstr ""

# type: TP
#: intro.2:175
#, no-wrap
msgid "4.3BSD/4.4BSD"
msgstr ""

# type: Plain text
#: intro.2:179
msgid ""
"The 4.3 and 4.4 distributions of Berkeley Unix.  4.4BSD was "
"upward-compatible from 4.3."
msgstr ""

# type: TP
#: intro.2:179
#, no-wrap
msgid "V7"
msgstr ""

# type: Plain text
#: intro.2:182
msgid "Version 7, the ancestral Unix from Bell Labs."
msgstr ""

# type: SH
#: intro.2:182
#, no-wrap
msgid "FILES"
msgstr ""

# type: Plain text
#: intro.2:184
msgid "I</usr/include/linux/unistd.h>"
msgstr ""

# type: SH
#: intro.2:184
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: intro.2:186
msgid "B<syscall>(2), B<errno>(3)"
msgstr ""

