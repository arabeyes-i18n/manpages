# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: socketpair.2:39
#, no-wrap
msgid "SOCKETPAIR"
msgstr ""

# type: TH
#: socketpair.2:39
#, no-wrap
msgid "2004-06-17"
msgstr ""

# type: TH
#: socketpair.2:39
#, no-wrap
msgid "Linux 2.6.7"
msgstr ""

# type: TH
#: socketpair.2:39
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: socketpair.2:40
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: socketpair.2:42
msgid "socketpair - create a pair of connected sockets"
msgstr ""

# type: SH
#: socketpair.2:42
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: socketpair.2:44
msgid "B<#include E<lt>sys/types.hE<gt>>"
msgstr ""

# type: Plain text
#: socketpair.2:46
msgid "B<#include E<lt>sys/socket.hE<gt>>"
msgstr ""

# type: Plain text
#: socketpair.2:48
msgid ""
"B<int socketpair(int >I<d>B<, int >I<type>B<, int >I<protocol>B<, int "
">I<sv>B<[2]);>"
msgstr ""

# type: SH
#: socketpair.2:48
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: socketpair.2:63
msgid ""
"The I<socketpair> call creates an unnamed pair of connected sockets in the "
"specified domain I<d>, of the specified I<type>, and using the optionally "
"specified I<protocol>.  The descriptors used in referencing the new sockets "
"are returned in I<sv>[0] and I<sv>[1].  The two sockets are "
"indistinguishable."
msgstr ""

# type: SH
#: socketpair.2:63
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: socketpair.2:67
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""

# type: SH
#: socketpair.2:67
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: socketpair.2:68
#, no-wrap
msgid "B<EAFNOSUPPORT>"
msgstr ""

# type: Plain text
#: socketpair.2:71
msgid "The specified address family is not supported on this machine."
msgstr ""

# type: TP
#: socketpair.2:71
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

# type: Plain text
#: socketpair.2:76
msgid ""
"The address I<sv> does not specify a valid part of the process address "
"space."
msgstr ""

# type: TP
#: socketpair.2:76
#, no-wrap
msgid "B<EMFILE>"
msgstr ""

# type: Plain text
#: socketpair.2:79
msgid "Too many descriptors are in use by this process."
msgstr ""

# type: TP
#: socketpair.2:79
#, no-wrap
msgid "B<ENFILE>"
msgstr ""

# type: Plain text
#: socketpair.2:82
msgid "The system limit on the total number of open files has been reached."
msgstr ""

# type: TP
#: socketpair.2:82
#, no-wrap
msgid "B<EOPNOTSUPP>"
msgstr ""

# type: Plain text
#: socketpair.2:85
msgid "The specified protocol does not support creation of socket pairs."
msgstr ""

# type: TP
#: socketpair.2:85
#, no-wrap
msgid "B<EPROTONOSUPPORT>"
msgstr ""

# type: Plain text
#: socketpair.2:88
msgid "The specified protocol is not supported on this machine."
msgstr ""

# type: SH
#: socketpair.2:88
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: socketpair.2:95
msgid ""
"4.4BSD, SUSv2, POSIX 1003.1-2001.  The B<socketpair> function call appeared "
"in 4.2BSD. It is generally portable to/from non-BSD systems supporting "
"clones of the BSD socket layer (including System V variants)."
msgstr ""

# type: SH
#: socketpair.2:95
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: socketpair.2:101
msgid ""
"On Linux, the only supported domain for this call is B<AF_UNIX> (or "
"synonymously, B<AF_LOCAL>).  (Most implementations have the same "
"restriction.)"
msgstr ""

# type: SH
#: socketpair.2:101
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: socketpair.2:106
msgid "B<pipe>(2), B<read>(2), B<socket>(2), B<write>(2), B<unix>(7)"
msgstr ""
