# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:05+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: setxattr.2:27
#, no-wrap
msgid "SETXATTR"
msgstr ""

# type: TH
#: setxattr.2:27
#, no-wrap
msgid "Extended Attributes"
msgstr ""

# type: TH
#: setxattr.2:27
#, no-wrap
msgid "Dec 2001"
msgstr ""

# type: TH
#: setxattr.2:27
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: setxattr.2:28
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: setxattr.2:30
msgid "setxattr, lsetxattr, fsetxattr - set an extended attribute value"
msgstr ""

# type: SH
#: setxattr.2:30
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: setxattr.2:35
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>attr/xattr.hE<gt>>\n"
msgstr ""

# type: Plain text
#: setxattr.2:42
#, no-wrap
msgid ""
"B<int setxattr (const char\\ *>I<path>B<, const char\\ *>I<name>B<,>\n"
"B<\\t\\t\\t const void\\ *>I<value>B<, size_t >I<size>B<, int "
">I<flags>B<);>\n"
"B<int lsetxattr (const char\\ *>I<path>B<, const char\\ *>I<name>B<,>\n"
"B<\\t\\t\\t const void\\ *>I<value>B<, size_t >I<size>B<, int "
">I<flags>B<);>\n"
"B<int fsetxattr (int >I<filedes>B<, const char\\ *>I<name>B<,>\n"
"B<\\t\\t\\t const void\\ *>I<value>B<, size_t >I<size>B<, int "
">I<flags>B<);>\n"
msgstr ""

# type: SH
#: setxattr.2:44
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: setxattr.2:55
msgid ""
"Extended attributes are I<name>:I<value> pairs associated with inodes "
"(files, directories, symlinks, etc).  They are extensions to the normal "
"attributes which are associated with all inodes in the system (i.e. the "
"B<stat>(2)  data).  A complete overview of extended attributes concepts can "
"be found in B<attr>(5)."
msgstr ""

# type: Plain text
#: setxattr.2:69
msgid ""
"B<setxattr> sets the I<value> of the extended attribute identified by "
"I<name> and associated with the given I<path> in the filesystem.  The "
"I<size> of the I<value> must be specified."
msgstr ""

# type: Plain text
#: setxattr.2:75
msgid ""
"B<lsetxattr> is identical to B<setxattr>, except in the case of a symbolic "
"link, where the extended attribute is set on the link itself, not the file "
"that it refers to."
msgstr ""

# type: Plain text
#: setxattr.2:85
msgid ""
"B<fsetxattr> is identical to B<setxattr>, only the extended attribute is set "
"on the open file pointed to by I<filedes> (as returned by B<open>(2))  in "
"place of I<path>."
msgstr ""

# type: Plain text
#: setxattr.2:95
msgid ""
"An extended attribute name is a simple NULL-terminated string.  The I<name> "
"includes a namespace prefix - there may be several, disjoint namespaces "
"associated with an individual inode.  The I<value> of an extended attribute "
"is a chunk of arbitrary textual or binary data of specified length."
msgstr ""

# type: Plain text
#: setxattr.2:105
msgid ""
"The I<flags> parameter can be used to refine the semantics of the "
"operation.  XATTR_CREATE specifies a pure create, which fails if the named "
"attribute exists already.  XATTR_REPLACE specifies a pure replace operation, "
"which fails if the named attribute does not already exist.  By default (no "
"flags), the extended attribute will be created if need be, or will simply "
"replace the value if the attribute exists."
msgstr ""

# type: SH
#: setxattr.2:105
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: setxattr.2:110
msgid ""
"On success, zero is returned.  On failure, -1 is returned and I<errno> is "
"set appropriately."
msgstr ""

# type: Plain text
#: setxattr.2:117
msgid ""
"If XATTR_CREATE is specified, and the attribute exists already, I<errno> is "
"set to EEXIST.  If XATTR_REPLACE is specified, and the attribute does not "
"exist, I<errno> is set to ENOATTR."
msgstr ""

# type: Plain text
#: setxattr.2:121
msgid ""
"If there is insufficient space remaining to store the extended attribute, "
"I<errno> is set to either ENOSPC, or EDQUOT if quota enforcement was the "
"cause."
msgstr ""

# type: Plain text
#: setxattr.2:125
msgid ""
"If extended attributes are not supported by the filesystem, or are disabled, "
"I<errno> is set to ENOTSUP."
msgstr ""

# type: Plain text
#: setxattr.2:129
msgid ""
"The errors documented for the B<stat>(2)  system call are also applicable "
"here."
msgstr ""

# type: SH
#: setxattr.2:129
#, no-wrap
msgid "AUTHORS"
msgstr ""

# type: Plain text
#: setxattr.2:135
msgid ""
"Andreas Gruenbacher, E<lt>I<a.gruenbacher@computer.org>E<gt> and the SGI XFS "
"development team, E<lt>I<linux-xfs@oss.sgi.com>E<gt>.  Please send any bug "
"reports or comments to these addresses."
msgstr ""

# type: SH
#: setxattr.2:135
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: setxattr.2:143
msgid ""
"B<getfattr>(1), B<setfattr>(1), B<getxattr>(2), B<listxattr>(2), B<open>(2), "
"B<removexattr>(2), B<stat>(2), B<attr>(5)"
msgstr ""
