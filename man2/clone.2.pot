# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:04+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: clone.2:33
#, no-wrap
msgid "CLONE"
msgstr ""

# type: TH
#: clone.2:33
#, no-wrap
msgid "2004-09-10"
msgstr ""

# type: TH
#: clone.2:33
#, no-wrap
msgid "Linux 2.6"
msgstr ""

# type: TH
#: clone.2:33
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: clone.2:34
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: clone.2:36
msgid "clone - create a child process"
msgstr ""

# type: SH
#: clone.2:36
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: clone.2:38
msgid "B<#include E<lt>sched.hE<gt>>"
msgstr ""

# type: Plain text
#: clone.2:40
msgid ""
"B<int clone(int (*>I<fn>B<)(void *), void *>I<child_stack>B<, int "
">I<flags>B<, void *>I<arg>B<);>"
msgstr ""

# type: Plain text
#: clone.2:42
msgid "B<_syscall2(int, >I<clone>B<, int, >I<flags>B<, void *, >I<child_stack>B<)>"
msgstr ""

# type: Plain text
#: clone.2:44
msgid "B<_syscall5(int, >I<clone>B<, int, >I<flags>B<, void *, >I<child_stack>B<,>"
msgstr ""

# type: Plain text
#: clone.2:46
msgid "B< int *, >I<parent_tidptr>B<, struct user_desc *, >I<newtls>B<,>"
msgstr ""

# type: Plain text
#: clone.2:48
msgid "B< int *, >I<child_tidptr>B<)>"
msgstr ""

# type: SH
#: clone.2:48
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: clone.2:61
msgid ""
"B<clone> creates a new process, just like B<fork>(2).  B<clone> is a library "
"function layered on top of the underlying B<clone> system call, hereinafter "
"referred to as B<sys_clone>.  A description of B<sys_clone> is given towards "
"the end of this page."
msgstr ""

# type: Plain text
#: clone.2:72
msgid ""
"Unlike B<fork>(2), these calls allow the child process to share parts of its "
"execution context with the calling process, such as the memory space, the "
"table of file descriptors, and the table of signal handlers.  (Note that on "
"this manual page, \"calling process\" normally corresponds to \"parent "
"process\".  But see the description of B<CLONE_PARENT> below.)"
msgstr ""

# type: Plain text
#: clone.2:77
msgid ""
"The main use of B<clone> is to implement threads: multiple threads of "
"control in a program that run concurrently in a shared memory space."
msgstr ""

# type: Plain text
#: clone.2:98
msgid ""
"When the child process is created with B<clone>, it executes the function "
"application I<fn>(I<arg>).  (This differs from B<fork>(2), where execution "
"continues in the child from the point of the B<fork>(2)  call.)  The I<fn> "
"argument is a pointer to a function that is called by the child process at "
"the beginning of its execution.  The I<arg> argument is passed to the I<fn> "
"function."
msgstr ""

# type: Plain text
#: clone.2:108
msgid ""
"When the I<fn>(I<arg>)  function application returns, the child process "
"terminates.  The integer returned by I<fn> is the exit code for the child "
"process.  The child process may also terminate explicitly by calling "
"B<exit>(2)  or after receiving a fatal signal."
msgstr ""

# type: Plain text
#: clone.2:123
msgid ""
"The I<child_stack> argument specifies the location of the stack used by the "
"child process.  Since the child and calling process may share memory, it is "
"not possible for the child process to execute in the same stack as the "
"calling process.  The calling process must therefore set up memory space for "
"the child stack and pass a pointer to this space to B<clone>.  Stacks grow "
"downwards on all processors that run Linux (except the HP PA processors), so "
"I<child_stack> usually points to the topmost address of the memory space set "
"up for the child stack."
msgstr ""

# type: Plain text
#: clone.2:137
msgid ""
"The low byte of I<flags> contains the number of the signal sent to the "
"parent when the child dies.  If this signal is specified as anything other "
"than B<SIGCHLD>, then the parent process must specify the B<__WALL> or "
"B<__WCLONE> options when waiting for the child with B<wait>(2).  If no "
"signal is specified, then the parent process is not signaled when the child "
"terminates."
msgstr ""

# type: Plain text
#: clone.2:142
msgid ""
"I<flags> may also be bitwise-or'ed with one or several of the following "
"constants, in order to specify what is shared between the calling process "
"and the child process:"
msgstr ""

# type: TP
#: clone.2:143
#, no-wrap
msgid "B<CLONE_PARENT> (since Linux 2.3.12)"
msgstr ""

# type: Plain text
#: clone.2:150
msgid ""
"If B<CLONE_PARENT> is set, then the parent of the new child (as returned by "
"B<getppid>(2))  will be the same as that of the calling process."
msgstr ""

# type: Plain text
#: clone.2:156
msgid ""
"If B<CLONE_PARENT> is not set, then (as with B<fork>(2))  the child's parent "
"is the calling process."
msgstr ""

# type: Plain text
#: clone.2:164
msgid ""
"Note that it is the parent process, as returned by B<getppid>(2), which is "
"signaled when the child terminates, so that if B<CLONE_PARENT> is set, then "
"the parent of the calling process, rather than the calling process itself, "
"will be signaled."
msgstr ""

# type: TP
#: clone.2:165
#, no-wrap
msgid "B<CLONE_FS>"
msgstr ""

# type: Plain text
#: clone.2:178
msgid ""
"If B<CLONE_FS> is set, the caller and the child processes share the same "
"file system information.  This includes the root of the file system, the "
"current working directory, and the umask.  Any call to B<chroot>(2), "
"B<chdir>(2), or B<umask>(2)  performed by the calling process or the child "
"process also takes effect in the other process."
msgstr ""

# type: Plain text
#: clone.2:190
msgid ""
"If B<CLONE_FS> is not set, the child process works on a copy of the file "
"system information of the calling process at the time of the B<clone> call.  "
"Calls to B<chroot>(2), B<chdir>(2), B<umask>(2)  performed later by one of "
"the processes do not affect the other process."
msgstr ""

# type: TP
#: clone.2:191
#, no-wrap
msgid "B<CLONE_FILES>"
msgstr ""

# type: Plain text
#: clone.2:201
msgid ""
"If B<CLONE_FILES> is set, the calling process and the child processes share "
"the same file descriptor table.  File descriptors always refer to the same "
"files in the calling process and in the child process.  Any file descriptor "
"created by the calling process or by the child process is also valid in the "
"other process.  Similarly, if one of the processes closes a file descriptor, "
"or changes its associated flags, the other process is also affected."
msgstr ""

# type: Plain text
#: clone.2:209
msgid ""
"If B<CLONE_FILES> is not set, the child process inherits a copy of all file "
"descriptors opened in the calling process at the time of B<clone>.  "
"Operations on file descriptors performed later by either the calling process "
"or the child process do not affect the other process."
msgstr ""

# type: TP
#: clone.2:210
#, no-wrap
msgid "B<CLONE_NEWNS> (since Linux 2.4.19)"
msgstr ""

# type: Plain text
#: clone.2:213
msgid "Start the child in a new namespace."
msgstr ""

# type: Plain text
#: clone.2:231
msgid ""
"Every process lives in a namespace. The I<namespace> of a process is the "
"data (the set of mounts) describing the file hierarchy as seen by that "
"process. After a B<fork>(2)  or B<clone>(2)  where the B<CLONE_NEWNS> flag "
"is not set, the child lives in the same namespace as the parent.  The system "
"calls B<mount>(2)  and B<umount>(2)  change the namespace of the calling "
"process, and hence affect all processes that live in the same namespace, but "
"do not affect processes in a different namespace."
msgstr ""

# type: Plain text
#: clone.2:238
msgid ""
"After a B<clone>(2)  where the B<CLONE_NEWNS> flag is set, the cloned child "
"is started in a new namespace, initialized with a copy of the namespace of "
"the parent."
msgstr ""

# type: Plain text
#: clone.2:250
msgid ""
"Only a privileged process (one having the CAP_SYS_ADMIN capability)  may "
"specify the B<CLONE_NEWNS> flag.  It is not permitted to specify both "
"B<CLONE_NEWNS> and B<CLONE_FS> in the same B<clone> call."
msgstr ""

# type: TP
#: clone.2:251
#, no-wrap
msgid "B<CLONE_SIGHAND>"
msgstr ""

# type: Plain text
#: clone.2:264
msgid ""
"If B<CLONE_SIGHAND> is set, the calling process and the child processes "
"share the same table of signal handlers.  If the calling process or child "
"process calls B<sigaction>(2)  to change the behavior associated with a "
"signal, the behavior is changed in the other process as well.  However, the "
"calling process and child processes still have distinct signal masks and "
"sets of pending signals.  So, one of them may block or unblock some signals "
"using B<sigprocmask>(2)  without affecting the other process."
msgstr ""

# type: Plain text
#: clone.2:274
msgid ""
"If B<CLONE_SIGHAND> is not set, the child process inherits a copy of the "
"signal handlers of the calling process at the time B<clone> is called.  "
"Calls to B<sigaction>(2)  performed later by one of the processes have no "
"effect on the other process."
msgstr ""

# type: TP
#: clone.2:275
#, no-wrap
msgid "B<CLONE_PTRACE>"
msgstr ""

# type: Plain text
#: clone.2:281
msgid ""
"If B<CLONE_PTRACE> is specified, and the calling process is being traced, "
"then trace the child also (see B<ptrace>(2))."
msgstr ""

# type: TP
#: clone.2:282
#, no-wrap
msgid "B<CLONE_VFORK>"
msgstr ""

# type: Plain text
#: clone.2:294
msgid ""
"If B<CLONE_VFORK> is set, the execution of the calling process is suspended "
"until the child releases its virtual memory resources via a call to "
"B<execve>(2)  or B<_exit>(2)  (as with B<vfork>(2))."
msgstr ""

# type: Plain text
#: clone.2:300
msgid ""
"If B<CLONE_VFORK> is not set then both the calling process and the child are "
"schedulable after the call, and an application should not rely on execution "
"occurring in any particular order."
msgstr ""

# type: TP
#: clone.2:301
#, no-wrap
msgid "B<CLONE_VM>"
msgstr ""

# type: Plain text
#: clone.2:313
msgid ""
"If B<CLONE_VM> is set, the calling process and the child processes run in "
"the same memory space.  In particular, memory writes performed by the "
"calling process or by the child process are also visible in the other "
"process.  Moreover, any memory mapping or unmapping performed with "
"B<mmap>(2)  or B<munmap>(2)  by the child or calling process also affects "
"the other process."
msgstr ""

# type: Plain text
#: clone.2:322
msgid ""
"If B<CLONE_VM> is not set, the child process runs in a separate copy of the "
"memory space of the calling process at the time of B<clone>.  Memory writes "
"or file mappings/unmappings performed by one of the processes do not affect "
"the other, as with B<fork>(2)."
msgstr ""

# type: TP
#: clone.2:323
#, no-wrap
msgid "B<CLONE_PID> (obsolete)"
msgstr ""

# type: Plain text
#: clone.2:332
msgid ""
"If B<CLONE_PID> is set, the child process is created with the same process "
"ID as the calling process. This is good for hacking the system, but "
"otherwise of not much use. Since 2.3.21 this flag can be specified only by "
"the system boot process (PID 0).  It disappeared in Linux 2.5.16."
msgstr ""

# type: TP
#: clone.2:333
#, no-wrap
msgid "B<CLONE_THREAD> (since Linux 2.4.0-test8)"
msgstr ""

# type: Plain text
#: clone.2:338
msgid ""
"If B<CLONE_THREAD> is set, the child is placed in the same thread group as "
"the calling process."
msgstr ""

# type: Plain text
#: clone.2:346
msgid ""
"If B<CLONE_THREAD> is not set, then the child is placed in its own (new)  "
"thread group, whose ID is the same as the process ID."
msgstr ""

# type: Plain text
#: clone.2:352
msgid ""
"(Thread groups are feature added in Linux 2.4 to support the POSIX threads "
"notion of a set of threads sharing a single PID.  In Linux since 2.4, calls "
"to B<getpid>(2)  return the thread group ID of the caller.)"
msgstr ""

# type: TP
#: clone.2:353
#, no-wrap
msgid "B<CLONE_SETTLS> (since Linux 2.5.32)"
msgstr ""

# type: Plain text
#: clone.2:360
msgid ""
"The I<newtls> parameter is the new TLS (Thread Local Storage) descriptor.  "
"(See B<set_thread_area>(2).)"
msgstr ""

# type: TP
#: clone.2:361
#, no-wrap
msgid "B<CLONE_PARENT_SETTID> (since Linux 2.5.49)"
msgstr ""

# type: Plain text
#: clone.2:367
msgid ""
"Store child thread ID at location I<parent_tidptr> in parent and child "
"memory.  (In Linux 2.5.32-2.5.48 there was a flag CLONE_SETTID that did "
"this.)"
msgstr ""

# type: TP
#: clone.2:368
#, no-wrap
msgid "B<CLONE_CHILD_SETTID> (since Linux 2.5.49)"
msgstr ""

# type: Plain text
#: clone.2:373
msgid "Store child thread ID at location I<child_tidptr> in child memory."
msgstr ""

# type: TP
#: clone.2:374
#, no-wrap
msgid "B<CLONE_CHILD_CLEARTID> (since Linux 2.5.49)"
msgstr ""

# type: Plain text
#: clone.2:383
msgid ""
"Erase child thread ID at location I<child_tidptr> in child memory when the "
"child exits, and do a wakeup on the futex at that address.  The address "
"involved may be changed by the B<set_tid_address>(2)  system call. This is "
"used by threading libraries."
msgstr ""

# type: SS
#: clone.2:385
#, no-wrap
msgid "sys_clone"
msgstr ""

# type: Plain text
#: clone.2:401
msgid ""
"The B<sys_clone> system call corresponds more closely to B<fork>(2)  in that "
"execution in the child continues from the point of the call.  Thus, "
"B<sys_clone> only requires the I<flags> and I<child_stack> arguments, which "
"have the same meaning as for B<clone>.  (Note that the order of these "
"arguments differs from B<clone>.)"
msgstr ""

# type: Plain text
#: clone.2:411
msgid ""
"Another difference for B<sys_clone> is that the I<child_stack> argument may "
"be zero, in which case copy-on-write semantics ensure that the child gets "
"separate copies of stack pages when either process modifies the stack.  In "
"this case, for correct operation, the B<CLONE_VM> option should not be "
"specified."
msgstr ""

# type: Plain text
#: clone.2:421
msgid ""
"Since Linux 2.5.49 the system call has five parameters.  The two new "
"parameters are I<parent_tidptr> which points to the location (in parent and "
"child memory) where the parent thread ID will be written in case "
"CLONE_PARENT_SETTID was specified, and I<child_tidptr> which points to the "
"location (in child memory) where the child thread ID will be written in case "
"CLONE_CHILD_SETTID was specified."
msgstr ""

# type: SH
#: clone.2:422
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: clone.2:430
msgid ""
"On success, the thread ID of the child process is returned in the caller's "
"thread of execution.  On failure, a -1 will be returned in the caller's "
"context, no child process will be created, and I<errno> will be set "
"appropriately."
msgstr ""

# type: SH
#: clone.2:431
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: clone.2:432
#, no-wrap
msgid "B<EAGAIN>"
msgstr ""

# type: Plain text
#: clone.2:435
msgid "Too many processes are already running."
msgstr ""

# type: TP
#: clone.2:435 clone.2:441 clone.2:447 clone.2:454 clone.2:462
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: clone.2:441
msgid ""
"B<CLONE_SIGHAND> was specified, but B<CLONE_VM> was not. (Since Linux "
"2.6.0-test6.)"
msgstr ""

# type: Plain text
#: clone.2:447
msgid ""
"B<CLONE_THREAD> was specified, but B<CLONE_SIGHAND> was not. (Since Linux "
"2.5.35.)"
msgstr ""

# type: Plain text
#: clone.2:454
msgid ""
"Precisely one of B<CLONE_DETACHED> and B<CLONE_THREAD> was specified. (Since "
"Linux 2.6.0-test6.)"
msgstr ""

# type: Plain text
#: clone.2:462
msgid "Both B<CLONE_FS> and B<CLONE_NEWNS> were specified in I<flags>."
msgstr ""

# type: Plain text
#: clone.2:468
msgid "Returned by B<clone> when a zero value is specified for I<child_stack>."
msgstr ""

# type: TP
#: clone.2:468
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

# type: Plain text
#: clone.2:473
msgid ""
"Cannot allocate sufficient memory to allocate a task structure for the "
"child, or to copy those parts of the caller's context that need to be "
"copied."
msgstr ""

# type: TP
#: clone.2:473 clone.2:477
#, no-wrap
msgid "B<EPERM>"
msgstr ""

# type: Plain text
#: clone.2:477
msgid ""
"B<CLONE_NEWNS> was specified by a non-root process (process without "
"CAP_SYS_ADMIN)."
msgstr ""

# type: Plain text
#: clone.2:481
msgid "B<CLONE_PID> was specified by a process other than process 0."
msgstr ""

# type: SH
#: clone.2:482
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

# type: Plain text
#: clone.2:488
msgid ""
"There is no entry for B<clone> in libc5.  glibc2 provides B<clone> as "
"described in this manual page."
msgstr ""

# type: SH
#: clone.2:489
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: clone.2:492
msgid ""
"For kernel versions 2.4.7-2.4.18 the CLONE_THREAD flag implied the "
"CLONE_PARENT flag."
msgstr ""

# type: SH
#: clone.2:493
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: clone.2:500
msgid ""
"The B<clone> and B<sys_clone> calls are Linux-specific and should not be "
"used in programs intended to be portable."
msgstr ""

# type: SH
#: clone.2:501
#, no-wrap
msgid "BUGS"
msgstr ""

# type: Plain text
#: clone.2:512
msgid ""
"Versions of the GNU C library that include the NPTL threading library "
"contain a wrapper function for B<getpid>()  that performs caching of PIDs.  "
"In programs linked against such libraries, calls to B<getpid>()  may return "
"the same value, even when the threads were not created using B<CLONE_THREAD> "
"(and thus are not in the same thread group).  To get the truth, it may be "
"necessary to use code such as the following"
msgstr ""

# type: Plain text
#: clone.2:515
#, no-wrap
msgid "    #include E<lt>syscall.hE<gt>\n"
msgstr ""

# type: Plain text
#: clone.2:517
#, no-wrap
msgid "    pid_t mypid;\n"
msgstr ""

# type: Plain text
#: clone.2:519
#, no-wrap
msgid "    mypid = syscall(SYS_getpid);\n"
msgstr ""

# type: SH
#: clone.2:521
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: clone.2:530
msgid ""
"B<fork>(2), B<futex>(2), B<getpid>(2), B<gettid>(2), B<set_thread_area>(2), "
"B<set_tid_address>(2), B<tkill>(2), B<wait>(2), B<capabilities>(7)"
msgstr ""
