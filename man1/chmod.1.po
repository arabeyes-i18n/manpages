# translation of chmod.1.po to Arabic
# SOME DESCRIPTIVE TITLE
# Copyright (C) 2006 Free Software Foundation, Inc.
#
# Khaled Hosny <dr.khaled.hosny@gmail.com>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: chmod.1\n"
"POT-Creation-Date: 2005-11-20 10:03+0200\n"
"PO-Revision-Date: 2006-07-09 23:19+0300\n"
"Last-Translator: Khaled Hosny <dr.khaled.hosny@gmail.com>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11\n"

# type: TH
#: chmod.1:9
#, no-wrap
msgid "CHMOD"
msgstr "CHMOD"

# type: TH
#: chmod.1:9
#, no-wrap
msgid "2004-06-17"
msgstr "2004-06-17"

# type: TH
#: chmod.1:9
#, no-wrap
msgid "GNU fileutils 4.0"
msgstr "GNU fileutils 4.0"

# type: SH
#: chmod.1:10
#, no-wrap
msgid "NAME"
msgstr "اﻹسم"

# type: Plain text
#: chmod.1:12
msgid "chmod - change access permissions of files"
msgstr "chmod - غَيِّر صلاحيات الوصول للملفات"

# type: SH
#: chmod.1:12
#, no-wrap
msgid "SYNOPSIS"
msgstr "الخلاصة"

# type: Plain text
#: chmod.1:14
msgid "B<chmod [>I<options>B<] >I<mode file...>"
msgstr "B<chmod [>I<options>B<] >I<mode file...>"

# type: Plain text
#: chmod.1:17
msgid "POSIX options: B<[-R] [--]>"
msgstr ""
"خيارات POSIX:"
"B<[-R] [--]>"

# type: Plain text
#: chmod.1:20
msgid "GNU mode denotation: B<[--reference=>I<rfile>B<]>"
msgstr ""
"دلالة جنو للأنماط:"
"B<[--reference=>I<rfile>B<]>"

# type: Plain text
#: chmod.1:24
msgid "GNU options (shortest form): B<[-cfvR]> B<[--help] [--version] [--]>"
msgstr ""
"خيارات جنو (الصورة الأقصر): "
"B<[-cfvR]>·B<[--help]·[--version]·[--]>"

# type: SH
#: chmod.1:24
#, no-wrap
msgid "DESCRIPTION"
msgstr "الوصف"

# type: Plain text
#: chmod.1:32
msgid ""
"B<chmod> changes the permissions of each given I<file> according to I<mode>, "
"which can be either a symbolic representation of changes to make, or an "
"octal number representing the bit pattern for the new permissions."
msgstr "يغير B<chmod> الصلاحيات لكل ملف I<file> معطى طبقا للنمط I<mode>، والذى يمكن أن يكون اما تعبيرا رمزيا عن التغييرات المطلةب عملها، أو رقم ثمانى يمثل نمط البت \"bit\" للصلاحيات الجديدة."

# type: Plain text
#: chmod.1:34
msgid "The format of a symbolic mode change argument is"
msgstr "تنسيق معامل تغيير النمط الرمزى هو"

# type: Plain text
#: chmod.1:36
msgid "\\&`[ugoa...][[+-=][rwxXstugo...]...][,...]'."
msgstr "\\&`[ugoa...][[+-=][rwxXstugo...]...][,...]'."

# type: Plain text
#: chmod.1:46
msgid ""
"Such an argument is a list of symbolic mode change commands, separated by "
"commas.  Each symbolic mode change command starts with zero or more of the "
"letters `ugoa'; these control which users' access to the file will be "
"changed: the user who owns it (u), other users in the file's group (g), "
"other users not in the file's group (o), or all users (a).  Thus, `a' is "
"here equivalent to `ugo'.  If none of these are given, the effect is as if "
"`a' were given, but bits that are set in the umask are not affected."
msgstr "هذا المعامل هو قائمة بأوامر تغيير النمط الرمزية، مفصولة بفاصلات `,`. كل أمر تغيير نمط رمزى يبدأ بلا حرف أو أكثر من الحروف `ugoa'؛ مالك هذا الملف (u)، باقى المستخدمين فى مجموعة الملف (g)، باقى المستخدمين من خارج مجموعة الملف (o)، أو كل المستخدمين (a).  لذا، فأن `a' مساوية لـ `ugo'.  اذا لم يعطى أى من هذه الحروف، فسيكون التأثير كما لو أعطى الحرف `a'، لكن البتات `bits` المضبوطة فى الـ `umask` لن تتأثر."

# type: Plain text
#: chmod.1:50
msgid ""
"The operator `+' causes the permissions selected to be added to the existing "
"permissions of each file; `-' causes them to be removed; and `=' causes them "
"to be the only permissions that the file has."
msgstr "تؤدى العلامة `+` ﻹضافة الصلاحيات المختارة الى الصلاحيات الموجودة بالفعل لكل ملف؛ تؤدى `-` لحذفهم؛ وتؤدى `=` لجعلهم الصلاحيات الوحيدة لهذا الملف."

# type: Plain text
#: chmod.1:62
msgid ""
"The letters `rwxXstugo' select the new permissions for the affected users: "
"read (r), write (w), execute (or access for directories) (x), execute only "
"if the file is a directory or already has execute permission for some user "
"(X), set user or group ID on execution (s), sticky bit (t), the permissions "
"that the user who owns the file currently has for it (u), the permissions "
"that other users in the file's group have for it (g), and the permissions "
"that other users not in the file's group have for it (o).  (Thus, `chmod g-s "
"file' removes the set-group-ID (sgid) bit, \\&`chmod ug+s file' sets both "
"the suid and sgid bits, while \\&`chmod o+s file' does nothing.)"
msgstr ""
"تختار الحروف `rwxXstugo' الصلاحيات الجديدة للمستخدمين المتأثرين: "
"القراءة (r)، الكتابة (w)، التنفيذ (أو الوصول بالنسبة للأدلة) (x)، نفذ فقط اذا كان الملف دليلا أو لديه صلاحية التنفيذ لبعض المستخدين بالفعل (X)، اضبط ID المستخدم أو المالك عند التنفيذ (s)، بت `bit` ملتصقة، الصلاحيات التى يمتلكها مالك الملف بالنسبة له حاليا (u)، الصلاحيات التى يمتلكها المستخدمين اﻵخرين فى مجموعة الملف بالنسبة له (g)،  الصلاحيات التى يمتلكها المستخدمين اﻵخرين من خارج مجموعة الملف بالنسبة له (o).  (لذا فإن `chmod g-s file' تحذف بت `bit` اضبط-ID-المجموعة (sgid)، و \\&`chmod ug+s file' تضبط بتات suid و sgid، بينما \\&`chmod o+s file' لا تفعل أى شئ.)"

# type: Plain text
#: chmod.1:70
msgid ""
"The name of the `sticky bit' derives from the original meaning: keep program "
"text on swap device.  These days, when set for a directory, it means that "
"only the owner of the file and the owner of that directory may remove the "
"file from that directory.  (This is commonly used on directories like /tmp "
"that have general write permission.)"
msgstr "اسم البت `الملتصقة` \"sticky bit\" مشتق من المعنى اﻷصلى: احفظ نص البرنامج فى جهاز التبديل \"swap\".  فى هذه اﻷيام، عن وضعها لمجلد، فإنها تعنى أن فقط مالك هذا الملف ومالك هذا الدليل يمكنهم حذف الملف من هذا الدليل.  (تستخدم بكثرة فى اﻷدلة مثل /tmp التى تمتلك تصاريح عامة للكتابة.)"

# type: Plain text
#: chmod.1:79
#, fuzzy
msgid ""
"A numeric mode is from one to four octal digits (0-7), derived by adding up "
"the bits with values 4, 2, and 1.  Any omitted digits are assumed to be "
"leading zeros.  The first digit selects the set user ID (4) and set group ID "
"(2) and save text image [`sticky'] (1) attributes.  The second digit selects "
"permissions for the user who owns the file: read (4), write (2), and execute "
"(1); the third selects permissions for other users in the file's group, with "
"the same values; and the fourth for other users not in the file's group, "
"with the same values."
msgstr "يمتد النمط الرقمى من واحد الى أربع أرقام ثمانية (0-7)، تحسب بجمع البتات ذات القيم 4، 2، و 1. أى أرقام مهمل ستعتبر أصفارا.  "

# type: Plain text
#: chmod.1:90
msgid ""
"B<chmod> never changes the permissions of symbolic links, since the B<chmod> "
"system call cannot change their permissions.  This is not a problem since "
"the permissions of symbolic links are never used. However, for each symbolic "
"link listed on the command line, B<chmod> changes the permissions of the "
"pointed-to file.  In contrast, B<chmod> ignores symbolic links encountered "
"during recursive directory traversals."
msgstr "لا يغير B<chmod> صلاحيات الروابط الرمزية بتاتا، حيث أن نداء النظام B<chmod> لا يستطيع تغيير صلاحياتها.  ليست هذه بمشكلة حيث أن صلاحيات الروابط الرمزية لا تستخدم أبدا.  بدلا من هذا، فإن B<chmod> يغير صلاحيات الملف الذى يشير اليه كل رابط رمزية مسرود فى سطر اﻷوامر.  فى المقابل فإن B<chmod> يتجاهل الروابط الرمزية التى يقابلها أثناء تنقله تتابعيا خلال اﻷدلة."

# type: SH
#: chmod.1:90
#, no-wrap
msgid "POSIX OPTIONS"
msgstr "خيارات POSIX"

# type: TP
#: chmod.1:91
#, no-wrap
msgid "B<-R>"
msgstr "B<-R>"

# type: Plain text
#: chmod.1:94 chmod.1:118
msgid "Recursively change permissions of directories and their contents."
msgstr "َغَيِّر تتابعيا الصلاحيات للأدلة و محتوياتها."

# type: TP
#: chmod.1:94 chmod.1:125
#, no-wrap
msgid "B<-->"
msgstr "B<-->"

# type: Plain text
#: chmod.1:97 chmod.1:128
msgid "Terminate option list."
msgstr "انهى قائمة الخيارات."

# type: SH
#: chmod.1:97
#, no-wrap
msgid "ADDITIONAL GNU DESCRIPTION"
msgstr "وصف جنو اضافى"

# type: Plain text
#: chmod.1:102
msgid ""
"A GNU extension (new in fileutils 4.0) allows one to use "
"B<--reference=>I<rfile> as a mode description: the same mode as that of "
"I<rfile>."
msgstr "امتداد جنو (جديد فى fileutils·4.0) يسمح للمرء باستخدام B<--reference=>I<rfile> كوصف للنمط: نفس النمط مثل ذلك الخاص بـ I<rfile>."

# type: SH
#: chmod.1:102
#, no-wrap
msgid "GNU OPTIONS"
msgstr "خيارات جنو"

# type: TP
#: chmod.1:103
#, no-wrap
msgid "B<-c, --changes>"
msgstr "B<-c, --changes>"

# type: Plain text
#: chmod.1:108
msgid ""
"Verbosely describe the action for each I<file> whose permissions actually "
"changes."
msgstr "صف بالتفصيل كل عمل تقوم به لكل ملف I<file> تتغير صلاحياته فعلا."

# type: TP
#: chmod.1:108
#, no-wrap
msgid "B<-f, --silent, --quiet>"
msgstr "B<-f, --silent, --quiet>"

# type: Plain text
#: chmod.1:111
msgid "Do not print error messages about files whose permissions cannot be changed."
msgstr "لا تطبع رسائل الخطأ عن الملفات التى لا يمكن تغيير صلاحياتها."

# type: TP
#: chmod.1:111
#, no-wrap
msgid "B<-v, --verbose>"
msgstr "B<-v, --verbose>"

# type: Plain text
#: chmod.1:115
msgid "Verbosely describe the action or non-action taken for every I<file>."
msgstr "صف بالتفصيل العمل أو عدم العمل المتخذ مع كل ملف I<file>."

# type: TP
#: chmod.1:115
#, no-wrap
msgid "B<-R, --recursive>"
msgstr "B<-R, --recursive>"

# type: SH
#: chmod.1:118
#, no-wrap
msgid "GNU STANDARD OPTIONS"
msgstr "خيارات جنو القياسية"

# type: TP
#: chmod.1:119
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

# type: Plain text
#: chmod.1:122
msgid "Print a usage message on standard output and exit successfully."
msgstr "اطبع رسالة استخدام فى الخرج القياسى ثم اغلق بنجاح."

# type: TP
#: chmod.1:122
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

# type: Plain text
#: chmod.1:125
msgid "Print version information on standard output, then exit successfully."
msgstr "اطبع معلومات اﻹصدارة فى الخرج القياسى, ثم اغلق بنجاح."

# type: SH
#: chmod.1:128
#, no-wrap
msgid "ENVIRONMENT"
msgstr "البيئة"

# type: Plain text
#: chmod.1:132
msgid ""
"The variables LANG, LC_ALL, LC_CTYPE and LC_MESSAGES have the usual "
"meaning. For an XSI-conforming system: NLSPATH has the usual meaning."
msgstr "المتغيرات LANG, LC_ALL, LC_CTYPE و LC_MESSAGES لها نفس المعانى المعتادة. للأنظمة المتوافقة مع NLSPATH :XSIلها المعنى المعتاد."

# type: SH
#: chmod.1:132
#, no-wrap
msgid "CONFORMING TO"
msgstr "استنادا الى"

# type: Plain text
#: chmod.1:139
msgid ""
"POSIX 1003.2 only requires the -R option. Use of other options may not be "
"portable. This standard does not describe the 't' permission bit. This "
"standard does not specify whether B<chmod> must preserve consistency by "
"clearing or refusing to set the suid and sgid bits, e.g., when all execute "
"bits are cleared, or whether B<chmod> honors the `s' bit at all."
msgstr "تتطلب POSIX·1003.2 الخيار -R فقط, استخدام باقى الخيرات قد لا يكون متنقلا (قد لا يعمل على كل الأنظمة). لا يصف هذا المعيار بت \"bit\" الصلاحيات 't'.  "

# type: SH
#: chmod.1:139
#, no-wrap
msgid "NONSTANDARD MODES"
msgstr "أنماط غير قياسية"

# type: Plain text
#: chmod.1:149
msgid ""
"Above we described the use of the `t' bit on directories.  Various systems "
"attach special meanings to otherwise meaningless combinations of mode bits.  "
"In particular, Linux, following System V (see System V Interface Definition "
"(SVID) Version 3), lets the sgid bit for files without group execute "
"permission mark the file for mandatory locking. For more details, see the "
"file I</usr/src/linux/Documentation/mandatory.txt>."
msgstr ""

# type: SH
#: chmod.1:149
#, no-wrap
msgid "NOTES"
msgstr "ملاحظات"

# type: Plain text
#: chmod.1:154
msgid ""
"This page describes B<chmod> as found in the fileutils-4.0 package; other "
"versions may differ slightly."
msgstr "تصف هذه الصفحة B<chmod> كما وجد فى حزمة fileutils-4.0؛ الإصدارات الأخرى قد تختلف قليلا."

# type: SH
#: chmod.1:154
#, no-wrap
msgid "SEE ALSO"
msgstr "انظر أيضا"

# type: Plain text
#: chmod.1:160
msgid ""
"B<chattr>(1), B<chown>(1), B<install>(1), B<chmod>(2), B<stat>(2), "
"B<umask>(2)"
msgstr ""
"B<chattr>(1), B<chown>(1), B<install>(1), B<chmod>(2), B<stat>(2), "
"B<umask>(2)"

