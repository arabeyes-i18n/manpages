# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:11+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: tzfile.5:4
#, no-wrap
msgid "TZFILE"
msgstr ""

# type: SH
#: tzfile.5:5
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: tzfile.5:7
msgid "tzfile - time zone information"
msgstr ""

# type: SH
#: tzfile.5:7
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: tzfile.5:10
msgid "B<#include E<lt>tzfile.hE<gt>>"
msgstr ""

# type: SH
#: tzfile.5:10
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: tzfile.5:22
msgid ""
"The time zone information files used by B<tzset>(3)  begin with the magic "
"characters \"TZif\" to identify then as time zone information files, "
"followed by sixteen bytes reserved for future use, followed by six four-byte "
"values of type B<long>, written in a ``standard'' byte order (the high-order "
"byte of the value is written first).  These values are, in order:"
msgstr ""

# type: TP
#: tzfile.5:22
#, no-wrap
msgid "I<tzh_ttisgmtcnt>"
msgstr ""

# type: Plain text
#: tzfile.5:25
msgid "The number of UTC/local indicators stored in the file."
msgstr ""

# type: TP
#: tzfile.5:25
#, no-wrap
msgid "I<tzh_ttisstdcnt>"
msgstr ""

# type: Plain text
#: tzfile.5:28
msgid "The number of standard/wall indicators stored in the file."
msgstr ""

# type: TP
#: tzfile.5:28
#, no-wrap
msgid "I<tzh_leapcnt>"
msgstr ""

# type: Plain text
#: tzfile.5:31
msgid "The number of leap seconds for which data is stored in the file."
msgstr ""

# type: TP
#: tzfile.5:31
#, no-wrap
msgid "I<tzh_timecnt>"
msgstr ""

# type: Plain text
#: tzfile.5:35
msgid "The number of \"transition times\" for which data is stored in the file."
msgstr ""

# type: TP
#: tzfile.5:35
#, no-wrap
msgid "I<tzh_typecnt>"
msgstr ""

# type: Plain text
#: tzfile.5:39
msgid ""
"The number of \"local time types\" for which data is stored in the file "
"(must not be zero)."
msgstr ""

# type: TP
#: tzfile.5:39
#, no-wrap
msgid "I<tzh_charcnt>"
msgstr ""

# type: Plain text
#: tzfile.5:43
msgid ""
"The number of characters of \"time zone abbreviation strings\" stored in the "
"file."
msgstr ""

# type: Plain text
#: tzfile.5:63
msgid ""
"The above header is followed by I<tzh_timecnt> four-byte values of type "
"B<long>, sorted in ascending order.  These values are written in "
"``standard'' byte order.  Each is used as a transition time (as returned by "
"B<time>(2))  at which the rules for computing local time change.  Next come "
"I<tzh_timecnt> one-byte values of type B<unsigned char>; each one tells "
"which of the different types of ``local time'' types described in the file "
"is associated with the same-indexed transition time.  These values serve as "
"indices into an array of I<ttinfo> structures that appears next in the file; "
"these structures are defined as follows:"
msgstr ""

# type: Plain text
#: tzfile.5:72
#, no-wrap
msgid ""
"struct ttinfo {\n"
"\tlong\ttt_gmtoff;\n"
"\tint\ttt_isdst;\n"
"\tunsigned int\ttt_abbrind;\n"
"};\n"
msgstr ""

# type: Plain text
#: tzfile.5:97
msgid ""
"Each structure is written as a four-byte value for I<tt_gmtoff> of type "
"B<long>, in a standard byte order, followed by a one-byte value for "
"I<tt_isdst> and a one-byte value for I<tt_abbrind>.  In each structure, "
"I<tt_gmtoff> gives the number of seconds to be added to UTC, I<tt_isdst> "
"tells whether I<tm_isdst> should be set by B<localtime>(3), and "
"I<tt_abbrind> serves as an index into the array of time zone abbreviation "
"characters that follow the I<ttinfo> structure(s) in the file."
msgstr ""

# type: Plain text
#: tzfile.5:109
msgid ""
"Then there are I<tzh_leapcnt> pairs of four-byte values, written in standard "
"byte order; the first value of each pair gives the time (as returned by "
"B<time>(2))  at which a leap second occurs; the second gives the I<total> "
"number of leap seconds to be applied after the given time.  The pairs of "
"values are sorted in ascending order by time."
msgstr ""

# type: Plain text
#: tzfile.5:117
msgid ""
"Then there are I<tzh_ttisstdcnt> standard/wall indicators, each stored as a "
"one-byte value; they tell whether the transition times associated with local "
"time types were specified as standard time or wall clock time, and are used "
"when a time zone file is used in handling POSIX-style time zone environment "
"variables."
msgstr ""

# type: Plain text
#: tzfile.5:125
msgid ""
"Finally, there are I<tzh_ttisgmtcnt> UTC/local indicators, each stored as a "
"one-byte value; they tell whether the transition times associated with local "
"time types were specified as UTC or local time, and are used when a time "
"zone file is used in handling POSIX-style time zone environment variables."
msgstr ""

# type: Plain text
#: tzfile.5:136
msgid ""
"I<Localtime> uses the first standard-time I<ttinfo> structure in the file "
"(or simply the first I<ttinfo> structure in the absence of a standard-time "
"structure)  if either I<tzh_timecnt> is zero or the time argument is less "
"than the first transition time recorded in the file."
msgstr ""
