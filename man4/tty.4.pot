# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:11+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: tty.4:26
#, no-wrap
msgid "TTY"
msgstr ""

# type: TH
#: tty.4:26
#, no-wrap
msgid "2003-04-07"
msgstr ""

# type: TH
#: tty.4:26
#, no-wrap
msgid "Linux"
msgstr ""

# type: TH
#: tty.4:26
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: tty.4:27
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: tty.4:29
msgid "tty - controlling terminal"
msgstr ""

# type: SH
#: tty.4:29
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: tty.4:33
msgid ""
"The file B</dev/tty> is a character file with major number 5 and minor "
"number 0, usually of mode 0666 and owner.group root.tty.  It is a synonym "
"for the controlling terminal of a process, if any."
msgstr ""

# type: Plain text
#: tty.4:36
msgid ""
"In addition to the B<ioctl()> requests supported by the device that B<tty> "
"refers to, the B<ioctl()> request B<TIOCNOTTY> is supported."
msgstr ""

# type: SS
#: tty.4:36
#, no-wrap
msgid "TIOCNOTTY"
msgstr ""

# type: Plain text
#: tty.4:38
msgid "Detach the current process from its controlling terminal."
msgstr ""

# type: Plain text
#: tty.4:42
msgid ""
"If the process is the session leader, then SIGHUP and SIGCONT signals are "
"sent to the foreground process group and all processes in the current "
"session lose their controlling tty."
msgstr ""

# type: Plain text
#: tty.4:50
msgid ""
"This B<ioctl()> call only works on file descriptors connected to "
"B</dev/tty>. It is used by daemon processes when they are invoked by a user "
"at a terminal.  The process attempts to open B</dev/tty>. If the open "
"succeeds, it detaches itself from the terminal by using B<TIOCNOTTY>, while "
"if the open fails, it is obviously not attached to a terminal and does not "
"need to detach itself."
msgstr ""

# type: SH
#: tty.4:50
#, no-wrap
msgid "FILES"
msgstr ""

# type: Plain text
#: tty.4:52
msgid "/dev/tty"
msgstr ""

# type: SH
#: tty.4:52
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: tty.4:59
msgid ""
"B<chown>(1), B<getty>(1), B<mknod>(1), B<ioctl>(2), B<termios>(3), "
"B<console>(4), B<ttys>(4)"
msgstr ""
