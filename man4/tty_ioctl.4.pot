# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:11+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: tty_ioctl.4:5
#, no-wrap
msgid "TTY_IOCTL"
msgstr ""

# type: TH
#: tty_ioctl.4:5
#, no-wrap
msgid "2002-12-29"
msgstr ""

# type: TH
#: tty_ioctl.4:5
#, no-wrap
msgid "Linux"
msgstr ""

# type: TH
#: tty_ioctl.4:5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: tty_ioctl.4:6
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: tty_ioctl.4:8
msgid "tty ioctl - ioctls for terminals and serial lines"
msgstr ""

# type: SH
#: tty_ioctl.4:8
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: tty_ioctl.4:11
msgid "B<#include E<lt>termios.hE<gt>>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:13
msgid "B<int ioctl(int >I<fd>B<, int >I<cmd>B<, ...);>"
msgstr ""

# type: SH
#: tty_ioctl.4:14
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: tty_ioctl.4:20
msgid ""
"The I<ioctl()> call for terminals and serial ports accepts many possible "
"command arguments.  Most require a third argument, of varying type, here "
"called I<argp> or I<arg>."
msgstr ""

# type: Plain text
#: tty_ioctl.4:26
msgid ""
"Use of I<ioctl> makes for non-portable programs. Use the POSIX interface "
"described in B<termios>(3)  whenever possible."
msgstr ""

# type: SS
#: tty_ioctl.4:27
#, no-wrap
msgid "Get and Set Terminal Attributes"
msgstr ""

# type: TP
#: tty_ioctl.4:28
#, no-wrap
msgid "B<TCGETS\tstruct termios *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:32
msgid "Equivalent to I<tcgetattr(fd, argp)>."
msgstr ""

# type: Plain text
#: tty_ioctl.4:34
msgid "Get the current serial port settings."
msgstr ""

# type: TP
#: tty_ioctl.4:34
#, no-wrap
msgid "B<TCSETS\tconst struct termios *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:38
msgid "Equivalent to I<tcsetattr(fd, TCSANOW, argp)>."
msgstr ""

# type: Plain text
#: tty_ioctl.4:40
msgid "Set the current serial port settings."
msgstr ""

# type: TP
#: tty_ioctl.4:40
#, no-wrap
msgid "B<TCSETSW\tconst struct termios *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:44
msgid "Equivalent to I<tcsetattr(fd, TCSADRAIN, argp)>."
msgstr ""

# type: Plain text
#: tty_ioctl.4:47
msgid "Allow the output buffer to drain, and set the current serial port settings."
msgstr ""

# type: TP
#: tty_ioctl.4:47
#, no-wrap
msgid "B<TCSETSF\tconst struct termios *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:51
msgid "Equivalent to I<tcsetattr(fd, TCSAFLUSH, argp)>."
msgstr ""

# type: Plain text
#: tty_ioctl.4:54
msgid ""
"Allow the output buffer to drain, discard pending input, and set the current "
"serial port settings."
msgstr ""

# type: Plain text
#: tty_ioctl.4:60
msgid ""
"The following four ioctls are just like TCGETS, TCSETS, TCSETSW, TCSETSF, "
"except that they take a B<struct termio *> instead of a B<struct termios *>."
msgstr ""

# type: TP
#: tty_ioctl.4:60
#, no-wrap
msgid "B<TCGETA\tstruct termio *>I<argp>"
msgstr ""

# type: TP
#: tty_ioctl.4:62
#, no-wrap
msgid "B<TCSETA\tconst struct termio *>I<argp>"
msgstr ""

# type: TP
#: tty_ioctl.4:64
#, no-wrap
msgid "B<TCSETAW\tconst struct termio *>I<argp>"
msgstr ""

# type: TP
#: tty_ioctl.4:66
#, no-wrap
msgid "B<TCSETAF\tconst struct termio *>I<argp>"
msgstr ""

# type: SS
#: tty_ioctl.4:69
#, no-wrap
msgid "Locking the termios structure"
msgstr ""

# type: Plain text
#: tty_ioctl.4:73
msgid ""
"The termios structure of a tty can be locked. The lock is itself a termios "
"structure, with nonzero bits or fields indicating a locked value."
msgstr ""

# type: TP
#: tty_ioctl.4:73
#, no-wrap
msgid "B<TIOCGLCKTRMIOS\tstruct termios *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:77
msgid "Gets the locking status of the termios structure of the terminal."
msgstr ""

# type: TP
#: tty_ioctl.4:77
#, no-wrap
msgid "B<TIOCSLCKTRMIOS\tconst struct termios *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:81
msgid ""
"Sets the locking status of the termios structure of the terminal. Only root "
"can do this."
msgstr ""

# type: SS
#: tty_ioctl.4:82
#, no-wrap
msgid "Get and Set Window Size"
msgstr ""

# type: Plain text
#: tty_ioctl.4:87
msgid ""
"Window sizes are kept in the kernel, but not used by the kernel (except in "
"the case of virtual consoles, where the kernel will update the window size "
"when the size of the virtual console changes, e.g. by loading a new font)."
msgstr ""

# type: TP
#: tty_ioctl.4:87
#, no-wrap
msgid "B<TIOCGWINSZ\tstruct winsize *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:90
msgid "Get window size."
msgstr ""

# type: TP
#: tty_ioctl.4:90
#, no-wrap
msgid "B<TIOCSWINSZ\tconst struct winsize *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:93
msgid "Set window size."
msgstr ""

# type: Plain text
#: tty_ioctl.4:95
msgid "The struct used by these ioctls is defined as"
msgstr ""

# type: Plain text
#: tty_ioctl.4:103
#, no-wrap
msgid ""
"struct winsize {\n"
"        unsigned short ws_row;\n"
"        unsigned short ws_col;\n"
"        unsigned short ws_xpixel;   /* unused */\n"
"        unsigned short ws_ypixel;   /* unused */\n"
"};\n"
msgstr ""

# type: Plain text
#: tty_ioctl.4:107
msgid ""
"When the window size changes, a SIGWINCH signal is sent to the foreground "
"process group."
msgstr ""

# type: SS
#: tty_ioctl.4:108
#, no-wrap
msgid "Sending a Break"
msgstr ""

# type: TP
#: tty_ioctl.4:109
#, no-wrap
msgid "B<TCSBRK\tint >I<arg>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:113
msgid "Equivalent to I<tcsendbreak(fd, arg)>."
msgstr ""

# type: Plain text
#: tty_ioctl.4:123
msgid ""
"If the terminal is using asynchronous serial data transmission, and I<arg> "
"is zero, then send a break (a stream of zero bits) for between 0.25 and 0.5 "
"seconds. If the terminal is not using asynchronous serial data transmission, "
"then either a break is sent, or the function returns without doing "
"anything.  When I<arg> is nonzero, nobody knows what will happen."
msgstr ""

# type: Plain text
#: tty_ioctl.4:141
msgid ""
"(SVR4, UnixWare, Solaris, Linux treat I<tcsendbreak(fd,arg)> with nonzero "
"I<arg> like I<tcdrain(fd)>.  SunOS treats I<arg> as a multiplier, and sends "
"a stream of bits I<arg> times as long as done for zero I<arg>.  DG-UX and "
"AIX treat I<arg> (when nonzero) as a timeinterval measured in milliseconds.  "
"HP-UX ignores I<arg>.)"
msgstr ""

# type: TP
#: tty_ioctl.4:141
#, no-wrap
msgid "B<TCSBRKP\tint >I<arg>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:147
msgid ""
"So-called \"POSIX version\" of TCSBRK. It treats nonzero I<arg> as a "
"timeinterval measured in deciseconds, and does nothing when the driver does "
"not support breaks."
msgstr ""

# type: TP
#: tty_ioctl.4:147
#, no-wrap
msgid "B<TIOCSBRK\tvoid>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:150
msgid "Turn break on, that is, start sending zero bits."
msgstr ""

# type: TP
#: tty_ioctl.4:150
#, no-wrap
msgid "B<TIOCCBRK\tvoid>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:153
msgid "Turn break off, that is, stop sending zero bits."
msgstr ""

# type: SS
#: tty_ioctl.4:154
#, no-wrap
msgid "Software flow control"
msgstr ""

# type: TP
#: tty_ioctl.4:155
#, no-wrap
msgid "B<TCXONC\tint >I<arg>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:159
msgid "Equivalent to I<tcflow(fd, arg)>."
msgstr ""

# type: Plain text
#: tty_ioctl.4:163
msgid "See B<tcflow>(3)  for the argument values TCOOFF, TCOON, TCIOFF, TCION."
msgstr ""

# type: SS
#: tty_ioctl.4:164
#, no-wrap
msgid "Buffer count and flushing"
msgstr ""

# type: TP
#: tty_ioctl.4:165
#, no-wrap
msgid "B<FIONREAD\tint *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:168
msgid "Get the number of bytes in the input buffer."
msgstr ""

# type: TP
#: tty_ioctl.4:168
#, no-wrap
msgid "B<TIOCINQ\tint *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:171
msgid "Same as FIONREAD."
msgstr ""

# type: TP
#: tty_ioctl.4:171
#, no-wrap
msgid "B<TIOCOUTQ\tint *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:174
msgid "Get the number of bytes in the output buffer."
msgstr ""

# type: TP
#: tty_ioctl.4:174
#, no-wrap
msgid "B<TCFLSH\tint >I<arg>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:178
msgid "Equivalent to I<tcflush(fd, arg)>."
msgstr ""

# type: Plain text
#: tty_ioctl.4:182
msgid "See B<tcflush>(3)  for the argument values TCIFLUSH, TCOFLUSH, TCIOFLUSH."
msgstr ""

# type: SS
#: tty_ioctl.4:183
#, no-wrap
msgid "Faking input"
msgstr ""

# type: TP
#: tty_ioctl.4:184
#, no-wrap
msgid "B<TIOCSTI\tconst char *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:187
msgid "Insert the given byte in the input queue."
msgstr ""

# type: SS
#: tty_ioctl.4:188
#, no-wrap
msgid "Redirecting console output"
msgstr ""

# type: TP
#: tty_ioctl.4:189
#, no-wrap
msgid "B<TIOCCONS\tvoid>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:204
msgid ""
"Redirect output that would have gone to I</dev/console> or I</dev/tty0> to "
"the given tty. If that was a pty master, send it to the slave.  Anybody can "
"do this as long as the output was not redirected yet.  If it was redirected "
"already EBUSY is returned, but root may stop redirection by using this ioctl "
"with I<fd> pointing at I</dev/console> or I</dev/tty0>."
msgstr ""

# type: SS
#: tty_ioctl.4:205
#, no-wrap
msgid "Controlling tty"
msgstr ""

# type: TP
#: tty_ioctl.4:206
#, no-wrap
msgid "B<TIOCSCTTY\tint >I<arg>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:216
msgid ""
"Make the given tty the controlling tty of the current process.  The current "
"process must be a session leader and not have a controlling tty already. If "
"this tty is already the controlling tty of a different session group then "
"the ioctl fails with EPERM, unless the caller is root and I<arg> equals 1, "
"in which case the tty is stolen, and all processes that had it as "
"controlling tty lose it."
msgstr ""

# type: TP
#: tty_ioctl.4:216
#, no-wrap
msgid "B<TIOCNOTTY\tvoid>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:222
msgid ""
"If the given tty was the controlling tty of the current process, give up "
"this controlling tty. If the process was session leader, then send SIGHUP "
"and SIGCONT to the foreground process group and all processes in the current "
"session lose their controlling tty."
msgstr ""

# type: SS
#: tty_ioctl.4:223
#, no-wrap
msgid "Process group and session ID"
msgstr ""

# type: TP
#: tty_ioctl.4:224
#, no-wrap
msgid "B<TIOCGPGRP\tpid_t *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:228
msgid "When successful, equivalent to I<*argp = tcgetpgrp(fd)>."
msgstr ""

# type: Plain text
#: tty_ioctl.4:230
msgid "Get the process group ID of the foreground proces group on this tty."
msgstr ""

# type: TP
#: tty_ioctl.4:230
#, no-wrap
msgid "B<TIOCSPGRP\tconst pid_t *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:234
msgid "Equivalent to I<tcsetpgrp(fd, *argp)>."
msgstr ""

# type: Plain text
#: tty_ioctl.4:236
msgid "Set the foreground process group id of this tty."
msgstr ""

# type: TP
#: tty_ioctl.4:236
#, no-wrap
msgid "B<TIOCGSID\tpid_t *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:240
msgid ""
"Get the session ID of the given tty. This will fail with ENOTTY in case the "
"tty is not a master pty and not our controlling tty. Strange."
msgstr ""

# type: SS
#: tty_ioctl.4:241
#, no-wrap
msgid "Exclusive mode"
msgstr ""

# type: TP
#: tty_ioctl.4:242
#, no-wrap
msgid "B<TIOCEXCL\tvoid>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:249
msgid ""
"Put the tty into exclusive mode.  No further B<open>(2)  operations on the "
"terminal are permitted.  (They will fail with EBUSY, except for root.)"
msgstr ""

# type: TP
#: tty_ioctl.4:249
#, no-wrap
msgid "B<TIOCNXCL\tvoid>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:252
msgid "Disable exclusive mode."
msgstr ""

# type: SS
#: tty_ioctl.4:253
#, no-wrap
msgid "Line discipline"
msgstr ""

# type: TP
#: tty_ioctl.4:254
#, no-wrap
msgid "B<TIOCGETD\tint *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:257
msgid "Get the line discipline of the tty."
msgstr ""

# type: TP
#: tty_ioctl.4:257
#, no-wrap
msgid "B<TIOCSETD\tconst int *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:260
msgid "Set the line discipline of the tty."
msgstr ""

# type: SS
#: tty_ioctl.4:261
#, no-wrap
msgid "Pseudo-tty ioctls"
msgstr ""

# type: TP
#: tty_ioctl.4:262
#, no-wrap
msgid "B<TIOCPKT\tconst int *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:274
msgid ""
"Enable (when *I<argp> is nonzero) or disable packet mode.  Can be applied to "
"the master side of a pseudotty only (and will return ENOTTY otherwise). In "
"packet mode, each subsequent B<read>(2)  will return a packet that either "
"contains a single nonzero control byte, or has a single zero byte followed "
"by data written on the slave side of the pty. If the first byte is not "
"TIOCPKT_DATA (0), it is an OR of one or more of the following bits:"
msgstr ""

# type: Plain text
#: tty_ioctl.4:282
#, no-wrap
msgid ""
"TIOCPKT_FLUSHREAD   The read queue for the terminal is flushed.\n"
"TIOCPKT_FLUSHWRITE  The write queue for the terminal is flushed.\n"
"TIOCPKT_STOP        Output to the terminal is stopped.\n"
"TIOCPKT_START       Output to the terminal is restarted.\n"
"TIOCPKT_DOSTOP      t_stopc is `^S' and t_startc is `^Q'.\n"
"TIOCPKT_NOSTOP      the start and stop characters are not `^S/^Q'.\n"
msgstr ""

# type: Plain text
#: tty_ioctl.4:289
msgid ""
"While this mode is in use, the presence of control status information to be "
"read from the master side may be detected by a B<select>(2)  for exceptional "
"conditions."
msgstr ""

# type: Plain text
#: tty_ioctl.4:295
msgid ""
"This mode is used by B<rlogin>(1)  and B<rlogind>(8)  to implement a "
"remote-echoed, locally `^S/^Q' flow-controlled remote login."
msgstr ""

# type: Plain text
#: tty_ioctl.4:298
msgid ""
"The BSD ioctls TIOCSTOP, TIOCSTART, TIOCUCNTL, TIOCREMOTE have not been "
"implemented under Linux."
msgstr ""

# type: SS
#: tty_ioctl.4:299
#, no-wrap
msgid "Modem control"
msgstr ""

# type: TP
#: tty_ioctl.4:300
#, no-wrap
msgid "B<TIOCMGET\tint *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:303
msgid "get the status of modem bits."
msgstr ""

# type: TP
#: tty_ioctl.4:303
#, no-wrap
msgid "B<TIOCMSET\tconst int *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:306
msgid "set the status of modem bits."
msgstr ""

# type: TP
#: tty_ioctl.4:306
#, no-wrap
msgid "B<TIOCMBIC\tconst int *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:309
msgid "clear the indicated modem bits."
msgstr ""

# type: TP
#: tty_ioctl.4:309
#, no-wrap
msgid "B<TIOCMBIS\tconst int *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:312
msgid "set the indicated modem bits."
msgstr ""

# type: Plain text
#: tty_ioctl.4:314
msgid "Bits used by these four ioctls:"
msgstr ""

# type: Plain text
#: tty_ioctl.4:327
#, no-wrap
msgid ""
"TIOCM_LE        DSR (data set ready/line enable)\n"
"TIOCM_DTR       DTR (data terminal ready)\n"
"TIOCM_RTS       RTS (request to send)\n"
"TIOCM_ST        Secondary TXD (transmit)\n"
"TIOCM_SR        Secondary RXD (receive)\n"
"TIOCM_CTS       CTS (clear to send)\n"
"TIOCM_CAR       DCD (data carrier detect)\n"
"TIOCM_CD         see TIOCM_CAR\n"
"TIOCM_RNG       RNG (ring)\n"
"TIOCM_RI         see TIOCM_RNG\n"
"TIOCM_DSR       DSR (data set ready)\n"
msgstr ""

# type: SS
#: tty_ioctl.4:329
#, no-wrap
msgid "Marking a line as local"
msgstr ""

# type: TP
#: tty_ioctl.4:330
#, no-wrap
msgid "B<TIOCGSOFTCAR\tint *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:335
msgid ""
"(\"Get software carrier flag\")  Get the status of the CLOCAL flag in the "
"c_cflag field of the termios structure."
msgstr ""

# type: TP
#: tty_ioctl.4:335
#, no-wrap
msgid "B<TIOCSSOFTCAR\tconst int *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:341
msgid ""
"(\"Set software carrier flag\")  Set the CLOCAL flag in the termios "
"structure when *I<argp> is nonzero, and clear it otherwise."
msgstr ""

# type: Plain text
#: tty_ioctl.4:350
msgid ""
"If the CLOCAL flag for a line is off, the hardware carrier detect (DCD)  "
"signal is significant, and an B<open>(2)  of the corresponding tty will "
"block until DCD is asserted, unless the O_NONBLOCK flag is given.  If CLOCAL "
"is set, the line behaves as if DCD is always asserted.  The software carrier "
"flag is usually turned on for local devices, and is off for lines with "
"modems."
msgstr ""

# type: SS
#: tty_ioctl.4:351
#, no-wrap
msgid "Linux specific"
msgstr ""

# type: Plain text
#: tty_ioctl.4:354
msgid "For the TIOCLINUX ioctl, see B<console_ioctl>(4)."
msgstr ""

# type: SS
#: tty_ioctl.4:355
#, no-wrap
msgid "Kernel debugging"
msgstr ""

# type: Plain text
#: tty_ioctl.4:358
msgid "B<#include E<lt>linux/tty.hE<gt>>"
msgstr ""

# type: TP
#: tty_ioctl.4:359
#, no-wrap
msgid "B<TIOCTTYGSTRUCT\tstruct tty_struct *>I<argp>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:363
msgid "Get the tty_struct corresponding to I<fd>."
msgstr ""

# type: SH
#: tty_ioctl.4:375
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: tty_ioctl.4:381
msgid ""
"The I<ioctl()> system call returns 0 on success. On error it returns -1 and "
"sets I<errno> appropriately."
msgstr ""

# type: SH
#: tty_ioctl.4:382
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: tty_ioctl.4:383
#, no-wrap
msgid "B<ENOIOCTLCMD>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:386
msgid "Unknown command."
msgstr ""

# type: TP
#: tty_ioctl.4:386
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:389
msgid "Invalid command parameter."
msgstr ""

# type: TP
#: tty_ioctl.4:389
#, no-wrap
msgid "B<EPERM>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:392
msgid "Insufficient permission."
msgstr ""

# type: TP
#: tty_ioctl.4:392
#, no-wrap
msgid "B<ENOTTY>"
msgstr ""

# type: Plain text
#: tty_ioctl.4:396
msgid "Inappropriate I<fd>."
msgstr ""

# type: SH
#: tty_ioctl.4:396
#, no-wrap
msgid "EXAMPLE"
msgstr ""

# type: Plain text
#: tty_ioctl.4:398
msgid "Check the condition of DTR on the serial port."
msgstr ""

# type: Plain text
#: tty_ioctl.4:403
#, no-wrap
msgid ""
"#include E<lt>termios.hE<gt>\n"
"#include E<lt>fcntl.hE<gt>\n"
"#include E<lt>sys/ioctl.hE<gt>\n"
msgstr ""

# type: Plain text
#: tty_ioctl.4:406
#, no-wrap
msgid ""
"main() {\n"
"    int fd, serial;\n"
msgstr ""

# type: Plain text
#: tty_ioctl.4:415
#, no-wrap
msgid ""
"    fd = open(\"/dev/ttyS0\", O_RDONLY);\n"
"    ioctl(fd, TIOCMGET, &serial);\n"
"    if (serial & TIOCM_DTR)\n"
"        puts(\"TIOCM_DTR is not set\");\n"
"    else\n"
"        puts(\"TIOCM_DTR is set\");\n"
"    close(fd);\n"
"}\n"
msgstr ""

# type: SH
#: tty_ioctl.4:417
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: tty_ioctl.4:421
msgid "B<ioctl>(2), B<termios>(3), B<console_ioctl>(4)"
msgstr ""
