# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: acos.3:31
#, no-wrap
msgid "ACOS"
msgstr ""

# type: TH
#: acos.3:31
#, no-wrap
msgid "2004-10-06"
msgstr ""

# type: TH
#: acos.3:31
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: acos.3:32
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: acos.3:34
msgid "acos, acosf, acosl - arc cosine function"
msgstr ""

# type: SH
#: acos.3:34
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: acos.3:37
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr ""

# type: Plain text
#: acos.3:41
#, no-wrap
msgid ""
"B<double acos(double >I<x>B<);>\n"
"B<float acosf(float >I<x>B<);>\n"
"B<long double acosl(long double >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: acos.3:44
msgid "Link with -lm."
msgstr ""

# type: SH
#: acos.3:44
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: acos.3:48
msgid ""
"The B<acos()> function calculates the arc cosine of I<x>; that is the value "
"whose cosine is I<x>.  If I<x> falls outside the range -1 to 1, B<acos()> "
"fails and I<errno> is set."
msgstr ""

# type: SH
#: acos.3:48
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: acos.3:51
msgid ""
"The B<acos()> function returns the arc cosine in radians and the value is "
"mathematically defined to be between 0 and PI (inclusive)."
msgstr ""

# type: SH
#: acos.3:51
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: acos.3:52
#, no-wrap
msgid "B<EDOM>"
msgstr ""

# type: Plain text
#: acos.3:55
msgid "I<x> is out of range."
msgstr ""

# type: SH
#: acos.3:55
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: acos.3:58
msgid ""
"SVID 3, POSIX, BSD 4.3, ISO 9899.  The float and long double variants are "
"C99 requirements."
msgstr ""

# type: SH
#: acos.3:58
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: acos.3:65
msgid ""
"B<asin>(3), B<atan>(3), B<atan2>(3), B<cacos>(3), B<cos>(3), B<sin>(3), "
"B<tan>(3)"
msgstr ""
