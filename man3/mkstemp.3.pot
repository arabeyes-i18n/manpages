# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: mkstemp.3:31
#, no-wrap
msgid "MKSTEMP"
msgstr ""

# type: TH
#: mkstemp.3:31
#, no-wrap
msgid "2001-12-23"
msgstr ""

# type: TH
#: mkstemp.3:31
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: mkstemp.3:31
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: mkstemp.3:32
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: mkstemp.3:34
msgid "mkstemp - create a unique temporary file"
msgstr ""

# type: SH
#: mkstemp.3:34
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: mkstemp.3:37
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr ""

# type: Plain text
#: mkstemp.3:39
#, no-wrap
msgid "B<int mkstemp(char *>I<template>B<);>\n"
msgstr ""

# type: SH
#: mkstemp.3:40
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: mkstemp.3:52
msgid ""
"The B<mkstemp()> function generates a unique temporary file name from "
"I<template>.  The last six characters of I<template> must be XXXXXX and "
"these are replaced with a string that makes the filename unique. The file is "
"then created with mode read/write and permissions 0666 (glibc 2.0.6 and "
"earlier), 0600 (glibc 2.0.7 and later).  Since it will be modified, "
"I<template> must not be a string constant, but should be declared as a "
"character array.  The file is opened with the O_EXCL flag, guaranteeing that "
"when B<mkstemp> returns successfully we are the only user."
msgstr ""

# type: SH
#: mkstemp.3:52
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: mkstemp.3:58
msgid ""
"On success, the B<mkstemp()> function returns the file descriptor of the "
"temporary file.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""

# type: SH
#: mkstemp.3:58
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: mkstemp.3:59
#, no-wrap
msgid "B<EEXIST>"
msgstr ""

# type: Plain text
#: mkstemp.3:63
msgid ""
"Could not create a unique temporary filename.  Now the contents of "
"I<template> are undefined."
msgstr ""

# type: TP
#: mkstemp.3:63
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: mkstemp.3:67
msgid ""
"The last six characters of I<template> were not XXXXXX.  Now I<template> is "
"unchanged."
msgstr ""

# type: SH
#: mkstemp.3:67
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: mkstemp.3:71
msgid ""
"The old behaviour (creating a file with mode 0666) may be a security risk, "
"especially since other Unix flavours use 0600, and somebody might overlook "
"this detail when porting programs."
msgstr ""

# type: Plain text
#: mkstemp.3:76
msgid ""
"More generally, the POSIX specification does not say anything about file "
"modes, so the application should make sure its umask is set appropriately "
"before calling B<mkstemp>."
msgstr ""

# type: SH
#: mkstemp.3:76
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: mkstemp.3:78
msgid "BSD 4.3, POSIX 1003.1-2001"
msgstr ""

# type: SH
#: mkstemp.3:78
#, no-wrap
msgid "NOTE"
msgstr ""

# type: Plain text
#: mkstemp.3:84
msgid ""
"The prototype is in I<E<lt>unistd.hE<gt>> for libc4, libc5, glibc1; glibc2 "
"follows the Single Unix Specification and has the prototype in "
"I<E<lt>stdlib.hE<gt>>."
msgstr ""

# type: SH
#: mkstemp.3:84
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: mkstemp.3:89
msgid "B<mkdtemp>(3), B<mktemp>(3), B<tempnam>(3), B<tmpfile>(3), B<tmpnam>(3)"
msgstr ""
