# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: psignal.3:28
#, no-wrap
msgid "PSIGNAL"
msgstr ""

# type: TH
#: psignal.3:28
#, no-wrap
msgid "1993-04-13"
msgstr ""

# type: TH
#: psignal.3:28
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: psignal.3:28
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: psignal.3:29
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: psignal.3:31
msgid "psignal - print signal message"
msgstr ""

# type: SH
#: psignal.3:31
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: psignal.3:34
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr ""

# type: Plain text
#: psignal.3:36
#, no-wrap
msgid "B<void psignal(int >I<sig>B<, const char *>I<s>B<);>\n"
msgstr ""

# type: Plain text
#: psignal.3:38
#, no-wrap
msgid "B<extern const char *const >I<sys_siglist>B<[];>\n"
msgstr ""

# type: SH
#: psignal.3:39
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: psignal.3:44
msgid ""
"The B<psignal()> function displays a message on I<stderr> consisting of the "
"string I<s>, a colon, a space, and a string describing the signal number "
"I<sig>.  If I<sig> is invalid, the message displayed will indicate an "
"unknown signal."
msgstr ""

# type: Plain text
#: psignal.3:47
msgid ""
"The array I<sys_siglist> holds the signal description strings indexed by "
"signal number."
msgstr ""

# type: SH
#: psignal.3:47
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: psignal.3:49
msgid "The B<psignal()> function returns no value."
msgstr ""

# type: SH
#: psignal.3:49
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: psignal.3:51
msgid "BSD 4.3"
msgstr ""

# type: SH
#: psignal.3:51
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: psignal.3:53
msgid "B<perror>(3), B<strsignal>(3)"
msgstr ""
