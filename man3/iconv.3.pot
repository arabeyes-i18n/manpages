# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: iconv.3:15
#, no-wrap
msgid "ICONV"
msgstr ""

# type: TH
#: iconv.3:15
#, no-wrap
msgid "2001-11-15"
msgstr ""

# type: TH
#: iconv.3:15
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: iconv.3:15
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: iconv.3:16
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: iconv.3:18
msgid "iconv - perform character set conversion"
msgstr ""

# type: SH
#: iconv.3:18
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: iconv.3:21
#, no-wrap
msgid "B<#include E<lt>iconv.hE<gt>>\n"
msgstr ""

# type: Plain text
#: iconv.3:25
#, no-wrap
msgid ""
"B<size_t iconv(iconv_t >I<cd>B<,>\n"
"B< char **>I<inbuf>B<, size_t *>I<inbytesleft>B<,>\n"
"B< char **>I<outbuf>B<, size_t *>I<outbytesleft>B<);>\n"
msgstr ""

# type: SH
#: iconv.3:26
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: iconv.3:29
msgid ""
"The argument I<cd> must be a conversion descriptor created using the "
"function B<iconv_open>."
msgstr ""

# type: Plain text
#: iconv.3:35
msgid ""
"The main case is when I<inbuf> is not NULL and I<*inbuf> is not NULL.  In "
"this case, the B<iconv> function converts the multibyte sequence starting at "
"I<*inbuf> to a multibyte sequence starting at I<*outbuf>.  At most "
"I<*inbytesleft> bytes, starting at I<*inbuf>, will be read.  At most "
"I<*outbytesleft> bytes, starting at I<*outbuf>, will be written."
msgstr ""

# type: Plain text
#: iconv.3:42
msgid ""
"The B<iconv> function converts one multibyte character at a time, and for "
"each character conversion it increments I<*inbuf> and decrements "
"I<*inbytesleft> by the number of converted input bytes, it increments "
"I<*outbuf> and decrements I<*outbytesleft> by the number of converted output "
"bytes, and it updates the conversion state contained in I<cd>.  The "
"conversion can stop for four reasons:"
msgstr ""

# type: Plain text
#: iconv.3:46
msgid ""
"1. An invalid multibyte sequence is encountered in the input. In this case "
"it sets B<errno> to B<EILSEQ> and returns (size_t)(-1). I<*inbuf> is left "
"pointing to the beginning of the invalid multibyte sequence."
msgstr ""

# type: Plain text
#: iconv.3:50
msgid ""
"2. The input byte sequence has been entirely converted, i.e. I<*inbytesleft> "
"has gone down to 0. In this case B<iconv> returns the number of "
"non-reversible conversions performed during this call."
msgstr ""

# type: Plain text
#: iconv.3:55
msgid ""
"3. An incomplete multibyte sequence is encountered in the input, and the "
"input byte sequence terminates after it. In this case it sets B<errno> to "
"B<EINVAL> and returns (size_t)(-1). I<*inbuf> is left pointing to the "
"beginning of the incomplete multibyte sequence."
msgstr ""

# type: Plain text
#: iconv.3:58
msgid ""
"4. The output buffer has no more room for the next converted character. In "
"this case it sets B<errno> to B<E2BIG> and returns (size_t)(-1)."
msgstr ""

# type: Plain text
#: iconv.3:68
msgid ""
"A different case is when I<inbuf> is NULL or I<*inbuf> is NULL, but "
"I<outbuf> is not NULL and I<*outbuf> is not NULL. In this case, the B<iconv> "
"function attempts to set I<cd>'s conversion state to the initial state and "
"store a corresponding shift sequence at I<*outbuf>.  At most "
"I<*outbytesleft> bytes, starting at I<*outbuf>, will be written.  If the "
"output buffer has no more room for this reset sequence, it sets B<errno> to "
"B<E2BIG> and returns (size_t)(-1). Otherwise it increments I<*outbuf> and "
"decrements I<*outbytesleft> by the number of bytes written."
msgstr ""

# type: Plain text
#: iconv.3:72
msgid ""
"A third case is when I<inbuf> is NULL or I<*inbuf> is NULL, and I<outbuf> is "
"NULL or I<*outbuf> is NULL. In this case, the B<iconv> function sets I<cd>'s "
"conversion state to the initial state."
msgstr ""

# type: SH
#: iconv.3:72
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: iconv.3:76
msgid ""
"The B<iconv> function returns the number of characters converted in a "
"non-reversible way during this call; reversible conversions are not "
"counted.  In case of error, it sets B<errno> and returns (size_t)(-1)."
msgstr ""

# type: SH
#: iconv.3:76
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: Plain text
#: iconv.3:78
msgid "The following errors can occur, among others:"
msgstr ""

# type: TP
#: iconv.3:78
#, no-wrap
msgid "B<E2BIG>"
msgstr ""

# type: Plain text
#: iconv.3:81
msgid "There is not sufficient room at I<*outbuf>."
msgstr ""

# type: TP
#: iconv.3:81
#, no-wrap
msgid "B<EILSEQ>"
msgstr ""

# type: Plain text
#: iconv.3:84
msgid "An invalid multibyte sequence has been encountered in the input."
msgstr ""

# type: TP
#: iconv.3:84
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: iconv.3:87
msgid "An incomplete multibyte sequence has been encountered in the input."
msgstr ""

# type: SH
#: iconv.3:87
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: iconv.3:89
msgid "UNIX98"
msgstr ""

# type: SH
#: iconv.3:89
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: iconv.3:91
msgid "B<iconv_close>(3), B<iconv_open>(3)"
msgstr ""
