# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: atoi.3:31
#, no-wrap
msgid "ATOI"
msgstr ""

# type: TH
#: atoi.3:31
#, no-wrap
msgid "2000-12-17"
msgstr ""

# type: TH
#: atoi.3:31
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: atoi.3:31
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: atoi.3:32
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: atoi.3:34
msgid "atoi, atol, atoll, atoq - convert a string to an integer"
msgstr ""

# type: SH
#: atoi.3:34
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: atoi.3:37
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr ""

# type: Plain text
#: atoi.3:39
#, no-wrap
msgid "B<int atoi(const char *>I<nptr>B<);>\n"
msgstr ""

# type: Plain text
#: atoi.3:41
#, no-wrap
msgid "B<long atol(const char *>I<nptr>B<);>\n"
msgstr ""

# type: Plain text
#: atoi.3:43
#, no-wrap
msgid "B<long long atoll(const char *>I<nptr>B<);>\n"
msgstr ""

# type: Plain text
#: atoi.3:45
#, no-wrap
msgid "B<long long atoq(const char *>I<nptr>B<);>\n"
msgstr ""

# type: SH
#: atoi.3:46
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: atoi.3:51
msgid ""
"The B<atoi()> function converts the initial portion of the string pointed to "
"by I<nptr> to I<int>.  The behaviour is the same as"
msgstr ""

# type: Plain text
#: atoi.3:54
msgid "B<strtol(nptr, (char **)NULL, 10);>"
msgstr ""

# type: Plain text
#: atoi.3:57
msgid "except that B<atoi()> does not detect errors."
msgstr ""

# type: Plain text
#: atoi.3:62
msgid ""
"The B<atol()> and B<atoll()> functions behave the same as B<atoi()>, except "
"that they convert the initial portion of the string to their return type of "
"I<long> or I<long long>.  B<atoq()> is an obsolete name for B<atoll()>."
msgstr ""

# type: SH
#: atoi.3:62
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: atoi.3:64
msgid "The converted value."
msgstr ""

# type: SH
#: atoi.3:64
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: atoi.3:68
msgid ""
"SVID 3, POSIX.1, BSD 4.3, ISO/IEC 9899.  ISO/IEC 9899:1990 (C89) and POSIX.1 "
"(1996 edition) include the functions B<atoi()> and B<atol()> only; C99 adds "
"the function B<atoll()>."
msgstr ""

# type: SH
#: atoi.3:68
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: atoi.3:74
msgid ""
"The non-standard B<atoq()> function is not present in libc 4.6.27 or glibc "
"2, but is present in libc5 and libc 4.7 (though only as an inline function "
"in B<E<lt>stdlib.hE<gt>> until libc 5.4.44).  The B<atoll()> function is "
"present in glibc 2 since version 2.0.2, but not in libc4 or libc5."
msgstr ""

# type: SH
#: atoi.3:74
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: atoi.3:78
msgid "B<atof>(3), B<strtod>(3), B<strtol>(3), B<strtoul>(3)"
msgstr ""
