# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: iswctype.3:14
#, no-wrap
msgid "ISWCTYPE"
msgstr ""

# type: TH
#: iswctype.3:14
#, no-wrap
msgid "1999-07-25"
msgstr ""

# type: TH
#: iswctype.3:14
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: iswctype.3:14
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: iswctype.3:15
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: iswctype.3:17
msgid "iswctype - wide character classification"
msgstr ""

# type: SH
#: iswctype.3:17
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: iswctype.3:20
#, no-wrap
msgid "B<#include E<lt>wctype.hE<gt>>\n"
msgstr ""

# type: Plain text
#: iswctype.3:22
#, no-wrap
msgid "B<int iswctype(wint_t >I<wc>B<, wctype_t >I<desc>B<);>\n"
msgstr ""

# type: SH
#: iswctype.3:23
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: iswctype.3:28
msgid ""
"If I<wc> is a wide character having the character property designated by "
"I<desc> (or in other words: belongs to the character class designated by "
"I<desc>), the B<iswctype> function returns non-zero. Otherwise it returns "
"zero. If I<wc> is WEOF, zero is returned."
msgstr ""

# type: Plain text
#: iswctype.3:31
msgid ""
"I<desc> must be a character property descriptor returned by the B<wctype> "
"function."
msgstr ""

# type: SH
#: iswctype.3:31
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: iswctype.3:34
msgid ""
"The B<iswctype> function returns non-zero if the I<wc> has the designated "
"property. Otherwise it returns 0."
msgstr ""

# type: SH
#: iswctype.3:34
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: iswctype.3:36
msgid "ISO/ANSI C, UNIX98"
msgstr ""

# type: SH
#: iswctype.3:36
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: iswctype.3:50
msgid ""
"B<iswalnum>(3), B<iswalpha>(3), B<iswblank>(3), B<iswcntrl>(3), "
"B<iswdigit>(3), B<iswgraph>(3), B<iswlower>(3), B<iswprint>(3), "
"B<iswpunct>(3), B<iswspace>(3), B<iswupper>(3), B<iswxdigit>(3), "
"B<wctype>(3)"
msgstr ""

# type: SH
#: iswctype.3:50
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: iswctype.3:52
msgid ""
"The behaviour of B<iswctype> depends on the LC_CTYPE category of the current "
"locale."
msgstr ""
