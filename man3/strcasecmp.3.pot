# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:10+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: strcasecmp.3:28
#, no-wrap
msgid "STRCASECMP"
msgstr ""

# type: TH
#: strcasecmp.3:28
#, no-wrap
msgid "1993-04-11"
msgstr ""

# type: TH
#: strcasecmp.3:28
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: strcasecmp.3:29
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: strcasecmp.3:31
msgid "strcasecmp, strncasecmp - compare two strings ignoring case"
msgstr ""

# type: SH
#: strcasecmp.3:31
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: strcasecmp.3:34
#, no-wrap
msgid "B<#include E<lt>strings.hE<gt>>\n"
msgstr ""

# type: Plain text
#: strcasecmp.3:36
#, no-wrap
msgid "B<int strcasecmp(const char *>I<s1>B<, const char *>I<s2>B<);>\n"
msgstr ""

# type: Plain text
#: strcasecmp.3:38
#, no-wrap
msgid ""
"B<int strncasecmp(const char *>I<s1>B<, const char *>I<s2>B<, size_t "
">I<n>B<);>\n"
msgstr ""

# type: SH
#: strcasecmp.3:39
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: strcasecmp.3:44
msgid ""
"The B<strcasecmp()> function compares the two strings I<s1> and I<s2>, "
"ignoring the case of the characters.  It returns an integer less than, equal "
"to, or greater than zero if I<s1> is found, respectively, to be less than, "
"to match, or be greater than I<s2>."
msgstr ""

# type: Plain text
#: strcasecmp.3:47
msgid ""
"The B<strncasecmp()> function is similar, except it only compares the first "
"I<n> characters of I<s1>."
msgstr ""

# type: SH
#: strcasecmp.3:47
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: strcasecmp.3:52
msgid ""
"The B<strcasecmp()> and B<strncasecmp()> functions return an integer less "
"than, equal to, or greater than zero if I<s1> (or the first I<n> bytes "
"thereof) is found, respectively, to be less than, to match, or be greater "
"than I<s2>."
msgstr ""

# type: SH
#: strcasecmp.3:52
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: strcasecmp.3:54
msgid "BSD 4.4, SUSv3"
msgstr ""

# type: SH
#: strcasecmp.3:54
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: strcasecmp.3:61
msgid ""
"B<bcmp>(3), B<memcmp>(3), B<strcmp>(3), B<strcoll>(3), B<strncmp>(3), "
"B<wcscasecmp>(3), B<wcsncasecmp>(3)"
msgstr ""
