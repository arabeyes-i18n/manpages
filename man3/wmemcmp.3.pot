# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:10+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: wmemcmp.3:13
#, no-wrap
msgid "WMEMCMP"
msgstr ""

# type: TH
#: wmemcmp.3:13
#, no-wrap
msgid "1999-07-25"
msgstr ""

# type: TH
#: wmemcmp.3:13
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: wmemcmp.3:13
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: wmemcmp.3:14
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: wmemcmp.3:16
msgid "wmemcmp - compare two arrays of wide-characters"
msgstr ""

# type: SH
#: wmemcmp.3:16
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: wmemcmp.3:19
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr ""

# type: Plain text
#: wmemcmp.3:21
#, no-wrap
msgid ""
"B<int wmemcmp(const wchar_t *>I<s1>B<, const wchar_t *>I<s2>B<, size_t "
">I<n>B<);>\n"
msgstr ""

# type: SH
#: wmemcmp.3:22
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: wmemcmp.3:26
msgid ""
"The B<wmemcmp> function is the wide-character equivalent of the B<memcmp> "
"function. It compares the I<n> wide-characters starting at I<s1> and the "
"I<n> wide-characters starting at I<s2>."
msgstr ""

# type: SH
#: wmemcmp.3:26
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: wmemcmp.3:34
msgid ""
"The B<wmemcmp> function returns zero if the wide-character arrays of size "
"I<n> at I<s1> and I<s2> are equal. It returns an integer greater than zero "
"if at the first differing position I<i> (I<i> E<lt> I<n>), the corresponding "
"wide-character I<s1[i]> is greater than I<s2[i]>. It returns an integer less "
"than zero if at the first differing position I<i> (I<i> E<lt> I<n>), the "
"corresponding wide-character I<s1[i]> is less than I<s2[i]>."
msgstr ""

# type: SH
#: wmemcmp.3:34
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: wmemcmp.3:36
msgid "ISO/ANSI C, UNIX98"
msgstr ""

# type: SH
#: wmemcmp.3:36
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: wmemcmp.3:38
msgid "B<memcmp>(3), B<wcscmp>(3)"
msgstr ""
