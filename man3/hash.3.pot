# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: hash.3:34
#, no-wrap
msgid "HASH"
msgstr ""

# type: TH
#: hash.3:34
#, no-wrap
msgid "1994-08-18"
msgstr ""

# type: SH
#: hash.3:36
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: hash.3:38
msgid "hash - hash database access method"
msgstr ""

# type: SH
#: hash.3:38
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: hash.3:43
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>\n"
"#include E<lt>db.hE<gt>>\n"
msgstr ""

# type: SH
#: hash.3:45
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: hash.3:53
msgid ""
"The routine I<dbopen> is the library interface to database files.  One of "
"the supported file formats is hash files.  The general description of the "
"database access methods is in I<dbopen>(3), this manual page describes only "
"the hash specific information."
msgstr ""

# type: Plain text
#: hash.3:55
msgid "The hash data structure is an extensible, dynamic hashing scheme."
msgstr ""

# type: Plain text
#: hash.3:59
msgid ""
"The access method specific data structure provided to I<dbopen> is defined "
"in the E<lt>db.hE<gt> include file as follows:"
msgstr ""

# type: Plain text
#: hash.3:61
msgid "typedef struct {"
msgstr ""

# type: Plain text
#: hash.3:63
msgid "u_int bsize;"
msgstr ""

# type: Plain text
#: hash.3:65
msgid "u_int ffactor;"
msgstr ""

# type: Plain text
#: hash.3:67
msgid "u_int nelem;"
msgstr ""

# type: Plain text
#: hash.3:69
msgid "u_int cachesize;"
msgstr ""

# type: Plain text
#: hash.3:71
msgid "u_int32_t (*hash)(const void *, size_t);"
msgstr ""

# type: Plain text
#: hash.3:73
msgid "int lorder;"
msgstr ""

# type: Plain text
#: hash.3:75
msgid "} HASHINFO;"
msgstr ""

# type: Plain text
#: hash.3:77
msgid "The elements of this structure are as follows:"
msgstr ""

# type: TP
#: hash.3:77
#, no-wrap
msgid "bsize"
msgstr ""

# type: Plain text
#: hash.3:83
msgid ""
"I<Bsize> defines the hash table bucket size, and is, by default, 256 bytes.  "
"It may be preferable to increase the page size for disk-resident tables and "
"tables with large data items."
msgstr ""

# type: TP
#: hash.3:83
#, no-wrap
msgid "ffactor"
msgstr ""

# type: Plain text
#: hash.3:90
msgid ""
"I<Ffactor> indicates a desired density within the hash table.  It is an "
"approximation of the number of keys allowed to accumulate in any one bucket, "
"determining when the hash table grows or shrinks.  The default value is 8."
msgstr ""

# type: TP
#: hash.3:90
#, no-wrap
msgid "nelem"
msgstr ""

# type: Plain text
#: hash.3:97
msgid ""
"I<Nelem> is an estimate of the final size of the hash table.  If not set or "
"set too low, hash tables will expand gracefully as keys are entered, "
"although a slight performance degradation may be noticed.  The default value "
"is 1."
msgstr ""

# type: TP
#: hash.3:97
#, no-wrap
msgid "cachesize"
msgstr ""

# type: Plain text
#: hash.3:104
msgid ""
"A suggested maximum size, in bytes, of the memory cache.  This value is "
"B<only> advisory, and the access method will allocate more memory rather "
"than fail."
msgstr ""

# type: TP
#: hash.3:104
#, no-wrap
msgid "hash"
msgstr ""

# type: Plain text
#: hash.3:114
msgid ""
"I<Hash> is a user defined hash function.  Since no hash function performs "
"equally well on all possible data, the user may find that the built-in hash "
"function does poorly on a particular data set.  User specified hash "
"functions must take two arguments (a pointer to a byte string and a length) "
"and return a 32-bit quantity to be used as the hash value."
msgstr ""

# type: TP
#: hash.3:114
#, no-wrap
msgid "lorder"
msgstr ""

# type: Plain text
#: hash.3:124
msgid ""
"The byte order for integers in the stored database metadata.  The number "
"should represent the order as an integer; for example, big endian order "
"would be the number 4,321.  If I<lorder> is 0 (no order is specified) the "
"current host order is used.  If the file already exists, the specified value "
"is ignored and the value specified when the tree was created is used."
msgstr ""

# type: Plain text
#: hash.3:128
msgid ""
"If the file already exists (and the O_TRUNC flag is not specified), the "
"values specified for the parameters bsize, ffactor, lorder and nelem are "
"ignored and the values specified when the tree was created are used."
msgstr ""

# type: Plain text
#: hash.3:133
msgid ""
"If a hash function is specified, I<hash_open> will attempt to determine if "
"the hash function specified is the same as the one with which the database "
"was created, and will fail if it is not."
msgstr ""

# type: Plain text
#: hash.3:140
msgid ""
"Backward compatible interfaces to the routines described in I<dbm>(3), and "
"I<ndbm>(3)  are provided, however these interfaces are not compatible with "
"previous file formats."
msgstr ""

# type: SH
#: hash.3:140
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: Plain text
#: hash.3:147
msgid ""
"The I<hash> access method routines may fail and set I<errno> for any of the "
"errors specified for the library routine I<dbopen>(3)."
msgstr ""

# type: SH
#: hash.3:147
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: hash.3:152
msgid "I<btree>(3), I<dbopen>(3), I<mpool>(3), I<recno>(3)"
msgstr ""

# type: Plain text
#: hash.3:155
msgid ""
"I<Dynamic Hash Tables>, Per-Ake Larson, Communications of the ACM, April "
"1988."
msgstr ""

# type: Plain text
#: hash.3:158
msgid ""
"I<A New Hash Package for UNIX>, Margo Seltzer, USENIX Proceedings, Winter "
"1991."
msgstr ""

# type: SH
#: hash.3:158
#, no-wrap
msgid "BUGS"
msgstr ""

# type: Plain text
#: hash.3:159
msgid "Only big and little endian byte order is supported."
msgstr ""
