# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: getaddrinfo.3:24
#, no-wrap
msgid "getaddrinfo"
msgstr ""

# type: TH
#: getaddrinfo.3:24
#, no-wrap
msgid "2000-12-18"
msgstr ""

# type: TH
#: getaddrinfo.3:24
#, no-wrap
msgid "Linux Man Page"
msgstr ""

# type: TH
#: getaddrinfo.3:24
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: getaddrinfo.3:25
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: getaddrinfo.3:27
msgid ""
"getaddrinfo, freeaddrinfo, gai_strerror - network address and service "
"translation"
msgstr ""

# type: SH
#: getaddrinfo.3:27
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: getaddrinfo.3:32
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>sys/socket.hE<gt>>\n"
"B<#include E<lt>netdb.hE<gt>>\n"
msgstr ""

# type: Plain text
#: getaddrinfo.3:36
#, no-wrap
msgid ""
"B<int getaddrinfo(const char *>I<node>B<, const char *>I<service>B<,>\n"
"B< const struct addrinfo *>I<hints>B<,>\n"
"B< struct addrinfo **>I<res>B<);>\n"
msgstr ""

# type: Plain text
#: getaddrinfo.3:38
#, no-wrap
msgid "B<void freeaddrinfo(struct addrinfo *>I<res>B<);>\n"
msgstr ""

# type: Plain text
#: getaddrinfo.3:40
#, no-wrap
msgid "B<const char *gai_strerror(int >I<errcode>B<);>\n"
msgstr ""

# type: SH
#: getaddrinfo.3:41
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: getaddrinfo.3:58
msgid ""
"The B<getaddrinfo>(3)  function combines the functionality provided by the "
"B<getipnodebyname>(3), B<getipnodebyaddr>(3), B<getservbyname>(3), and "
"B<getservbyport>(3)  functions into a single interface.  The thread-safe "
"B<getaddrinfo>(3)  function creates one or more socket address structures "
"that can be used by the B<bind>(2)  and B<connect>(2)  system calls to "
"create a client or a server socket."
msgstr ""

# type: Plain text
#: getaddrinfo.3:68
msgid ""
"The B<getaddrinfo>(3)  function is not limited to creating IPv4 socket "
"address structures; IPv6 socket address structures can be created if IPv6 "
"support is available.  These socket address structures can be used directly "
"by B<bind>(2)  or B<connect>(2), to prepare a client or a server socket."
msgstr ""

# type: Plain text
#: getaddrinfo.3:72
msgid ""
"The B<addrinfo> structure used by this function contains the following "
"members:"
msgstr ""

# type: Plain text
#: getaddrinfo.3:84
#, no-wrap
msgid ""
"B<struct addrinfo {>\n"
"B< int >I<ai_flags>B<;>\n"
"B< int >I<ai_family>B<;>\n"
"B< int >I<ai_socktype>B<;>\n"
"B< int >I<ai_protocol>B<;>\n"
"B< size_t >I<ai_addrlen>B<;>\n"
"B< struct sockaddr *>I<ai_addr>B<;>\n"
"B< char *>I<ai_canonname>B<;>\n"
"B< struct addrinfo *>I<ai_next>B<;>\n"
"B<};>\n"
msgstr ""

# type: Plain text
#: getaddrinfo.3:104
msgid ""
"B<getaddrinfo>(3)  sets I<res> to point to a dynamically-allocated link list "
"of B<addrinfo> structures, linked by the I<ai_next> member.  There are "
"several reasons why the link list may have more than one B<addrinfo> "
"structure, including: if the network host is multi-homed; or if the same "
"service is available from multiple socket protocols (one B<SOCK_STREAM> "
"address and another B<SOCK_DGRAM> address, for example)."
msgstr ""

# type: Plain text
#: getaddrinfo.3:122
msgid ""
"The members I<ai_family>, I<ai_socktype>, and I<ai_protocol> have the same "
"meaning as the corresponding parameters in the B<socket>(2)  system call.  "
"The B<getaddrinfo>(3)  function returns socket addresses in either IPv4 or "
"IPv6 address family, (I<ai_family> will be set to either B<PF_INET> or "
"B<PF_INET6>)."
msgstr ""

# type: Plain text
#: getaddrinfo.3:158
msgid ""
"The I<hints> parameter specifies the preferred socket type, or protocol.  A "
"NULL I<hints> specifies that any network address or protocol is acceptable.  "
"If this parameter is not B<NULL> it points to an B<addrinfo> structure whose "
"I<ai_family>, I<ai_socktype>, and I<ai_protocol> members specify the "
"preferred socket type.  B<PF_UNSPEC> in I<ai_family> specifies any protocol "
"family (either IPv4 or IPv6, for example).  0 in I<ai_socktype> or "
"I<ai_protocol> specifies that any socket type or protocol is acceptable as "
"well.  The I<ai_flags> member specifies additional options, defined below.  "
"Multiple flags are specified by logically OR-ing them together.  All the "
"other members in the I<hints> parameter must contain either 0, or a null "
"pointer."
msgstr ""

# type: Plain text
#: getaddrinfo.3:180
msgid ""
"The I<node> or I<service> parameter, but not both, may be NULL.  I<node> "
"specifies either a numerical network address (dotted-decimal format for "
"IPv4, hexadecimal format for IPv6)  or a network hostname, whose network "
"addresses are looked up and resolved.  If the I<ai_flags> member in the "
"I<hints> parameter contains the B<AI_NUMERICHOST> flag then the I<node> "
"parameter must be a numerical network address.  The B<AI_NUMERICHOST> flag "
"suppresses any potentially lengthy network host address lookups."
msgstr ""

# type: Plain text
#: getaddrinfo.3:208
msgid ""
"The B<getaddrinfo>(3)  function creates a link list of B<addrinfo> "
"structures, one for each network address subject to any restrictions imposed "
"by the I<hints> parameter.  I<ai_canonname> is set to point to the official "
"name of the host, if I<ai_flags> in I<hints> includes the B<AI_CANONNAME> "
"flag.  I<ai_family>, I<ai_socktype>, and I<ai_protocol> specify the socket "
"creation parameters.  A pointer to the socket address is placed in the "
"I<ai_addr> member, and the length of the socket address, in bytes, is placed "
"in the I<ai_addrlen> member."
msgstr ""

# type: Plain text
#: getaddrinfo.3:232
msgid ""
"If I<node> is NULL, the network address in each socket structure is "
"initialized according to the B<AI_PASSIVE> flag, which is set in the "
"I<ai_flags> member of the I<hints> parameter.  The network address in each "
"socket structure will be left unspecified if B<AI_PASSIVE> flag is set.  "
"This is used by server applications, which intend to accept client "
"connections on any network address.  The network address will be set to the "
"loopback interface address if the B<AI_PASSIVE> flag is not set.  This is "
"used by client applications, which intend to connect to a server running on "
"the same network host."
msgstr ""

# type: Plain text
#: getaddrinfo.3:238
msgid ""
"I<service> sets the port number in the network address of each socket "
"structure.  If I<service> is NULL the port number will be left "
"uninitialized."
msgstr ""

# type: Plain text
#: getaddrinfo.3:244
msgid ""
"The B<freeaddrinfo>(3)  function frees the memory that was allocated for the "
"dynamically allocated link list I<res>."
msgstr ""

# type: SH
#: getaddrinfo.3:244
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: getaddrinfo.3:247
msgid ""
"B<getaddrinfo>(3)  returns 0 if it succeeds, or one of the following "
"non-zero error codes:"
msgstr ""

# type: TP
#: getaddrinfo.3:247
#, no-wrap
msgid "B<EAI_FAMILY>"
msgstr ""

# type: Plain text
#: getaddrinfo.3:250
msgid "The requested address family is not supported at all."
msgstr ""

# type: TP
#: getaddrinfo.3:250
#, no-wrap
msgid "B<EAI_SOCKTYPE>"
msgstr ""

# type: Plain text
#: getaddrinfo.3:253
msgid "The requested socket type is not supported at all."
msgstr ""

# type: TP
#: getaddrinfo.3:253
#, no-wrap
msgid "B<EAI_BADFLAGS>"
msgstr ""

# type: Plain text
#: getaddrinfo.3:257
msgid "I<ai_flags> contains invalid flags."
msgstr ""

# type: TP
#: getaddrinfo.3:257
#, no-wrap
msgid "B<EAI_NONAME>"
msgstr ""

# type: Plain text
#: getaddrinfo.3:269
msgid ""
"The I<node> or I<service> is not known.  This error is also returned if both "
"I<node> and I<service> are NULL."
msgstr ""

# type: TP
#: getaddrinfo.3:269
#, no-wrap
msgid "B<EAI_SERVICE>"
msgstr ""

# type: Plain text
#: getaddrinfo.3:273
msgid ""
"The requested service is not available for the requested socket type.  It "
"may be available through another socket type."
msgstr ""

# type: TP
#: getaddrinfo.3:273
#, no-wrap
msgid "B<EAI_ADDRFAMILY>"
msgstr ""

# type: Plain text
#: getaddrinfo.3:277
msgid ""
"The specified network host does not have any network addresses in the "
"requested address family."
msgstr ""

# type: TP
#: getaddrinfo.3:277
#, no-wrap
msgid "B<EAI_NODATA>"
msgstr ""

# type: Plain text
#: getaddrinfo.3:281
msgid ""
"The specified network host exists, but does not have any network addresses "
"defined."
msgstr ""

# type: TP
#: getaddrinfo.3:281
#, no-wrap
msgid "B<EAI_MEMORY>"
msgstr ""

# type: Plain text
#: getaddrinfo.3:284
msgid "Out of memory."
msgstr ""

# type: TP
#: getaddrinfo.3:284
#, no-wrap
msgid "B<EAI_FAIL>"
msgstr ""

# type: Plain text
#: getaddrinfo.3:287
msgid "The name server returned a permanent failure indication."
msgstr ""

# type: TP
#: getaddrinfo.3:287
#, no-wrap
msgid "B<EAI_AGAIN>"
msgstr ""

# type: Plain text
#: getaddrinfo.3:291
msgid "The name server returned a temporary failure indication.  Try again later."
msgstr ""

# type: TP
#: getaddrinfo.3:291
#, no-wrap
msgid "B<EAI_SYSTEM>"
msgstr ""

# type: Plain text
#: getaddrinfo.3:296
msgid "Other system error, check I<errno> for details."
msgstr ""

# type: Plain text
#: getaddrinfo.3:301
msgid ""
"The B<gai_strerror>(3)  function translates these error codes to a human "
"readable string, suitable for error reporting."
msgstr ""

# type: SH
#: getaddrinfo.3:301
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: getaddrinfo.3:306
msgid "POSIX 1003.1-2003.  The B<getaddrinfo()> function is documented in RFC 2553."
msgstr ""

# type: SH
#: getaddrinfo.3:306
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: getaddrinfo.3:308
msgid "B<getipnodebyaddr>(3), B<getipnodebyname>(3)"
msgstr ""
