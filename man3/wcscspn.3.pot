# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:10+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: wcscspn.3:14
#, no-wrap
msgid "WCSCSPN"
msgstr ""

# type: TH
#: wcscspn.3:14
#, no-wrap
msgid "1999-07-25"
msgstr ""

# type: TH
#: wcscspn.3:14
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: wcscspn.3:14
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: wcscspn.3:15
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: wcscspn.3:17
msgid "wcscspn - search a wide-character string for any of a set of wide characters"
msgstr ""

# type: SH
#: wcscspn.3:17
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: wcscspn.3:20
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr ""

# type: Plain text
#: wcscspn.3:22
#, no-wrap
msgid "B<size_t wcscspn(const wchar_t *>I<wcs>B<, const wchar_t *>I<reject>B<);>\n"
msgstr ""

# type: SH
#: wcscspn.3:23
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: wcscspn.3:30
msgid ""
"The B<wcscspn> function is the wide-character equivalent of the B<strcspn> "
"function. It determines the length of the longest initial segment of I<wcs> "
"which consists entirely of wide-characters not listed in I<reject>. In other "
"words, it searches for the first occurrence in the wide-character string "
"I<wcs> of any of the characters in the wide-character string I<reject>."
msgstr ""

# type: SH
#: wcscspn.3:30
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: wcscspn.3:36
msgid ""
"The B<wcscspn> function returns the number of wide characters in the longest "
"initial segment of I<wcs> which consists entirely of wide-characters not "
"listed in I<reject>. In other words, it returns the position of the first "
"occurrence in the wide-character string I<wcs> of any of the characters in "
"the wide-character string I<reject>, or I<wcslen(wcs)> if there is none."
msgstr ""

# type: SH
#: wcscspn.3:36
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: wcscspn.3:38
msgid "ISO/ANSI C, UNIX98"
msgstr ""

# type: SH
#: wcscspn.3:38
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: wcscspn.3:41
msgid "B<strcspn>(3), B<wcspbrk>(3), B<wcsspn>(3)"
msgstr ""
