# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:07+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: fnmatch.3:26
#, no-wrap
msgid "FNMATCH"
msgstr ""

# type: TH
#: fnmatch.3:26
#, no-wrap
msgid "2000-10-15"
msgstr ""

# type: TH
#: fnmatch.3:26
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: fnmatch.3:26
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: fnmatch.3:27
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: fnmatch.3:29
msgid "fnmatch - match filename or pathname"
msgstr ""

# type: SH
#: fnmatch.3:29
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: fnmatch.3:32
#, no-wrap
msgid "B<#include E<lt>fnmatch.hE<gt>>\n"
msgstr ""

# type: Plain text
#: fnmatch.3:34
#, no-wrap
msgid ""
"B<int fnmatch(const char *>I<pattern>B<, const char *>I<string>B<, int "
">I<flags>B<);>\n"
msgstr ""

# type: SH
#: fnmatch.3:35
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: fnmatch.3:43
msgid ""
"The B<fnmatch()> function checks whether the I<string> argument matches the "
"I<pattern> argument, which is a shell wildcard pattern."
msgstr ""

# type: Plain text
#: fnmatch.3:48
msgid ""
"The I<flags> argument modifies the behaviour; it is the bitwise OR of zero "
"or more of the following flags:"
msgstr ""

# type: TP
#: fnmatch.3:48
#, no-wrap
msgid "B<FNM_NOESCAPE>"
msgstr ""

# type: Plain text
#: fnmatch.3:52
msgid ""
"If this flag is set, treat backslash as an ordinary character, instead of an "
"escape character."
msgstr ""

# type: TP
#: fnmatch.3:52
#, no-wrap
msgid "B<FNM_PATHNAME>"
msgstr ""

# type: Plain text
#: fnmatch.3:59
msgid ""
"If this flag is set, match a slash in I<string> only with a slash in "
"I<pattern> and not, for example, with a [] - sequence containing a slash."
msgstr ""

# type: TP
#: fnmatch.3:59
#, no-wrap
msgid "B<FNM_PERIOD>"
msgstr ""

# type: Plain text
#: fnmatch.3:70
msgid ""
"If this flag is set, a leading period in I<string> has to be matched exactly "
"by a period in I<pattern>.  A period is considered to be leading if it is "
"the first character in I<string>, or if both B<FNM_PATHNAME> is set and the "
"period immediately follows a slash."
msgstr ""

# type: TP
#: fnmatch.3:70
#, no-wrap
msgid "B<FNM_FILE_NAME>"
msgstr ""

# type: Plain text
#: fnmatch.3:73
msgid "This is a GNU synonym for B<FNM_PATHNAME>."
msgstr ""

# type: TP
#: fnmatch.3:73
#, no-wrap
msgid "B<FNM_LEADING_DIR>"
msgstr ""

# type: Plain text
#: fnmatch.3:80
msgid ""
"If this flag (a GNU extension) is set, the pattern is considered to be "
"matched if it matches an initial segment of I<string> which is followed by a "
"slash.  This flag is mainly for the internal use of glibc and is only "
"implemented in certain cases."
msgstr ""

# type: TP
#: fnmatch.3:80
#, no-wrap
msgid "B<FNM_CASEFOLD>"
msgstr ""

# type: Plain text
#: fnmatch.3:84
msgid ""
"If this flag (a GNU extension) is set, the pattern is matched "
"case-insensitively."
msgstr ""

# type: SH
#: fnmatch.3:84
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: fnmatch.3:91
msgid ""
"Zero if I<string> matches I<pattern>, B<FNM_NOMATCH> if there is no match or "
"another non-zero value if there is an error."
msgstr ""

# type: SH
#: fnmatch.3:91
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: fnmatch.3:95
msgid ""
"ISO/IEC 9945-2: 1993 (POSIX.2).  The B<FNM_FILE_NAME>, B<FNM_LEADING_DIR>, "
"and B<FNM_CASEFOLD> flags are GNU extensions."
msgstr ""

# type: SH
#: fnmatch.3:95
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: fnmatch.3:100
msgid "B<sh>(1), B<glob>(3), B<scandir>(3), B<wordexp>(3), B<glob>(7)"
msgstr ""
