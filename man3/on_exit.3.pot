# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: on_exit.3:29
#, no-wrap
msgid "ON_EXIT"
msgstr ""

# type: TH
#: on_exit.3:29
#, no-wrap
msgid "1993-04-02"
msgstr ""

# type: TH
#: on_exit.3:29
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: on_exit.3:29
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: on_exit.3:30
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: on_exit.3:32
msgid "on_exit - register a function to be called at normal program termination"
msgstr ""

# type: SH
#: on_exit.3:32
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: on_exit.3:35
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr ""

# type: Plain text
#: on_exit.3:37
#, no-wrap
msgid "B<int on_exit(void (*>I<function>B<)(int , void *), void *>I<arg>B<);>\n"
msgstr ""

# type: SH
#: on_exit.3:38
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: on_exit.3:46
msgid ""
"The B<on_exit()> function registers the given I<function> to be called at "
"normal program termination, whether via B<exit>(3)  or via return from the "
"program's B<main>.  The I<function> is passed the argument to B<exit>(3)  "
"and the I<arg> argument from B<on_exit()>."
msgstr ""

# type: SH
#: on_exit.3:46
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: on_exit.3:49
msgid ""
"The B<on_exit()> function returns the value 0 if successful; otherwise it "
"returns a non-zero value."
msgstr ""

# type: SH
#: on_exit.3:49
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: on_exit.3:55
msgid ""
"This function comes from SunOS 4, but is also present in libc4, libc5 and "
"glibc. It no longer occurs in Solaris (SunOS 5).  Avoid this function, and "
"use the standard B<atexit>(3)  instead."
msgstr ""

# type: SH
#: on_exit.3:55
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: on_exit.3:58
msgid "B<_exit>(3), B<atexit>(3), B<exit>(3)"
msgstr ""
