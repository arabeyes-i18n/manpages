# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: memset.3:28
#, no-wrap
msgid "MEMSET"
msgstr ""

# type: TH
#: memset.3:28
#, no-wrap
msgid "1993-04-11"
msgstr ""

# type: TH
#: memset.3:28
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: memset.3:28
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: memset.3:29
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: memset.3:31
msgid "memset - fill memory with a constant byte"
msgstr ""

# type: SH
#: memset.3:31
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: memset.3:34
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr ""

# type: Plain text
#: memset.3:36
#, no-wrap
msgid "B<void *memset(void *>I<s>B<, int >I<c>B<, size_t >I<n>B<);>\n"
msgstr ""

# type: SH
#: memset.3:37
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: memset.3:40
msgid ""
"The B<memset()> function fills the first I<n> bytes of the memory area "
"pointed to by I<s> with the constant byte I<c>."
msgstr ""

# type: SH
#: memset.3:40
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: memset.3:43
msgid "The B<memset()> function returns a pointer to the memory area I<s>."
msgstr ""

# type: SH
#: memset.3:43
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: memset.3:45
msgid "SVID 3, BSD 4.3, ISO 9899"
msgstr ""

# type: SH
#: memset.3:45
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: memset.3:48
msgid "B<bzero>(3), B<swab>(3), B<wmemset>(3)"
msgstr ""
