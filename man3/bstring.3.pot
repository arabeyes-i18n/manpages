# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: bstring.3:30
#, no-wrap
msgid "BSTRING"
msgstr ""

# type: TH
#: bstring.3:30
#, no-wrap
msgid "2002-01-20"
msgstr ""

# type: TH
#: bstring.3:30
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: bstring.3:31
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: bstring.3:34
msgid ""
"bcmp, bcopy, bzero, memccpy, memchr, memcmp, memcpy, memfrob, memmem, "
"memmove, memset - byte string operations"
msgstr ""

# type: SH
#: bstring.3:34
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: bstring.3:37
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr ""

# type: Plain text
#: bstring.3:39
#, no-wrap
msgid "B<int bcmp(const void *>I<s1>B<, const void *>I<s2>B<, int >I<n>B<);>\n"
msgstr ""

# type: Plain text
#: bstring.3:41
#, no-wrap
msgid "B<void bcopy(const void *>I<src>B<, void *>I<dest>B<, int >I<n>B<);>\n"
msgstr ""

# type: Plain text
#: bstring.3:43
#, no-wrap
msgid "B<void bzero(void *>I<s>B<, int >I<n>B<);>\n"
msgstr ""

# type: Plain text
#: bstring.3:45
#, no-wrap
msgid ""
"B<void *memccpy(void *>I<dest>B<, const void *>I<src>B<, int >I<c>B<, size_t "
">I<n>B<);>\n"
msgstr ""

# type: Plain text
#: bstring.3:47
#, no-wrap
msgid "B<void *memchr(const void *>I<s>B<, int >I<c>B<, size_t >I<n>B<);>\n"
msgstr ""

# type: Plain text
#: bstring.3:49
#, no-wrap
msgid "B<int memcmp(const void *>I<s1>B<, const void *>I<s2>B<, size_t >I<n>B<);>\n"
msgstr ""

# type: Plain text
#: bstring.3:51
#, no-wrap
msgid "B<void *memcpy(void *>I<dest>B<, const void *>I<src>B<, size_t >I<n>B<);>\n"
msgstr ""

# type: Plain text
#: bstring.3:53
#, no-wrap
msgid "B<void *memfrob(void *>I<s>B<, size_t >I<n>B<);>\n"
msgstr ""

# type: Plain text
#: bstring.3:55
#, no-wrap
msgid "B<void *memmem(const void *>I<needle>B<, size_t >I<needlelen>B<,>\n"
msgstr ""

# type: Plain text
#: bstring.3:57
#, no-wrap
msgid "B<const void *>I<haystack>B<, size_t >I<haystacklen>B<);>\n"
msgstr ""

# type: Plain text
#: bstring.3:60
#, no-wrap
msgid "B<void *memmove(void *>I<dest>B<, const void *>I<src>B<, size_t >I<n>B<);>\n"
msgstr ""

# type: Plain text
#: bstring.3:62
#, no-wrap
msgid "B<void *memset(void *>I<s>B<, int >I<c>B<, size_t >I<n>B<);>\n"
msgstr ""

# type: SH
#: bstring.3:63
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: bstring.3:67
msgid ""
"The byte string functions perform operations on strings (byte arrays)  that "
"are not necessarily NUL-terminated.  See the individual man pages for "
"descriptions of each function."
msgstr ""

# type: SH
#: bstring.3:67
#, no-wrap
msgid "NOTE"
msgstr ""

# type: Plain text
#: bstring.3:79
msgid ""
"The functions B<bcmp>(), B<bcopy>()  and B<bzero>()  are obsolete. Use "
"B<memcmp>(), B<memcpy>()  and B<memset>()  instead."
msgstr ""

# type: SH
#: bstring.3:80
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: bstring.3:91
msgid ""
"B<bcmp>(3), B<bcopy>(3), B<bzero>(3), B<memccpy>(3), B<memchr>(3), "
"B<memcmp>(3), B<memcpy>(3), B<memfrob>(3), B<memmem>(3), B<memmove>(3), "
"B<memset>(3)"
msgstr ""
