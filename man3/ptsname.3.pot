# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: ptsname.3:6
#, no-wrap
msgid "PTSNAME"
msgstr ""

# type: TH
#: ptsname.3:6
#, no-wrap
msgid "2004-12-17"
msgstr ""

# type: TH
#: ptsname.3:6
#, no-wrap
msgid "PTY Control"
msgstr ""

# type: TH
#: ptsname.3:6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: ptsname.3:7
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: ptsname.3:9
msgid "ptsname, ptsname_r - get the name of the slave pseudo-terminal"
msgstr ""

# type: SH
#: ptsname.3:9
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: ptsname.3:12
#, no-wrap
msgid "B<#define _XOPEN_SOURCE>\n"
msgstr ""

# type: Plain text
#: ptsname.3:14 ptsname.3:20
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr ""

# type: Plain text
#: ptsname.3:16
#, no-wrap
msgid "B<char *ptsname(int >I<fd>B<);>\n"
msgstr ""

# type: Plain text
#: ptsname.3:18
#, no-wrap
msgid "B<#define _GNU_SOURCE>\n"
msgstr ""

# type: Plain text
#: ptsname.3:22
#, no-wrap
msgid "B<char *ptsname_r(int >I<fd>B<, char * >I<buf>B<, size_t >I<buflen>B<);>\n"
msgstr ""

# type: SH
#: ptsname.3:23
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: ptsname.3:29
msgid ""
"The B<ptsname>()  function returns the name of the slave pseudo-terminal "
"(pty) device corresponding to the master referred to by I<fd>."
msgstr ""

# type: Plain text
#: ptsname.3:41
msgid ""
"The B<ptsname_r>()  function is the reentrant equivalent of B<ptsname>().  "
"It returns the name of the slave pseudo-terminal device as a null-terminated "
"string in the buffer pointed to by I<buf>.  The I<buflen> argument specifies "
"the number of bytes available in I<buf>."
msgstr ""

# type: SH
#: ptsname.3:41
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: ptsname.3:48
msgid ""
"On success, B<ptsname>()  returns a pointer to a string in static storage "
"which will be overwritten by subsequent calls.  This pointer must not be "
"freed.  On failure, a NULL pointer is returned."
msgstr ""

# type: Plain text
#: ptsname.3:56
msgid ""
"On success, B<ptsname_r>()  returns 0.  On failure, a non-zero value is "
"returned and I<errno> is set to indicate the error."
msgstr ""

# type: SH
#: ptsname.3:58
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: ptsname.3:59
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: ptsname.3:65
msgid "(B<ptsname_r>()  only)  I<buf> is NULL."
msgstr ""

# type: TP
#: ptsname.3:65
#, no-wrap
msgid "B<ENOTTY>"
msgstr ""

# type: Plain text
#: ptsname.3:69
msgid "I<fd> does not refer to a pseudo-terminal master device."
msgstr ""

# type: TP
#: ptsname.3:69
#, no-wrap
msgid "B<ERANGE>"
msgstr ""

# type: Plain text
#: ptsname.3:75
msgid "(B<ptsname_r>()  only)  I<buf> is too small."
msgstr ""

# type: SH
#: ptsname.3:75
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: ptsname.3:80
msgid ""
"B<ptsname>()  is part of the Unix98 pseudo-terminal support (see "
"B<pts>(4)).  This function is specified in POSIX 1003.1-2001."
msgstr ""

# type: Plain text
#: ptsname.3:88
msgid ""
"B<pstname_r>()  is a Linux extension.  A version of this function is "
"documented on Tru64 and HP-UX, but on those implementations, -1 is returned "
"on error, with I<errno> set to indicate the error.  Avoid using this "
"function in portable programs."
msgstr ""

# type: SH
#: ptsname.3:88
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: ptsname.3:93
msgid "B<grantpt>(3), B<posix_openpt>(3), B<ttyname>(3), B<unlockpt>(3), B<pts>(4)"
msgstr ""
