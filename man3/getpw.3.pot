# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: getpw.3:30
#, no-wrap
msgid "GETPW"
msgstr ""

# type: TH
#: getpw.3:30
#, no-wrap
msgid "1996-05-27"
msgstr ""

# type: TH
#: getpw.3:30
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: getpw.3:30
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: getpw.3:31
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: getpw.3:33
msgid "getpw - Re-construct password line entry"
msgstr ""

# type: SH
#: getpw.3:33
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: getpw.3:37
#, no-wrap
msgid ""
"B<#include E<lt>sys/types.hE<gt>>\n"
"B<#include E<lt>pwd.hE<gt>>\n"
msgstr ""

# type: Plain text
#: getpw.3:39
#, no-wrap
msgid "B<int getpw(uid_t >I<uid>B<, char *>I<buf>B<);>\n"
msgstr ""

# type: SH
#: getpw.3:40
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: getpw.3:44
msgid ""
"The B<getpw()> function re-constructs the password line entry for the given "
"user uid I<uid> in the buffer I<buf>.  The returned buffer contains a line "
"of format"
msgstr ""

# type: Plain text
#: getpw.3:47
msgid "B<name:passwd:uid:gid:gecos:dir:shell>"
msgstr ""

# type: Plain text
#: getpw.3:50
msgid "The I<passwd> structure is defined in I<E<lt>pwd.hE<gt>> as follows:"
msgstr ""

# type: Plain text
#: getpw.3:63
#, no-wrap
msgid ""
"struct passwd {\n"
"        char    *pw_name;\t\t/* user name */\n"
"        char    *pw_passwd;\t\t/* user password */\n"
"        uid_t   pw_uid;\t\t\t/* user id */\n"
"        gid_t   pw_gid;\t\t\t/* group id */\n"
"        char    *pw_gecos;      \t/* real name */\n"
"        char    *pw_dir;  \t\t/* home directory */\n"
"        char    *pw_shell;      \t/* shell program */\n"
"};\n"
msgstr ""

# type: SH
#: getpw.3:66
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: getpw.3:69
msgid "The B<getpw()> function returns 0 on success, or -1 if an error occurs."
msgstr ""

# type: SH
#: getpw.3:69
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: getpw.3:70
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

# type: Plain text
#: getpw.3:73
msgid "Insufficient memory to allocate passwd structure."
msgstr ""

# type: SH
#: getpw.3:73
#, no-wrap
msgid "FILES"
msgstr ""

# type: TP
#: getpw.3:74
#, no-wrap
msgid "I</etc/passwd>"
msgstr ""

# type: Plain text
#: getpw.3:77
msgid "password database file"
msgstr ""

# type: SH
#: getpw.3:77
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: getpw.3:79
msgid "SYSVr2."
msgstr ""

# type: SH
#: getpw.3:79
#, no-wrap
msgid "BUGS"
msgstr ""

# type: Plain text
#: getpw.3:86
msgid ""
"The B<getpw>()  function is dangerous as it may overflow the provided buffer "
"I<buf>.  It is obsoleted by B<getpwuid>()."
msgstr ""

# type: SH
#: getpw.3:86
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: getpw.3:94
msgid ""
"B<endpwent>(3), B<fgetpwent>(3), B<getpwent>(3), B<getpwnam>(3), "
"B<getpwuid>(3), B<putpwent>(3), B<setpwent>(3), B<passwd>(5)"
msgstr ""
