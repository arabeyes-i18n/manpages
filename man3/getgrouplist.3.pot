# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: getgrouplist.3:6
#, no-wrap
msgid "GETGROUPLIST"
msgstr ""

# type: TH
#: getgrouplist.3:6
#, no-wrap
msgid "2003-11-18"
msgstr ""

# type: TH
#: getgrouplist.3:6
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: getgrouplist.3:6
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: getgrouplist.3:7
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: getgrouplist.3:9
msgid "getgrouplist - list of groups a user belongs to"
msgstr ""

# type: SH
#: getgrouplist.3:9
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: getgrouplist.3:12
msgid "B<#include E<lt>grp.hE<gt>>"
msgstr ""

# type: Plain text
#: getgrouplist.3:14
msgid "B<int getgrouplist (const char *>I<user>B<, gid_t >I<group>B<,>"
msgstr ""

# type: Plain text
#: getgrouplist.3:16
msgid "B<gid_t *>I<groups>B<, int *>I<ngroups>B<);>"
msgstr ""

# type: SH
#: getgrouplist.3:16
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: getgrouplist.3:30
msgid ""
"The B<getgrouplist()> function scans the group database for all the groups "
"I<user> belongs to.  Up to *I<ngroups> group IDs corresponding to these "
"groups are stored in the array I<groups>; the return value from the function "
"is the number of group IDs actually stored. The group I<group> is "
"automatically included in the list of groups returned by "
"B<getgroup\\%list()>."
msgstr ""

# type: SH
#: getgrouplist.3:30
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: getgrouplist.3:38
msgid ""
"If *I<ngroups> is smaller than the total number of groups found, then "
"B<getgrouplist()> returns a value of `-1'.  In all cases the actual number "
"of groups is stored in *I<ngroups>."
msgstr ""

# type: SH
#: getgrouplist.3:38
#, no-wrap
msgid "BUGS"
msgstr ""

# type: Plain text
#: getgrouplist.3:42
msgid ""
"The glibc 2.3.2 implementation of this function is broken: it overwrites "
"memory when the actual number of groups is larger than *I<ngroups>."
msgstr ""

# type: SH
#: getgrouplist.3:42
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: getgrouplist.3:44
msgid "This function is present since glibc 2.2.4."
msgstr ""

# type: SH
#: getgrouplist.3:44
#, no-wrap
msgid "EXAMPLE"
msgstr ""

# type: Plain text
#: getgrouplist.3:51
#, no-wrap
msgid ""
"/* This crashes with glibc 2.3.2 */\n"
"#include E<lt>stdio.hE<gt>\n"
"#include E<lt>stdlib.hE<gt>\n"
"#include E<lt>grp.hE<gt>\n"
"#include E<lt>pwd.hE<gt>\n"
msgstr ""

# type: Plain text
#: getgrouplist.3:59
#, no-wrap
msgid ""
"int main() {\n"
"        int i, ng = 0;\n"
"        char *user = \"who\";\t/* username here */\n"
"        gid_t *groups = NULL;\n"
"        struct passwd *pw = getpwnam(user);\n"
"        if (pw == NULL)\n"
"                return 0;\n"
msgstr ""

# type: Plain text
#: getgrouplist.3:64
#, no-wrap
msgid ""
"        if (getgrouplist(user, pw-E<gt>pw_gid, NULL, &ng) E<lt> 0) {\n"
"                groups = (gid_t *) malloc(ng * sizeof (gid_t));\n"
"                getgrouplist(user, pw-E<gt>pw_gid, groups, &ng);\n"
"        }\n"
msgstr ""

# type: Plain text
#: getgrouplist.3:67
#, no-wrap
msgid ""
"        for(i = 0; i E<lt> ng; i++)\n"
"                printf(\"%d\\en\", groups[i]);\n"
msgstr ""

# type: Plain text
#: getgrouplist.3:70
#, no-wrap
msgid ""
"        return 0;\n"
"}\n"
msgstr ""

# type: SH
#: getgrouplist.3:71
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: getgrouplist.3:73
msgid "B<getgroups>(3), B<setgroups>(3)"
msgstr ""
