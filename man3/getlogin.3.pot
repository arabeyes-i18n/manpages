# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: getlogin.3:28
#, no-wrap
msgid "GETLOGIN"
msgstr ""

# type: TH
#: getlogin.3:28
#, no-wrap
msgid "2003-08-24"
msgstr ""

# type: TH
#: getlogin.3:28
#, no-wrap
msgid "Linux 2.4"
msgstr ""

# type: TH
#: getlogin.3:28
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: getlogin.3:29
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: getlogin.3:31
msgid "getlogin, getlogin_r, cuserid - get user name"
msgstr ""

# type: SH
#: getlogin.3:31
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: getlogin.3:33
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr ""

# type: Plain text
#: getlogin.3:35
msgid "B<char *getlogin(void);>"
msgstr ""

# type: Plain text
#: getlogin.3:37
msgid "B<int getlogin_r(char *>I<buf>B<, size_t >I<bufsize>B<);>"
msgstr ""

# type: Plain text
#: getlogin.3:39
msgid "B<#include E<lt>stdio.hE<gt>>"
msgstr ""

# type: Plain text
#: getlogin.3:41
msgid "B<char *cuserid(char *>I<string>B<);>"
msgstr ""

# type: SH
#: getlogin.3:41
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: getlogin.3:47
msgid ""
"B<getlogin> returns a pointer to a string containing the name of the user "
"logged in on the controlling terminal of the process, or a null pointer if "
"this information cannot be determined.  The string is statically allocated "
"and might be overwritten on subsequent calls to this function or to "
"B<cuserid>."
msgstr ""

# type: Plain text
#: getlogin.3:52
msgid ""
"B<getlogin_r> returns this same user name in the array I<buf> of size "
"I<bufsize>."
msgstr ""

# type: Plain text
#: getlogin.3:60
msgid ""
"B<cuserid> returns a pointer to a string containing a user name associated "
"with the effective user ID of the process.  If I<string> is not a null "
"pointer, it should be an array that can hold at least B<L_cuserid> "
"characters; the string is returned in this array.  Otherwise, a pointer to a "
"string in a static area is returned. This string is statically allocated and "
"might be overwritten on subsequent calls to this function or to B<getlogin>."
msgstr ""

# type: Plain text
#: getlogin.3:64
msgid ""
"The macro B<L_cuserid> is an integer constant that indicates how long an "
"array you might need to store a user name.  B<L_cuserid> is declared in "
"B<stdio.h>."
msgstr ""

# type: Plain text
#: getlogin.3:69
msgid ""
"These functions let your program identify positively the user who is running "
"(B<cuserid>) or the user who logged in this session (B<getlogin>).  (These "
"can differ when setuid programs are involved.)"
msgstr ""

# type: Plain text
#: getlogin.3:73
msgid ""
"For most purposes, it is more useful to use the environment variable "
"B<LOGNAME> to find out who the user is.  This is more flexible precisely "
"because the user can set B<LOGNAME> arbitrarily."
msgstr ""

# type: SH
#: getlogin.3:73
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: getlogin.3:77
msgid ""
"B<getlogin> returns a pointer to the user name when successful, and NULL on "
"failure.  B<getlogin_r> returns 0 when successful, and nonzero on failure."
msgstr ""

# type: SH
#: getlogin.3:77
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: Plain text
#: getlogin.3:79
msgid "POSIX specifies"
msgstr ""

# type: TP
#: getlogin.3:79
#, no-wrap
msgid "B<EMFILE>"
msgstr ""

# type: Plain text
#: getlogin.3:82
msgid "The calling process already has the maximum allowed number of open files."
msgstr ""

# type: TP
#: getlogin.3:82
#, no-wrap
msgid "B<ENFILE>"
msgstr ""

# type: Plain text
#: getlogin.3:85
msgid "The system already has the maximum allowed number of open files."
msgstr ""

# type: TP
#: getlogin.3:85
#, no-wrap
msgid "B<ENXIO>"
msgstr ""

# type: Plain text
#: getlogin.3:88
msgid "The calling process has no controlling tty."
msgstr ""

# type: TP
#: getlogin.3:88
#, no-wrap
msgid "B<ERANGE>"
msgstr ""

# type: Plain text
#: getlogin.3:93
msgid ""
"(getlogin_r)  The length of the user name, including final NUL, is larger "
"than I<bufsize>."
msgstr ""

# type: Plain text
#: getlogin.3:95
msgid "Linux/glibc also has"
msgstr ""

# type: TP
#: getlogin.3:95
#, no-wrap
msgid "B<ENOENT>"
msgstr ""

# type: Plain text
#: getlogin.3:98
msgid "There was no corresponding entry in the utmp-file."
msgstr ""

# type: TP
#: getlogin.3:98
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

# type: Plain text
#: getlogin.3:101
msgid "Insufficient memory to allocate passwd structure."
msgstr ""

# type: SH
#: getlogin.3:101
#, no-wrap
msgid "FILES"
msgstr ""

# type: Plain text
#: getlogin.3:104
#, no-wrap
msgid "I</etc/passwd>\tpassword database file\n"
msgstr ""

# type: Plain text
#: getlogin.3:107
#, no-wrap
msgid ""
"I</var/run/utmp>\t(traditionally I</etc/utmp>;\n"
"\t\t\tsome libc versions used I</var/adm/utmp>)\n"
msgstr ""

# type: SH
#: getlogin.3:108
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: getlogin.3:112
msgid ""
"POSIX.1.  System V has a B<cuserid> function which uses the real user ID "
"rather than the effective user ID. The B<cuserid> function was included in "
"the 1988 version of POSIX, but removed from the 1990 version."
msgstr ""

# type: Plain text
#: getlogin.3:115
msgid ""
"OpenBSD has B<getlogin> and B<setlogin>, and a username associated with a "
"session, even if it has no controlling tty."
msgstr ""

# type: SH
#: getlogin.3:115
#, no-wrap
msgid "BUGS"
msgstr ""

# type: Plain text
#: getlogin.3:122
msgid ""
"Unfortunately, it is often rather easy to fool getlogin().  Sometimes it "
"does not work at all, because some program messed up the utmp file. Often, "
"it gives only the first 8 characters of the login name. The user currently "
"logged in on the controlling tty of our program need not be the user who "
"started it.  Avoid getlogin() for security-related purposes."
msgstr ""

# type: Plain text
#: getlogin.3:128
msgid ""
"Note that glibc does not follow the POSIX spec and uses stdin instead of "
"I</dev/tty>.  A bug. (Other recent systems, like SunOS 5.8 and HPUX 11.11 "
"and FreeBSD 4.8 all return the login name also when stdin is redirected.)"
msgstr ""

# type: Plain text
#: getlogin.3:133
msgid ""
"Nobody knows precisely what cuserid() does - avoid it in portable programs - "
"avoid it altogether - use getpwuid(geteuid()) instead, if that is what you "
"meant.  DO NOT USE cuserid()."
msgstr ""

# type: SH
#: getlogin.3:133
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: getlogin.3:135
msgid "B<geteuid>(2), B<getuid>(2)"
msgstr ""
