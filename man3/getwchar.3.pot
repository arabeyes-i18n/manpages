# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: getwchar.3:15
#, no-wrap
msgid "GETWCHAR"
msgstr ""

# type: TH
#: getwchar.3:15
#, no-wrap
msgid "1999-07-25"
msgstr ""

# type: TH
#: getwchar.3:15
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: getwchar.3:15
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: getwchar.3:16
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: getwchar.3:18
msgid "getwchar - read a wide character from standard input"
msgstr ""

# type: SH
#: getwchar.3:18
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: getwchar.3:21
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr ""

# type: Plain text
#: getwchar.3:23
#, no-wrap
msgid "B<wint_t getwchar(void);>\n"
msgstr ""

# type: SH
#: getwchar.3:24
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: getwchar.3:30
msgid ""
"The B<getwchar> function is the wide-character equivalent of the B<getchar> "
"function. It reads a wide character from B<stdin> and returns it. If the end "
"of stream is reached, or if I<ferror(stdin)> becomes true, it returns "
"WEOF. If a wide character conversion error occurs, it sets B<errno> to "
"B<EILSEQ> and returns WEOF."
msgstr ""

# type: Plain text
#: getwchar.3:33
msgid "For a non-locking counterpart, see B<unlocked_stdio>(3)."
msgstr ""

# type: SH
#: getwchar.3:33
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: getwchar.3:36
msgid ""
"The B<getwchar> function returns the next wide-character from standard "
"input, or WEOF."
msgstr ""

# type: SH
#: getwchar.3:36
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: getwchar.3:38
msgid "ISO/ANSI C, UNIX98"
msgstr ""

# type: SH
#: getwchar.3:38
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: getwchar.3:41
msgid ""
"The behaviour of B<getwchar> depends on the LC_CTYPE category of the current "
"locale."
msgstr ""

# type: Plain text
#: getwchar.3:44
msgid ""
"It is reasonable to expect that B<getwchar> will actually read a multibyte "
"sequence from standard input and then convert it to a wide character."
msgstr ""

# type: SH
#: getwchar.3:44
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: getwchar.3:46
msgid "B<fgetwc>(3), B<unlocked_stdio>(3)"
msgstr ""
