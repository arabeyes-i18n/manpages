# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: aio_error.3:23
#, no-wrap
msgid "AIO_ERROR"
msgstr ""

# type: TH
#: aio_error.3:23
#, no-wrap
msgid "2003-11-14"
msgstr ""

# type: TH
#: aio_error.3:23
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: aio_error.3:24
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: aio_error.3:26
msgid "aio_error - get error status of asynchronous I/O operation"
msgstr ""

# type: SH
#: aio_error.3:26
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: aio_error.3:29
msgid "B<#include E<lt>aio.hE<gt>>"
msgstr ""

# type: Plain text
#: aio_error.3:31
msgid "B<int aio_error(const struct aiocb *>I<aiocbp>B<);>"
msgstr ""

# type: SH
#: aio_error.3:32
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: aio_error.3:38
msgid ""
"The B<aio_error> function returns the error status for the asynchronous I/O "
"request with control block pointed to by I<aiocbp>."
msgstr ""

# type: SH
#: aio_error.3:38
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: aio_error.3:53
msgid ""
"This function returns EINPROGRESS if the request has not been completed "
"yet. It returns ECANCELED if the request was cancelled.  It returns 0 if the "
"request completed successfully.  Otherwise an error value is returned, the "
"same value that would have been stored in the I<errno> variable in case of a "
"synchronous I<read>, I<write>, I<fsync>, or I<fdatasync> request.  On error, "
"the error value is returned."
msgstr ""

# type: SH
#: aio_error.3:53
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: aio_error.3:54
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: aio_error.3:61
msgid ""
"I<aiocbp> does not point at a control block for an asynchronous I/O request "
"of which the return status (see B<aio_return>(3))  has not been retrieved "
"yet."
msgstr ""

# type: SH
#: aio_error.3:61
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: aio_error.3:63
msgid "POSIX 1003.1-2003"
msgstr ""

# type: SH
#: aio_error.3:63
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: aio_error.3:69
msgid ""
"B<aio_cancel>(3), B<aio_fsync>(3), B<aio_read>(3), B<aio_return>(3), "
"B<aio_suspend>(3), B<aio_write>(3)"
msgstr ""
