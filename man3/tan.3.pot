# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:10+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: tan.3:31
#, no-wrap
msgid "TAN"
msgstr ""

# type: TH
#: tan.3:31
#, no-wrap
msgid "2002-07-27"
msgstr ""

# type: TH
#: tan.3:31
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: tan.3:32
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: tan.3:34
msgid "tan, tanf, tanl - tangent function"
msgstr ""

# type: SH
#: tan.3:34
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: tan.3:37
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr ""

# type: Plain text
#: tan.3:39
#, no-wrap
msgid "B<double tan(double >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: tan.3:41
#, no-wrap
msgid "B<float tanf(float >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: tan.3:43
#, no-wrap
msgid "B<long double tanl(long double >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: tan.3:46
msgid "Link with -lm."
msgstr ""

# type: SH
#: tan.3:46
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: tan.3:49
msgid ""
"The B<tan()> function returns the tangent of I<x>, where I<x> is given in "
"radians."
msgstr ""

# type: SH
#: tan.3:49
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: tan.3:52
msgid ""
"SVID 3, POSIX, BSD 4.3, ISO 9899.  The float and the long double variants "
"are C99 requirements."
msgstr ""

# type: SH
#: tan.3:52
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: tan.3:59
msgid ""
"B<acos>(3), B<asin>(3), B<atan>(3), B<atan2>(3), B<cos>(3), B<ctan>(3), "
"B<sin>(3)"
msgstr ""
