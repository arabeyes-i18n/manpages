# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: clog.3:4
#, no-wrap
msgid "CLOG"
msgstr ""

# type: TH
#: clog.3:4
#, no-wrap
msgid "2002-07-28"
msgstr ""

# type: TH
#: clog.3:4
#, no-wrap
msgid "complex math routines"
msgstr ""

# type: SH
#: clog.3:5
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: clog.3:7
msgid "clog, clogf, clogl - natural logarithm of a complex number"
msgstr ""

# type: SH
#: clog.3:7
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: clog.3:9
msgid "B<#include E<lt>complex.hE<gt>>"
msgstr ""

# type: Plain text
#: clog.3:11
msgid "B<double complex clog(double complex >I<z>B<);>"
msgstr ""

# type: Plain text
#: clog.3:13
msgid "B<float complex clogf(float complex >I<z>B<);>"
msgstr ""

# type: Plain text
#: clog.3:15
msgid "B<long double complex clogl(long double complex >I<z>B<);>"
msgstr ""

# type: Plain text
#: clog.3:17
msgid "Link with -lm."
msgstr ""

# type: SH
#: clog.3:17
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: clog.3:21
msgid ""
"The logarithm clog is the inverse function of the exponential cexp.  Thus, "
"if y = clog(z), then z = cexp(y).  The imaginary part of y is chosen in the "
"interval [-pi,pi]."
msgstr ""

# type: Plain text
#: clog.3:23
msgid "One has clog(z) = log(cabs(z))+I*carg(z)."
msgstr ""

# type: Plain text
#: clog.3:25
msgid "Please note that z close to zero will cause an overflow."
msgstr ""

# type: SH
#: clog.3:25
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: clog.3:27
msgid "C99"
msgstr ""

# type: SH
#: clog.3:27
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: clog.3:31
msgid "B<cabs>(3), B<cexp>(3), B<clog10>(3), B<complex>(5)"
msgstr ""
