# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:10+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: wcrtomb.3:14
#, no-wrap
msgid "WCRTOMB"
msgstr ""

# type: TH
#: wcrtomb.3:14
#, no-wrap
msgid "1999-07-25"
msgstr ""

# type: TH
#: wcrtomb.3:14
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: wcrtomb.3:14
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: wcrtomb.3:15
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: wcrtomb.3:17
msgid "wcrtomb - convert a wide character to a multibyte sequence"
msgstr ""

# type: SH
#: wcrtomb.3:17
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: wcrtomb.3:20
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr ""

# type: Plain text
#: wcrtomb.3:22
#, no-wrap
msgid "B<size_t wcrtomb(char *>I<s>B<, wchar_t >I<wc>B<, mbstate_t *>I<ps>B<);>\n"
msgstr ""

# type: SH
#: wcrtomb.3:23
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: wcrtomb.3:31
msgid ""
"The main case for this function is when I<s> is not NULL and I<wc> is not "
"L'\\e0'.  In this case, the B<wcrtomb> function converts the wide character "
"I<wc> to its multibyte representation and stores it at the beginning of the "
"character array pointed to by I<s>. It updates the shift state I<*ps>, and "
"returns the length of said multibyte representation, i.e. the number of "
"bytes written at I<s>."
msgstr ""

# type: Plain text
#: wcrtomb.3:38
msgid ""
"A different case is when I<s> is not NULL but I<wc> is L'\\e0'. In this case "
"the B<wcrtomb> function stores at the character array pointed to by I<s> the "
"shift sequence needed to bring I<*ps> back to the initial state, followed by "
"a '\\e0' byte. It updates the shift state I<*ps> (i.e. brings it into the "
"initial state), and returns the length of the shift sequence plus one, "
"i.e. the number of bytes written at I<s>."
msgstr ""

# type: Plain text
#: wcrtomb.3:42
msgid ""
"A third case is when I<s> is NULL. In this case I<wc> is ignored, and the "
"function effectively returns wcrtomb(buf,L'\\e0',I<ps>) where buf is an "
"internal anonymous buffer."
msgstr ""

# type: Plain text
#: wcrtomb.3:45
msgid ""
"In all of the above cases, if I<ps> is a NULL pointer, a static anonymous "
"state only known to the wcrtomb function is used instead."
msgstr ""

# type: SH
#: wcrtomb.3:45
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: wcrtomb.3:50
msgid ""
"The B<wcrtomb> function returns the number of bytes that have been or would "
"have been written to the byte array at I<s>. If I<wc> can not be represented "
"as a multibyte sequence (according to the current locale), (size_t)(-1) is "
"returned, and B<errno> set to B<EILSEQ>."
msgstr ""

# type: SH
#: wcrtomb.3:50
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: wcrtomb.3:52
msgid "ISO/ANSI C, UNIX98"
msgstr ""

# type: SH
#: wcrtomb.3:52
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: wcrtomb.3:54
msgid "B<wcsrtombs>(3)"
msgstr ""

# type: SH
#: wcrtomb.3:54
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: wcrtomb.3:57
msgid ""
"The behaviour of B<wcrtomb> depends on the LC_CTYPE category of the current "
"locale."
msgstr ""

# type: Plain text
#: wcrtomb.3:58
msgid "Passing NULL as I<ps> is not multi-thread safe."
msgstr ""
