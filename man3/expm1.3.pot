# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:07+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: expm1.3:26
#, no-wrap
msgid "EXPM1"
msgstr ""

# type: TH
#: expm1.3:26
#, no-wrap
msgid "2002-07-27"
msgstr ""

# type: TH
#: expm1.3:26
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: expm1.3:27
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: expm1.3:29
msgid "expm1, expm1f, expm1l - exponential minus 1"
msgstr ""

# type: SH
#: expm1.3:29
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: expm1.3:32
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr ""

# type: Plain text
#: expm1.3:34
#, no-wrap
msgid "B<double expm1(double >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: expm1.3:36
#, no-wrap
msgid "B<float expm1f(float >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: expm1.3:38
#, no-wrap
msgid "B<long double expm1l(long double >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: expm1.3:41
msgid "Link with -lm."
msgstr ""

# type: SH
#: expm1.3:41
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: expm1.3:47
msgid ""
"B<expm1(>I<x>B<)> returns a value equivalent to `exp (I<x>) - 1'. It is "
"computed in a way that is accurate even if the value of I<x> is near zero--a "
"case where `exp (I<x>) - 1' would be inaccurate due to subtraction of two "
"numbers that are nearly equal."
msgstr ""

# type: SH
#: expm1.3:47
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: expm1.3:50
msgid "BSD, C99.  The float and the long double variants are C99 requirements."
msgstr ""

# type: SH
#: expm1.3:50
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: expm1.3:53
msgid "B<exp>(3), B<log>(3), B<log1p>(3)"
msgstr ""
