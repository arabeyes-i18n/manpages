# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:10+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: wcsnlen.3:13
#, no-wrap
msgid "WCSNLEN"
msgstr ""

# type: TH
#: wcsnlen.3:13
#, no-wrap
msgid "1999-07-25"
msgstr ""

# type: TH
#: wcsnlen.3:13
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: wcsnlen.3:13
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: wcsnlen.3:14
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: wcsnlen.3:16
msgid "wcsnlen - determine the length of a fixed-size wide-character string"
msgstr ""

# type: SH
#: wcsnlen.3:16
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: wcsnlen.3:19
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr ""

# type: Plain text
#: wcsnlen.3:21
#, no-wrap
msgid "B<size_t wcsnlen(const wchar_t *>I<s>B<, size_t >I<maxlen>B<);>\n"
msgstr ""

# type: SH
#: wcsnlen.3:22
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: wcsnlen.3:28
msgid ""
"The B<wcsnlen> function is the wide-character equivalent of the B<strnlen> "
"function. It returns the number of wide-characters in the string pointed to "
"by I<s>, not including the terminating L'\\e0' character, but at most "
"I<maxlen>. In doing this, B<wcsnlen> looks only at the first I<maxlen> "
"wide-characters at I<s> and never beyond I<s+maxlen>."
msgstr ""

# type: SH
#: wcsnlen.3:28
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: wcsnlen.3:32
msgid ""
"The B<wcsnlen> function returns I<wcslen(s)>, if that is less than "
"I<maxlen>, or I<maxlen> if there is no L'\\e0' character among the first "
"I<maxlen> wide characters pointed to by I<s>."
msgstr ""

# type: SH
#: wcsnlen.3:32
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: wcsnlen.3:34
msgid "This function is a GNU extension."
msgstr ""

# type: SH
#: wcsnlen.3:34
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: wcsnlen.3:36
msgid "B<strnlen>(3), B<wcslen>(3)"
msgstr ""
