# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: memcpy.3:28
#, no-wrap
msgid "MEMCPY"
msgstr ""

# type: TH
#: memcpy.3:28
#, no-wrap
msgid "1993-04-10"
msgstr ""

# type: TH
#: memcpy.3:28
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: memcpy.3:29
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: memcpy.3:31
msgid "memcpy - copy memory area"
msgstr ""

# type: SH
#: memcpy.3:31
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: memcpy.3:34
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr ""

# type: Plain text
#: memcpy.3:36
#, no-wrap
msgid "B<void *memcpy(void *>I<dest>B<, const void *>I<src>B<, size_t >I<n>B<);>\n"
msgstr ""

# type: SH
#: memcpy.3:37
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: memcpy.3:41
msgid ""
"The B<memcpy()> function copies I<n> bytes from memory area I<src> to memory "
"area I<dest>.  The memory areas should not overlap.  Use B<memmove>(3) if "
"the memory areas do overlap."
msgstr ""

# type: SH
#: memcpy.3:41
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: memcpy.3:43
msgid "The B<memcpy()> function returns a pointer to I<dest>."
msgstr ""

# type: SH
#: memcpy.3:43
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: memcpy.3:45
msgid "SVID 3, BSD 4.3, ISO 9899"
msgstr ""

# type: SH
#: memcpy.3:45
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: memcpy.3:52
msgid ""
"B<bcopy>(3), B<memccpy>(3), B<memmove>(3), B<mempcpy>(3), B<strcpy>(3), "
"B<strncpy>(3), B<wmemcpy>(3)"
msgstr ""
