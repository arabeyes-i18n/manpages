# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: killpg.3:26
#, no-wrap
msgid "KILLPG"
msgstr ""

# type: TH
#: killpg.3:26
#, no-wrap
msgid "1993-04-04"
msgstr ""

# type: TH
#: killpg.3:26
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: killpg.3:26
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: killpg.3:27
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: killpg.3:29
msgid "killpg - send signal to all members of a process group"
msgstr ""

# type: SH
#: killpg.3:29
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: killpg.3:32
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr ""

# type: Plain text
#: killpg.3:34
#, no-wrap
msgid "B<int killpg(pid_t >I<pgrp>B<, int >I<signal>B<);>\n"
msgstr ""

# type: SH
#: killpg.3:35
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: killpg.3:45
msgid ""
"The B<killpg()> function causes signal I<signal> to be sent to all the "
"processes in the process group I<pgrp> or to the processes' own process "
"group if I<pgrp> is equal to zero."
msgstr ""

# type: Plain text
#: killpg.3:49
msgid "If I<pgrp> is greater than 1, it is equivalent to"
msgstr ""

# type: Plain text
#: killpg.3:52
#, no-wrap
msgid "B<kill(->I<pgrp>B<,>I<signal>B<);>\n"
msgstr ""

# type: SH
#: killpg.3:53
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: killpg.3:55
msgid "The value returned is -1 on error, or 0 for success."
msgstr ""

# type: SH
#: killpg.3:55
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: Plain text
#: killpg.3:59
msgid "Errors are returned in I<errno> and can be one of the following:"
msgstr ""

# type: TP
#: killpg.3:59
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

# type: Plain text
#: killpg.3:62
msgid "for an invalid signal,"
msgstr ""

# type: TP
#: killpg.3:62
#, no-wrap
msgid "B<EPERM>"
msgstr ""

# type: Plain text
#: killpg.3:67
msgid ""
"if the userid of the calling process is not equal to that of the process the "
"signal is sent to, and the userid is not that of the superuser."
msgstr ""

# type: TP
#: killpg.3:67
#, no-wrap
msgid "B<ESRCH>"
msgstr ""

# type: Plain text
#: killpg.3:70
msgid "for a process group which does not exist, and"
msgstr ""

# type: SH
#: killpg.3:70
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: killpg.3:72
msgid "SUSv2, POSIX.1-2001"
msgstr ""

# type: SH
#: killpg.3:72
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: killpg.3:76
msgid "B<kill>(2), B<signal>(2), B<sigqueue>(2), B<signal>(7)"
msgstr ""
