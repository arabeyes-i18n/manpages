# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: iswgraph.3:14
#, no-wrap
msgid "ISWGRAPH"
msgstr ""

# type: TH
#: iswgraph.3:14
#, no-wrap
msgid "1999-07-25"
msgstr ""

# type: TH
#: iswgraph.3:14
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: iswgraph.3:14
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: iswgraph.3:15
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: iswgraph.3:17
msgid "iswgraph - test for graphic wide character"
msgstr ""

# type: SH
#: iswgraph.3:17
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: iswgraph.3:20
#, no-wrap
msgid "B<#include E<lt>wctype.hE<gt>>\n"
msgstr ""

# type: Plain text
#: iswgraph.3:22
#, no-wrap
msgid "B<int iswgraph(wint_t >I<wc>B<);>\n"
msgstr ""

# type: SH
#: iswgraph.3:23
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: iswgraph.3:27
msgid ""
"The B<iswgraph> function is the wide-character equivalent of the B<isgraph> "
"function. It tests whether I<wc> is a wide character belonging to the wide "
"character class \"graph\"."
msgstr ""

# type: Plain text
#: iswgraph.3:30
msgid ""
"The wide character class \"graph\" is a subclass of the wide character class "
"\"print\"."
msgstr ""

# type: Plain text
#: iswgraph.3:33
msgid ""
"Being a subclass of the wide character class \"print\", the wide character "
"class \"graph\" is disjoint from the wide character class \"cntrl\"."
msgstr ""

# type: Plain text
#: iswgraph.3:36
msgid ""
"The wide character class \"graph\" is disjoint from the wide character class "
"\"space\" and therefore also disjoint from its subclass \"blank\"."
msgstr ""

# type: Plain text
#: iswgraph.3:43
msgid ""
"The wide character class \"graph\" contains all the wide characters from the "
"wide character class \"print\" except the space character. It therefore "
"contains the wide character classes \"alnum\" and \"punct\"."
msgstr ""

# type: SH
#: iswgraph.3:43
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: iswgraph.3:46
msgid ""
"The B<iswgraph> function returns non-zero if I<wc> is a wide character "
"belonging to the wide character class \"graph\". Otherwise it returns zero."
msgstr ""

# type: SH
#: iswgraph.3:46
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: iswgraph.3:48
msgid "ISO/ANSI C, UNIX98"
msgstr ""

# type: SH
#: iswgraph.3:48
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: iswgraph.3:51
msgid "B<isgraph>(3), B<iswctype>(3)"
msgstr ""

# type: SH
#: iswgraph.3:51
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: iswgraph.3:53
msgid ""
"The behaviour of B<iswgraph> depends on the LC_CTYPE category of the current "
"locale."
msgstr ""
