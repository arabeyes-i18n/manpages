# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: resolver.3:30
#, no-wrap
msgid "RESOLVER"
msgstr ""

# type: TH
#: resolver.3:30
#, no-wrap
msgid "2004-10-31"
msgstr ""

# type: TH
#: resolver.3:30
#, no-wrap
msgid "BSD"
msgstr ""

# type: TH
#: resolver.3:30
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: resolver.3:31
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: resolver.3:34
msgid ""
"res_init, res_query, res_search, res_querydomain, res_mkquery, res_send, "
"dn_comp, dn_expand - resolver routines"
msgstr ""

# type: SH
#: resolver.3:34
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: resolver.3:40
#, no-wrap
msgid ""
"B<#include E<lt>netinet/in.hE<gt>>\n"
"B<#include E<lt>arpa/nameser.hE<gt>>\n"
"B<#include E<lt>resolv.hE<gt>>\n"
"B<extern struct state _res;>\n"
msgstr ""

# type: Plain text
#: resolver.3:42
#, no-wrap
msgid "B<int res_init(void);>\n"
msgstr ""

# type: Plain text
#: resolver.3:44
#, no-wrap
msgid "B<int res_query(const char *>I<dname>B<, int >I<class>B<, int >I<type>B<,>\n"
msgstr ""

# type: Plain text
#: resolver.3:46 resolver.3:51
#, no-wrap
msgid "B<unsigned char *>I<answer>B<, int >I<anslen>B<);>\n"
msgstr ""

# type: Plain text
#: resolver.3:49
#, no-wrap
msgid ""
"B<int res_search(const char *>I<dname>B<, int >I<class>B<, int "
">I<type>B<,>\n"
msgstr ""

# type: Plain text
#: resolver.3:54
#, no-wrap
msgid "B<int res_querydomain(const char *>I<name>B<, const char *>I<domain>B<,>\n"
msgstr ""

# type: Plain text
#: resolver.3:57
#, no-wrap
msgid ""
"B<int >I<class>B<, int >I<type>B<, unsigned char *>I<answer>B<,>\n"
"B<int >I<anslen>B<);>\n"
msgstr ""

# type: Plain text
#: resolver.3:60
#, no-wrap
msgid "B<int res_mkquery(int >I<op>B<, const char *>I<dname>B<, int >I<class>B<,>\n"
msgstr ""

# type: Plain text
#: resolver.3:63
#, no-wrap
msgid ""
"B<int >I<type>B<, char *>I<data>B<, int >I<datalen>B<, struct rrec "
"*>I<newrr>B<,>\n"
"B<char *>I<buf>B<, int >I<buflen>B<);>\n"
msgstr ""

# type: Plain text
#: resolver.3:66
#, no-wrap
msgid ""
"B<int res_send(const char *>I<msg>B<, int >I<msglen>B<, char "
"*>I<answer>B<,>\n"
msgstr ""

# type: Plain text
#: resolver.3:68
#, no-wrap
msgid "B<int >I<anslen>B<);>\n"
msgstr ""

# type: Plain text
#: resolver.3:71
#, no-wrap
msgid "B<int dn_comp(unsigned char *>I<exp_dn>B<, unsigned char *>I<comp_dn>B<,>\n"
msgstr ""

# type: Plain text
#: resolver.3:74
#, no-wrap
msgid ""
"B<int >I<length>B<, unsigned char **>I<dnptrs>B<, unsigned char "
"*>I<exp_dn>B<,>\n"
"B<unsigned char **>I<lastdnptr>B<);>\n"
msgstr ""

# type: Plain text
#: resolver.3:77
#, no-wrap
msgid "B<int dn_expand(unsigned char *>I<msg>B<, unsigned char *>I<eomorig>B<,>\n"
msgstr ""

# type: Plain text
#: resolver.3:80
#, no-wrap
msgid ""
"B<unsigned char *>I<comp_dn>B<, unsigned char *>I<exp_dn>B<,>\n"
"B<int >I<length>B<);>\n"
msgstr ""

# type: Plain text
#: resolver.3:84
msgid "Link with -lresolv."
msgstr ""

# type: SH
#: resolver.3:84
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: resolver.3:87
msgid ""
"These functions make queries to and interpret the responses from Internet "
"domain name servers."
msgstr ""

# type: Plain text
#: resolver.3:95
msgid ""
"The B<res_init()> function reads the configuration files (see resolv+(8)) to "
"get the default domain name, search order and name server address(es).  If "
"no server is given, the local host is tried.  If no domain is given, that "
"associated with the local host is used.  It can be overridden with the "
"environment variable LOCALDOMAIN.  B<res_init()> is normally executed by the "
"first call to one of the other functions."
msgstr ""

# type: Plain text
#: resolver.3:100
msgid ""
"The B<res_query()> function queries the name server for the fully-qualified "
"domain name I<name> of specified I<type> and I<class>.  The reply is left in "
"the buffer I<answer> of length I<anslen> supplied by the caller."
msgstr ""

# type: Plain text
#: resolver.3:105
msgid ""
"The B<res_search()> function makes a query and waits for the response like "
"B<res_query()>, but in addition implements the default and search rules "
"controlled by RES_DEFNAMES and RES_DNSRCH (see description of I<_res> "
"options below)."
msgstr ""

# type: Plain text
#: resolver.3:108
msgid ""
"The B<res_querydomain()> function makes a query using B<res_query()> on the "
"concatenation of I<name> and I<domain>."
msgstr ""

# type: Plain text
#: resolver.3:110
msgid "The following functions are lower-level routines used by B<res_query()>."
msgstr ""

# type: Plain text
#: resolver.3:115
msgid ""
"The B<res_mkquery()> function constructs a query message in I<buf> of length "
"I<buflen> for the domain name I<dname>.  The query type I<op> is usually "
"QUERY, but can be any of the types defined in I<E<lt>arpa/nameser.hE<gt>>.  "
"I<newrr> is currently unused."
msgstr ""

# type: Plain text
#: resolver.3:120
msgid ""
"The B<res_send()> function sends a pre-formatted query given in I<msg> of "
"length I<msglen> and returns the answer in I<answer> which is of length "
"I<anslen>.  It will call B<res_init()>, if it has not already been called."
msgstr ""

# type: Plain text
#: resolver.3:129
msgid ""
"The B<dn_comp()> function compresses the domain name I<exp_dn> and stores it "
"in the buffer I<comp_dn> of length I<length>.  The compression uses an array "
"of pointers I<dnptrs> to previously compressed names in the current "
"message.  The first pointer points to the beginning of the message and the "
"list ends with NULL.  The limit of the array is specified by I<lastdnptr>.  "
"if I<dnptr> is NULL, domain names are not compressed.  If I<lastdnptr> is "
"NULL, the list of labels is not updated."
msgstr ""

# type: Plain text
#: resolver.3:135
msgid ""
"The dn_expand() function expands the compressed domain name I<comp_dn> to a "
"full domain name, which is placed in the buffer I<exp_dn> of size "
"I<length>.  The compressed name is contained in a query or reply message, "
"and I<msg> points to the beginning of the message."
msgstr ""

# type: Plain text
#: resolver.3:141
msgid ""
"The resolver routines use global configuration and state information "
"contained in the structure I<_res>, which is defined in "
"I<E<lt>resolv.hE<gt>>.  The only field that is normally manipulated by the "
"user is I<_res.options>.  This field can contain the bitwise ``or'' of the "
"following options:"
msgstr ""

# type: TP
#: resolver.3:142
#, no-wrap
msgid "B<RES_INIT>"
msgstr ""

# type: Plain text
#: resolver.3:145
msgid "True if B<res_init()> has been called."
msgstr ""

# type: TP
#: resolver.3:145
#, no-wrap
msgid "B<RES_DEBUG>"
msgstr ""

# type: Plain text
#: resolver.3:148
msgid "Print debugging messages."
msgstr ""

# type: TP
#: resolver.3:148
#, no-wrap
msgid "B<RES_AAONLY>"
msgstr ""

# type: Plain text
#: resolver.3:153
msgid ""
"Accept authoritative answers only.  B<res_send()> continues until it fins an "
"authoritative answer or returns an error.  [Not currently implemented]."
msgstr ""

# type: TP
#: resolver.3:153
#, no-wrap
msgid "B<RES_USEVC>"
msgstr ""

# type: Plain text
#: resolver.3:156
msgid "Use TCP connections for queries rather than UDP datagrams."
msgstr ""

# type: TP
#: resolver.3:156
#, no-wrap
msgid "B<RES_PRIMARY>"
msgstr ""

# type: Plain text
#: resolver.3:159
msgid "Query primary domain name server only."
msgstr ""

# type: TP
#: resolver.3:159
#, no-wrap
msgid "B<RES_IGNTC>"
msgstr ""

# type: Plain text
#: resolver.3:163
msgid ""
"Ignore truncation errors.  Don't retry with TCP.  [Not currently "
"implemented]."
msgstr ""

# type: TP
#: resolver.3:163
#, no-wrap
msgid "B<RES_RECURSE>"
msgstr ""

# type: Plain text
#: resolver.3:168
msgid ""
"Set the recursion desired bit in queries.  Recursion is carried out by the "
"domain name server, not by B<res_send()>.  [Enabled by default]."
msgstr ""

# type: TP
#: resolver.3:168
#, no-wrap
msgid "B<RES_DEFNAMES>"
msgstr ""

# type: Plain text
#: resolver.3:173
msgid ""
"If set, B<res_search()> will append the default domain name to single "
"component names, ie. those that do not contain a dot.  [Enabled by default]."
msgstr ""

# type: TP
#: resolver.3:173
#, no-wrap
msgid "B<RES_STAYOPEN>"
msgstr ""

# type: Plain text
#: resolver.3:176
msgid "Used with RES_USEVC to keep the TCP connection open between queries."
msgstr ""

# type: TP
#: resolver.3:176
#, no-wrap
msgid "B<RES_DNSRCH>"
msgstr ""

# type: Plain text
#: resolver.3:182
msgid ""
"If set, B<res_search()> will search for host names in the current domain and "
"in parent domains.  This option is used by B<gethostbyname>(3).  [Enabled by "
"default]."
msgstr ""

# type: SH
#: resolver.3:182
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: resolver.3:185
msgid "The B<res_init()> function returns 0 on success, or -1 if an error occurs."
msgstr ""

# type: Plain text
#: resolver.3:189
msgid ""
"The B<res_query()>, B<res_search()>, B<res_querydomain()>, B<res_mkquery()> "
"and B<res_send()> functions return the length of the response, or -1 if an "
"error occurs."
msgstr ""

# type: Plain text
#: resolver.3:192
msgid ""
"The B<dn_comp()> and B<dn_expand()> functions return the length of the "
"compressed name, or -1 if an error occurs."
msgstr ""

# type: SH
#: resolver.3:192
#, no-wrap
msgid "FILES"
msgstr ""

# type: Plain text
#: resolver.3:196
#, no-wrap
msgid ""
"/etc/resolv.conf          resolver configuration file\n"
"/etc/host.conf            resolver configuration file\n"
msgstr ""

# type: SH
#: resolver.3:197
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: resolver.3:199
msgid "BSD 4.3"
msgstr ""

# type: SH
#: resolver.3:199
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: resolver.3:203
msgid "B<gethostbyname>(3), B<hostname>(7), B<named>(8), B<resolv+>(8)"
msgstr ""
