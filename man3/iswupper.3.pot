# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: iswupper.3:14
#, no-wrap
msgid "ISWUPPER"
msgstr ""

# type: TH
#: iswupper.3:14
#, no-wrap
msgid "1999-07-25"
msgstr ""

# type: TH
#: iswupper.3:14
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: iswupper.3:14
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: iswupper.3:15
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: iswupper.3:17
msgid "iswupper - test for uppercase wide character"
msgstr ""

# type: SH
#: iswupper.3:17
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: iswupper.3:20
#, no-wrap
msgid "B<#include E<lt>wctype.hE<gt>>\n"
msgstr ""

# type: Plain text
#: iswupper.3:22
#, no-wrap
msgid "B<int iswupper(wint_t >I<wc>B<);>\n"
msgstr ""

# type: SH
#: iswupper.3:23
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: iswupper.3:27
msgid ""
"The B<iswupper> function is the wide-character equivalent of the B<isupper> "
"function. It tests whether I<wc> is a wide character belonging to the wide "
"character class \"upper\"."
msgstr ""

# type: Plain text
#: iswupper.3:31
msgid ""
"The wide character class \"upper\" is a subclass of the wide character class "
"\"alpha\", and therefore also a subclass of the wide character class "
"\"alnum\", of the wide character class \"graph\" and of the wide character "
"class \"print\"."
msgstr ""

# type: Plain text
#: iswupper.3:34
msgid ""
"Being a subclass of the wide character class \"print\", the wide character "
"class \"upper\" is disjoint from the wide character class \"cntrl\"."
msgstr ""

# type: Plain text
#: iswupper.3:38
msgid ""
"Being a subclass of the wide character class \"graph\", the wide character "
"class \"upper\" is disjoint from the wide character class \"space\" and its "
"subclass \"blank\"."
msgstr ""

# type: Plain text
#: iswupper.3:41
msgid ""
"Being a subclass of the wide character class \"alnum\", the wide character "
"class \"upper\" is disjoint from the wide character class \"punct\"."
msgstr ""

# type: Plain text
#: iswupper.3:44
msgid ""
"Being a subclass of the wide character class \"alpha\", the wide character "
"class \"upper\" is disjoint from the wide character class \"digit\"."
msgstr ""

# type: Plain text
#: iswupper.3:47
msgid ""
"The wide character class \"upper\" contains at least those characters I<wc> "
"which are equal to I<towupper(wc)> and different from I<towlower(wc)>."
msgstr ""

# type: Plain text
#: iswupper.3:50
msgid ""
"The wide character class \"upper\" always contains at least the letters 'A' "
"to 'Z'."
msgstr ""

# type: SH
#: iswupper.3:50
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: iswupper.3:53
msgid ""
"The B<iswupper> function returns non-zero if I<wc> is a wide character "
"belonging to the wide character class \"upper\". Otherwise it returns zero."
msgstr ""

# type: SH
#: iswupper.3:53
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: iswupper.3:55
msgid "ISO/ANSI C, UNIX98"
msgstr ""

# type: SH
#: iswupper.3:55
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: iswupper.3:59
msgid "B<isupper>(3), B<iswctype>(3), B<towupper>(3)"
msgstr ""

# type: SH
#: iswupper.3:59
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: iswupper.3:62
msgid ""
"The behaviour of B<iswupper> depends on the LC_CTYPE category of the current "
"locale."
msgstr ""

# type: Plain text
#: iswupper.3:64
msgid ""
"This function is not very appropriate for dealing with Unicode characters, "
"because Unicode knows about three cases: upper, lower and title case."
msgstr ""
