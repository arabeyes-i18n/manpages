# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: iswxdigit.3:14
#, no-wrap
msgid "ISWXDIGIT"
msgstr ""

# type: TH
#: iswxdigit.3:14
#, no-wrap
msgid "1999-07-25"
msgstr ""

# type: TH
#: iswxdigit.3:14
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: iswxdigit.3:14
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: iswxdigit.3:15
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: iswxdigit.3:17
msgid "iswxdigit - test for hexadecimal digit wide character"
msgstr ""

# type: SH
#: iswxdigit.3:17
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: iswxdigit.3:20
#, no-wrap
msgid "B<#include E<lt>wctype.hE<gt>>\n"
msgstr ""

# type: Plain text
#: iswxdigit.3:22
#, no-wrap
msgid "B<int iswxdigit(wint_t >I<wc>B<);>\n"
msgstr ""

# type: SH
#: iswxdigit.3:23
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: iswxdigit.3:27
msgid ""
"The B<iswxdigit> function is the wide-character equivalent of the "
"B<isxdigit> function. It tests whether I<wc> is a wide character belonging "
"to the wide character class \"xdigit\"."
msgstr ""

# type: Plain text
#: iswxdigit.3:31
msgid ""
"The wide character class \"xdigit\" is a subclass of the wide character "
"class \"alnum\", and therefore also a subclass of the wide character class "
"\"graph\" and of the wide character class \"print\"."
msgstr ""

# type: Plain text
#: iswxdigit.3:34
msgid ""
"Being a subclass of the wide character class \"print\", the wide character "
"class \"xdigit\" is disjoint from the wide character class \"cntrl\"."
msgstr ""

# type: Plain text
#: iswxdigit.3:38
msgid ""
"Being a subclass of the wide character class \"graph\", the wide character "
"class \"xdigit\" is disjoint from the wide character class \"space\" and its "
"subclass \"blank\"."
msgstr ""

# type: Plain text
#: iswxdigit.3:41
msgid ""
"Being a subclass of the wide character class \"alnum\", the wide character "
"class \"xdigit\" is disjoint from the wide character class \"punct\"."
msgstr ""

# type: Plain text
#: iswxdigit.3:44
msgid ""
"The wide character class \"xdigit\" always contains at least the letters 'A' "
"to 'F', 'a' to 'f' and the digits '0' to '9'."
msgstr ""

# type: SH
#: iswxdigit.3:44
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: iswxdigit.3:47
msgid ""
"The B<iswxdigit> function returns non-zero if I<wc> is a wide character "
"belonging to the wide character class \"xdigit\". Otherwise it returns zero."
msgstr ""

# type: SH
#: iswxdigit.3:47
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: iswxdigit.3:49
msgid "ISO/ANSI C, UNIX98"
msgstr ""

# type: SH
#: iswxdigit.3:49
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: iswxdigit.3:52
msgid "B<iswctype>(3), B<isxdigit>(3)"
msgstr ""

# type: SH
#: iswxdigit.3:52
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: iswxdigit.3:54
msgid ""
"The behaviour of B<iswxdigit> depends on the LC_CTYPE category of the "
"current locale."
msgstr ""
