# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: acosh.3:31
#, no-wrap
msgid "ACOSH"
msgstr ""

# type: TH
#: acosh.3:31
#, no-wrap
msgid "2002-07-25"
msgstr ""

# type: TH
#: acosh.3:31
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: acosh.3:32
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: acosh.3:34
msgid "acosh, acoshf, acoshl - inverse hyperbolic cosine function"
msgstr ""

# type: SH
#: acosh.3:34
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: acosh.3:37
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr ""

# type: Plain text
#: acosh.3:39
#, no-wrap
msgid "B<double acosh(double >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: acosh.3:41
#, no-wrap
msgid "B<float acoshf(float >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: acosh.3:43
#, no-wrap
msgid "B<long double acoshl(long double >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: acosh.3:46
msgid "Link with -lm."
msgstr ""

# type: SH
#: acosh.3:46
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: acosh.3:51
msgid ""
"The B<acosh()> function calculates the inverse hyperbolic cosine of I<x>; "
"that is the value whose hyperbolic cosine is I<x>.  If I<x> is less than "
"1.0, B<acosh()> returns not-a-number (NaN) and I<errno> is set."
msgstr ""

# type: SH
#: acosh.3:51
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: TP
#: acosh.3:52
#, no-wrap
msgid "B<EDOM>"
msgstr ""

# type: Plain text
#: acosh.3:55
msgid "I<x> is out of range."
msgstr ""

# type: SH
#: acosh.3:55
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: acosh.3:58
msgid ""
"SVID 3, POSIX, BSD 4.3, ISO 9899.  The float and long double variants are "
"C99 requirements."
msgstr ""

# type: SH
#: acosh.3:58
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: acosh.3:64
msgid "B<asinh>(3), B<atanh>(3), B<cacosh>(3), B<cosh>(3), B<sinh>(3), B<tanh>(3)"
msgstr ""
