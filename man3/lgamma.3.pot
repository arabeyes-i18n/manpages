# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: lgamma.3:5
#, no-wrap
msgid "LGAMMA"
msgstr ""

# type: TH
#: lgamma.3:5
#, no-wrap
msgid "2002-08-10"
msgstr ""

# type: TH
#: lgamma.3:5
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: lgamma.3:6
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: lgamma.3:8
msgid ""
"lgamma, lgammaf, lgammal, lgamma_r, lgammaf_r, lgammal_r - log gamma "
"function"
msgstr ""

# type: SH
#: lgamma.3:8
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: lgamma.3:11
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr ""

# type: Plain text
#: lgamma.3:13
#, no-wrap
msgid "B<double lgamma(double >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: lgamma.3:15
#, no-wrap
msgid "B<float lgammaf(float >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: lgamma.3:17
#, no-wrap
msgid "B<long double lgammal(long double >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: lgamma.3:19
#, no-wrap
msgid "B<double lgamma_r(double >I<x>B<, int *>I<signp>B<);>\n"
msgstr ""

# type: Plain text
#: lgamma.3:21
#, no-wrap
msgid "B<float lgammaf_r(float >I<x>B<, int *>I<signp>B<);>\n"
msgstr ""

# type: Plain text
#: lgamma.3:23
#, no-wrap
msgid "B<long double lgammal_r(long double >I<x>B<, int *>I<signp>B<);>\n"
msgstr ""

# type: Plain text
#: lgamma.3:26
msgid "Compile with -std=c99; link with -lm."
msgstr ""

# type: SH
#: lgamma.3:26
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: lgamma.3:29
msgid "For the definition of the Gamma function, see B<tgamma>(3)."
msgstr ""

# type: Plain text
#: lgamma.3:37
msgid ""
"The B<lgamma()> function returns the natural logarithm of the absolute value "
"of the Gamma function.  The sign of the Gamma function is returned in the "
"external integer I<signgam> declared in I<E<lt>math.hE<gt>>.  It is 1 when "
"the Gamma function is positive or zero, -1 when it is negative."
msgstr ""

# type: Plain text
#: lgamma.3:43
msgid ""
"Since using a constant location I<signgam> is not thread-safe, the functions "
"B<lgamma_r()> etc. have been introduced; they return this sign via the "
"parameter I<signp>."
msgstr ""

# type: Plain text
#: lgamma.3:48
msgid ""
"For nonpositive integer values of I<x>, B<lgamma()> returns HUGE_VAL, sets "
"I<errno> to ERANGE and raises the zero divide exception.  (Similarly, "
"B<lgammaf()> returns HUGE_VALF and B<lgammal()> returns HUGE_VALL.)"
msgstr ""

# type: SH
#: lgamma.3:48
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: Plain text
#: lgamma.3:58
msgid ""
"In order to check for errors, set I<errno> to zero and call "
"I<feclearexcept(FE_ALL_EXCEPT)> before calling these functions. On return, "
"if I<errno> is non-zero or I<fetestexcept(FE_INVALID | FE_DIVBYZERO | "
"FE_OVERFLOW | FE_UNDERFLOW)> is non-zero, an error has occurred."
msgstr ""

# type: Plain text
#: lgamma.3:61
msgid ""
"A range error occurs if x is too large.  A pole error occurs if x is a "
"negative integer or zero."
msgstr ""

# type: SH
#: lgamma.3:61
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: lgamma.3:63
msgid "C99, SVID 3, BSD 4.3"
msgstr ""

# type: SH
#: lgamma.3:63
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: lgamma.3:64
msgid "B<tgamma>(3)"
msgstr ""
