# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: cos.3:30
#, no-wrap
msgid "COS"
msgstr ""

# type: TH
#: cos.3:30
#, no-wrap
msgid "2002-07-27"
msgstr ""

# type: TH
#: cos.3:30
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: cos.3:31
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: cos.3:33
msgid "cos, cosf, cosl - cosine function"
msgstr ""

# type: SH
#: cos.3:33
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: cos.3:36
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr ""

# type: Plain text
#: cos.3:38
#, no-wrap
msgid "B<double cos(double >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: cos.3:40
#, no-wrap
msgid "B<float cosf(float >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: cos.3:42
#, no-wrap
msgid "B<long double cosl(long double >I<x>B<);>\n"
msgstr ""

# type: Plain text
#: cos.3:45
msgid "Link with -lm."
msgstr ""

# type: SH
#: cos.3:45
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: cos.3:48
msgid ""
"The B<cos()> function returns the cosine of I<x>, where I<x> is given in "
"radians."
msgstr ""

# type: SH
#: cos.3:48
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: cos.3:50
msgid "The B<cos()> function returns a value between -1 and 1."
msgstr ""

# type: SH
#: cos.3:50
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: cos.3:53
msgid ""
"SVID 3, POSIX, BSD 4.3, ISO 9899.  The float and the long double variants "
"are C99 requirements."
msgstr ""

# type: SH
#: cos.3:53
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: cos.3:60
msgid ""
"B<acos>(3), B<asin>(3), B<atan>(3), B<atan2>(3), B<ccos>(3), B<sin>(3), "
"B<tan>(3)"
msgstr ""
