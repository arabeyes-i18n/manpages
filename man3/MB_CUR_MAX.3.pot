# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: MB_CUR_MAX.3:15
#, no-wrap
msgid "MB_CUR_MAX"
msgstr ""

# type: TH
#: MB_CUR_MAX.3:15
#, no-wrap
msgid "1999-07-04"
msgstr ""

# type: TH
#: MB_CUR_MAX.3:15
#, no-wrap
msgid "Linux"
msgstr ""

# type: TH
#: MB_CUR_MAX.3:15
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: MB_CUR_MAX.3:16
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: MB_CUR_MAX.3:18
msgid "MB_CUR_MAX - maximum length of a multibyte character in the current locale"
msgstr ""

# type: SH
#: MB_CUR_MAX.3:18
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: MB_CUR_MAX.3:21
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr ""

# type: SH
#: MB_CUR_MAX.3:22
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: MB_CUR_MAX.3:29
msgid ""
"The B<MB_CUR_MAX> macro defines an integer expression giving the maximum "
"number of bytes needed to represent a single wide character in the current "
"locale.  It is locale dependent and therefore not a compile-time constant."
msgstr ""

# type: SH
#: MB_CUR_MAX.3:29
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: MB_CUR_MAX.3:32
msgid ""
"An integer E<gt>= 1 and E<lt>= MB_LEN_MAX.  The value 1 denotes traditional "
"8-bit encoded characters."
msgstr ""

# type: SH
#: MB_CUR_MAX.3:32
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: MB_CUR_MAX.3:34
msgid "ANSI C, POSIX.1"
msgstr ""

# type: SH
#: MB_CUR_MAX.3:34
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: MB_CUR_MAX.3:40
msgid ""
"B<MB_LEN_MAX>(3), B<mblen>(3), B<mbstowcs>(3), B<mbtowc>(3), B<wcstombs>(3), "
"B<wctomb>(3)"
msgstr ""
