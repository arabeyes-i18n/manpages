# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: getdate.3:24
#, no-wrap
msgid "GETDATE"
msgstr ""

# type: TH
#: getdate.3:24
#, no-wrap
msgid "2001-12-26"
msgstr ""

# type: TH
#: getdate.3:24
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: getdate.3:25
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: getdate.3:27
msgid "getdate() - convert a string to struct tm"
msgstr ""

# type: SH
#: getdate.3:28
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: getdate.3:30
msgid "B<#define _XOPEN_SOURCE>"
msgstr ""

# type: Plain text
#: getdate.3:32
msgid "B<#define _XOPEN_SOURCE_EXTENDED>"
msgstr ""

# type: Plain text
#: getdate.3:34 getdate.3:42
msgid "B<#include E<lt>time.hE<gt>>"
msgstr ""

# type: Plain text
#: getdate.3:36
msgid "B<struct tm *getdate (const char *>I<string>B<);>"
msgstr ""

# type: Plain text
#: getdate.3:38
msgid "B<extern int getdate_err;>"
msgstr ""

# type: Plain text
#: getdate.3:40
msgid "B<#define _GNU_SOURCE>"
msgstr ""

# type: Plain text
#: getdate.3:44
msgid "B<int getdate_r (const char *>I<string>B<, struct tm *>I<res>B<);>"
msgstr ""

# type: SH
#: getdate.3:45
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: getdate.3:53
msgid ""
"The function B<getdate()> converts a string pointed to by I<string> into the "
"tm structure that it returns.  This tm structure may be found in static "
"storage, so that it will be overwritten by the next call."
msgstr ""

# type: Plain text
#: getdate.3:65
msgid ""
"In contrast to B<strptime>(3), (which has a I<format> argument), "
"B<getdate()> uses the formats found in the file of which the full path name "
"is given in the environment variable B<DATEMSK>.  The first line in the file "
"that matches the given input string is used for the conversion."
msgstr ""

# type: Plain text
#: getdate.3:69
msgid ""
"The matching is done case insensitively.  Superfluous whitespace, either in "
"the pattern or in the string to be converted, is ignored."
msgstr ""

# type: Plain text
#: getdate.3:73
msgid ""
"The conversion specifications that a pattern can contain are those given for "
"B<strptime>(3).  One more conversion specification is accepted:"
msgstr ""

# type: TP
#: getdate.3:73
#, no-wrap
msgid "B<%Z>"
msgstr ""

# type: Plain text
#: getdate.3:76
msgid "Timezone name."
msgstr ""

# type: Plain text
#: getdate.3:83
msgid ""
"When B<%Z> is given, the value to be returned is initialised to the "
"broken-down time corresponding to the current time in the given time zone.  "
"Otherwise, it is initialised to the broken-down time corresponding to the "
"current local time."
msgstr ""

# type: Plain text
#: getdate.3:86
msgid ""
"When only the weekday is given, the day is taken to be the first such day on "
"or after today."
msgstr ""

# type: Plain text
#: getdate.3:90
msgid ""
"When only the month is given (and no year), the month is taken to be the "
"first such month equal to or after the current month.  If no day is given, "
"it is the first day of the month."
msgstr ""

# type: Plain text
#: getdate.3:93
msgid ""
"When no hour, minute and second are given, the current hour, minute and "
"second are taken."
msgstr ""

# type: Plain text
#: getdate.3:96
msgid ""
"If no date is given, but we know the hour, then that hour is taken to be the "
"first such hour equal to or after the current hour."
msgstr ""

# type: SH
#: getdate.3:96
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: getdate.3:106
msgid ""
"When successful, this function returns a pointer to a B<struct tm>.  "
"Otherwise, it returns NULL and sets the global variable B<getdate_err>.  "
"Changes to I<errno> are unspecified.  The following values for "
"B<getdate_err> are defined:"
msgstr ""

# type: TP
#: getdate.3:106
#, no-wrap
msgid "B<1>"
msgstr ""

# type: Plain text
#: getdate.3:109
msgid "The DATEMSK environment variable is null or undefined."
msgstr ""

# type: TP
#: getdate.3:109
#, no-wrap
msgid "B<2>"
msgstr ""

# type: Plain text
#: getdate.3:112
msgid "The template file cannot be opened for reading."
msgstr ""

# type: TP
#: getdate.3:112
#, no-wrap
msgid "B<3>"
msgstr ""

# type: Plain text
#: getdate.3:115
msgid "Failed to get file status information."
msgstr ""

# type: TP
#: getdate.3:115
#, no-wrap
msgid "B<4>"
msgstr ""

# type: Plain text
#: getdate.3:118
msgid "The template file is not a regular file."
msgstr ""

# type: TP
#: getdate.3:118
#, no-wrap
msgid "B<5>"
msgstr ""

# type: Plain text
#: getdate.3:121
msgid "An error is encountered while reading the template file."
msgstr ""

# type: TP
#: getdate.3:121
#, no-wrap
msgid "B<6>"
msgstr ""

# type: Plain text
#: getdate.3:124
msgid "Memory allocation failed (not enough memory available)."
msgstr ""

# type: TP
#: getdate.3:124
#, no-wrap
msgid "B<7>"
msgstr ""

# type: Plain text
#: getdate.3:127
msgid "There is no line in the file that matches the input."
msgstr ""

# type: TP
#: getdate.3:127
#, no-wrap
msgid "B<8>"
msgstr ""

# type: Plain text
#: getdate.3:130
msgid "Invalid input specification."
msgstr ""

# type: SH
#: getdate.3:130
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: getdate.3:142
msgid ""
"Since B<getdate()> is not reentrant because of the use of B<getdate_err> and "
"the static buffer to return the result in, glibc provides a thread-safe "
"variant.  The functionality is the same.  The result is returned in the "
"buffer pointed to by I<res> and in case of an error the return value is "
"nonzero with the same values as given above for I<getdate_err>."
msgstr ""

# type: Plain text
#: getdate.3:156
msgid ""
"The POSIX 1003.1-2001 specification for B<strptime()> contains conversion "
"specifications using the B<%E> or B<%O> modifier, while such specifications "
"are not given for B<getdate()>.  The glibc implementation implements "
"B<getdate()> using B<strptime()> so that automatically precisely the same "
"conversions are supported by both."
msgstr ""

# type: Plain text
#: getdate.3:160
msgid ""
"The glibc implementation does not support the B<%Z> conversion "
"specification."
msgstr ""

# type: SH
#: getdate.3:160
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

# type: TP
#: getdate.3:161
#, no-wrap
msgid "B<DATEMSK>"
msgstr ""

# type: Plain text
#: getdate.3:164
msgid "File containing format patterns."
msgstr ""

# type: TP
#: getdate.3:164
#, no-wrap
msgid "B<TZ>, B<LC_TIME>"
msgstr ""

# type: Plain text
#: getdate.3:167
msgid "Variables used by B<strptime()>."
msgstr ""

# type: SH
#: getdate.3:167
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: getdate.3:169
msgid "ISO 9899, POSIX 1003.1-2001"
msgstr ""

# type: SH
#: getdate.3:169
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: getdate.3:173
msgid "B<localtime>(3), B<strftime>(3), B<strptime>(3), B<time>(3)"
msgstr ""
