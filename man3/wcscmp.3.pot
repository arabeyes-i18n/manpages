# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:10+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: wcscmp.3:14
#, no-wrap
msgid "WCSCMP"
msgstr ""

# type: TH
#: wcscmp.3:14
#, no-wrap
msgid "1999-07-25"
msgstr ""

# type: TH
#: wcscmp.3:14
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: wcscmp.3:14
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: wcscmp.3:15
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: wcscmp.3:17
msgid "wcscmp - compare two wide-character strings"
msgstr ""

# type: SH
#: wcscmp.3:17
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: wcscmp.3:20
#, no-wrap
msgid "B<#include E<lt>wchar.hE<gt>>\n"
msgstr ""

# type: Plain text
#: wcscmp.3:22
#, no-wrap
msgid "B<int wcscmp(const wchar_t *>I<s1>B<, const wchar_t *>I<s2>B<);>\n"
msgstr ""

# type: SH
#: wcscmp.3:23
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: wcscmp.3:27
msgid ""
"The B<wcscmp> function is the wide-character equivalent of the B<strcmp> "
"function. It compares the wide-character string pointed to by I<s1> and the "
"wide-character string pointed to by I<s2>."
msgstr ""

# type: SH
#: wcscmp.3:27
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: wcscmp.3:34
msgid ""
"The B<wcscmp> function returns zero if the wide-character strings at I<s1> "
"and I<s2> are equal. It returns an integer greater than zero if at the first "
"differing position I<i>, the corresponding wide-character I<s1[i]> is "
"greater than I<s2[i]>. It returns an integer less than zero if at the first "
"differing position I<i>, the corresponding wide-character I<s1[i]> is less "
"than I<s2[i]>."
msgstr ""

# type: SH
#: wcscmp.3:34
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: wcscmp.3:36
msgid "ISO/ANSI C, UNIX98"
msgstr ""

# type: SH
#: wcscmp.3:36
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: wcscmp.3:39
msgid "B<strcmp>(3), B<wcscasecmp>(3), B<wmemcmp>(3)"
msgstr ""
