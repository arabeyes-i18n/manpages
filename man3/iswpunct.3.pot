# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:08+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: iswpunct.3:14
#, no-wrap
msgid "ISWPUNCT"
msgstr ""

# type: TH
#: iswpunct.3:14
#, no-wrap
msgid "1999-07-25"
msgstr ""

# type: TH
#: iswpunct.3:14
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: iswpunct.3:14
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: iswpunct.3:15
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: iswpunct.3:17
msgid "iswpunct - test for punctuation or symbolic wide character"
msgstr ""

# type: SH
#: iswpunct.3:17
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: iswpunct.3:20
#, no-wrap
msgid "B<#include E<lt>wctype.hE<gt>>\n"
msgstr ""

# type: Plain text
#: iswpunct.3:22
#, no-wrap
msgid "B<int iswpunct(wint_t >I<wc>B<);>\n"
msgstr ""

# type: SH
#: iswpunct.3:23
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: iswpunct.3:27
msgid ""
"The B<iswpunct> function is the wide-character equivalent of the B<ispunct> "
"function. It tests whether I<wc> is a wide character belonging to the wide "
"character class \"punct\"."
msgstr ""

# type: Plain text
#: iswpunct.3:30
msgid ""
"The wide character class \"punct\" is a subclass of the wide character class "
"\"graph\", and therefore also a subclass of the wide character class "
"\"print\"."
msgstr ""

# type: Plain text
#: iswpunct.3:34
msgid ""
"The wide character class \"punct\" is disjoint from the wide character class "
"\"alnum\" and therefore also disjoint from its subclasses \"alpha\", "
"\"upper\", \"lower\", \"digit\", \"xdigit\"."
msgstr ""

# type: Plain text
#: iswpunct.3:37
msgid ""
"Being a subclass of the wide character class \"print\", the wide character "
"class \"punct\" is disjoint from the wide character class \"cntrl\"."
msgstr ""

# type: Plain text
#: iswpunct.3:41
msgid ""
"Being a subclass of the wide character class \"graph\", the wide character "
"class \"punct\" is disjoint from the wide character class \"space\" and its "
"subclass \"blank\"."
msgstr ""

# type: SH
#: iswpunct.3:41
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: iswpunct.3:44
msgid ""
"The B<iswpunct> function returns non-zero if I<wc> is a wide character "
"belonging to the wide character class \"punct\". Otherwise it returns zero."
msgstr ""

# type: SH
#: iswpunct.3:44
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: iswpunct.3:46
msgid "ISO/ANSI C, UNIX98"
msgstr ""

# type: SH
#: iswpunct.3:46
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: iswpunct.3:49
msgid "B<ispunct>(3), B<iswctype>(3)"
msgstr ""

# type: SH
#: iswpunct.3:49
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: iswpunct.3:52
msgid ""
"The behaviour of B<iswpunct> depends on the LC_CTYPE category of the current "
"locale."
msgstr ""

# type: Plain text
#: iswpunct.3:55
msgid ""
"This function's name is a misnomer when dealing with Unicode characters, "
"because the wide character class \"punct\" contains both punctuation "
"characters and symbol (math, currency, etc.) characters."
msgstr ""
