# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: setlogmask.3:23
#, no-wrap
msgid "SETLOGMASK"
msgstr ""

# type: TH
#: setlogmask.3:23
#, no-wrap
msgid "2001-10-05"
msgstr ""

# type: TH
#: setlogmask.3:23
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: setlogmask.3:24
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: setlogmask.3:26
msgid "setlogmask - set log priority mask"
msgstr ""

# type: SH
#: setlogmask.3:26
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: setlogmask.3:29
#, no-wrap
msgid "B<#include E<lt>syslog.hE<gt>>\n"
msgstr ""

# type: Plain text
#: setlogmask.3:31
#, no-wrap
msgid "B<int setlogmask(int >I<mask>B<);>\n"
msgstr ""

# type: SH
#: setlogmask.3:32
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: setlogmask.3:40
msgid ""
"A process has a log priority mask that determines which calls to "
"B<syslog>(3)  may be logged. All other calls will be ignored.  Logging is "
"enabled for the priorities that have the corresponding bit set in I<mask>.  "
"The initial mask is such that logging is enabled for all priorities."
msgstr ""

# type: Plain text
#: setlogmask.3:46
msgid ""
"The B<setlogmask()> function sets this logmask for the current process, and "
"returns the previous mask.  If the mask argument is 0, the current logmask "
"is not modified."
msgstr ""

# type: Plain text
#: setlogmask.3:52
msgid ""
"The eight priorities are LOG_EMERG, LOG_ALERT, LOG_CRIT, LOG_ERR, "
"LOG_WARNING, LOG_NOTICE, LOG_INFO and LOG_DEBUG.  The bit corresponding to a "
"priority I<p> is LOG_MASK(I<p>).  Some systems also provide a macro "
"LOG_UPTO(I<p>) for the mask of all priorities in the above list up to and "
"including I<p>."
msgstr ""

# type: SH
#: setlogmask.3:52
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

# type: Plain text
#: setlogmask.3:54
msgid "This function returns the previous log priority mask."
msgstr ""

# type: SH
#: setlogmask.3:54
#, no-wrap
msgid "ERRORS"
msgstr ""

# type: Plain text
#: setlogmask.3:56
msgid "None."
msgstr ""

# type: SH
#: setlogmask.3:58
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: setlogmask.3:61
msgid "XPG4.  Note that the description in POSIX 1003.1-2001 is flawed."
msgstr ""

# type: SH
#: setlogmask.3:61
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: setlogmask.3:64
msgid "B<closelog>(3), B<openlog>(3), B<syslog>(3)"
msgstr ""
