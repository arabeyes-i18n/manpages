# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:06+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: copysign.3:29
#, no-wrap
msgid "COPYSIGN"
msgstr ""

# type: TH
#: copysign.3:29
#, no-wrap
msgid "2002-08-10"
msgstr ""

# type: TH
#: copysign.3:29
#, no-wrap
msgid "GNU"
msgstr ""

# type: TH
#: copysign.3:29
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: copysign.3:30
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: copysign.3:32
msgid "copysign, copysignf, copysignl - copy sign of a number"
msgstr ""

# type: SH
#: copysign.3:32
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: copysign.3:35
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr ""

# type: Plain text
#: copysign.3:37
#, no-wrap
msgid "B<double copysign(double >I<x>B<, double >I<y>B<);>\n"
msgstr ""

# type: Plain text
#: copysign.3:39
#, no-wrap
msgid "B<float copysignf(float >I<x>B<, float >I<y>B<);>\n"
msgstr ""

# type: Plain text
#: copysign.3:41
#, no-wrap
msgid "B<long double copysignl(long double >I<x>B<, long double >I<y>B<);>\n"
msgstr ""

# type: Plain text
#: copysign.3:44
msgid "Link with -lm."
msgstr ""

# type: SH
#: copysign.3:44
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: copysign.3:48
msgid ""
"The B<copysign()> functions return a value whose absolute value matches that "
"of I<x>, but whose sign matches that of I<y>.  If I<x> is a NaN, then a NaN "
"with the sign of I<y> is returned."
msgstr ""

# type: SH
#: copysign.3:48
#, no-wrap
msgid "NOTES"
msgstr ""

# type: Plain text
#: copysign.3:50
msgid "The B<copysign()> functions may treat a negative zero as positive."
msgstr ""

# type: SH
#: copysign.3:50
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

# type: Plain text
#: copysign.3:54
msgid ""
"C99, BSD 4.3.  This function is defined in IEC 559 (and the appendix with "
"recommended functions in IEEE 754/IEEE 854)."
msgstr ""

# type: SH
#: copysign.3:54
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: copysign.3:55
msgid "B<signbit>(3)"
msgstr ""
