# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2005-11-20 10:10+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: string.3:28
#, no-wrap
msgid "STRING"
msgstr ""

# type: TH
#: string.3:28
#, no-wrap
msgid "1993-04-09"
msgstr ""

# type: TH
#: string.3:28
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

# type: SH
#: string.3:29
#, no-wrap
msgid "NAME"
msgstr ""

# type: Plain text
#: string.3:34
msgid ""
"strcasecmp, strcat, strchr, strcmp, strcoll, strcpy, strcspn, strdup, "
"strfry, strlen, strncat, strncmp, strncpy, strncasecmp, strpbrk, strrchr, "
"strsep, strspn, strstr, strtok, strxfrm, index, rindex - string operations"
msgstr ""

# type: SH
#: string.3:34
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: string.3:37
#, no-wrap
msgid "B<#include E<lt>strings.hE<gt>>\n"
msgstr ""

# type: Plain text
#: string.3:39
#, no-wrap
msgid "B<int strcasecmp(const char *>I<s1>B<, const char *>I<s2>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:41
#, no-wrap
msgid ""
"B<int strncasecmp(const char *>I<s1>B<, const char *>I<s2>B<, size_t "
">I<n>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:43
#, no-wrap
msgid "B<char *index(const char *>I<s>B<, int >I<c>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:45
#, no-wrap
msgid "B<char *rindex(const char *>I<s>B<, int >I<c>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:47
#, no-wrap
msgid "B<#include E<lt>string.hE<gt>>\n"
msgstr ""

# type: Plain text
#: string.3:49
#, no-wrap
msgid "B<char *strcat(char *>I<dest>B<, const char *>I<src>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:51
#, no-wrap
msgid "B<char *strchr(const char *>I<s>B<, int >I<c>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:53
#, no-wrap
msgid "B<int strcmp(const char *>I<s1>B<, const char *>I<s2>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:55
#, no-wrap
msgid "B<int strcoll(const char *>I<s1>B<, const char *>I<s2>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:57
#, no-wrap
msgid "B<char *strcpy(char *>I<dest>B<, const char *>I<src>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:59
#, no-wrap
msgid "B<size_t strcspn(const char *>I<s>B<, const char *>I<reject>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:61
#, no-wrap
msgid "B<char *strdup(const char *>I<s>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:63
#, no-wrap
msgid "B<char *strfry(char *>I<string>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:65
#, no-wrap
msgid "B<size_t strlen(const char *>I<s>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:67
#, no-wrap
msgid "B<char *strncat(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:69
#, no-wrap
msgid ""
"B<int strncmp(const char *>I<s1>B<, const char *>I<s2>B<, size_t "
">I<n>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:71
#, no-wrap
msgid "B<char *strncpy(char *>I<dest>B<, const char *>I<src>B<, size_t >I<n>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:73
#, no-wrap
msgid "B<char *strpbrk(const char *>I<s>B<, const char *>I<accept>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:75
#, no-wrap
msgid "B<char *strrchr(const char *>I<s>B<, int >I<c>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:77
#, no-wrap
msgid "B<char *strsep(char **>I<stringp>B<, const char *>I<delim>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:79
#, no-wrap
msgid "B<size_t strspn(const char *>I<s>B<, const char *>I<accept>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:81
#, no-wrap
msgid "B<char *strstr(const char *>I<haystack>B<, const char *>I<needle>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:83
#, no-wrap
msgid "B<char *strtok(char *>I<s>B<, const char *>I<delim>B<);>\n"
msgstr ""

# type: Plain text
#: string.3:85
#, no-wrap
msgid ""
"B<size_t strxfrm(char *>I<dest>B<, const char *>I<src>B<, size_t "
">I<n>B<);>\n"
msgstr ""

# type: SH
#: string.3:86
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: Plain text
#: string.3:90
msgid ""
"The string functions perform string operations on NUL-terminated strings.  "
"See the individual man pages for descriptions of each function."
msgstr ""

# type: SH
#: string.3:90
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: Plain text
#: string.3:113
msgid ""
"B<index>(3), B<rindex>(3), B<strcasecmp>(3), B<strcat>(3), B<strchr>(3), "
"B<strcmp>(3), B<strcoll>(3), B<strcpy>(3), B<strcspn>(3), B<strdup>(3), "
"B<strfry>(3), B<strlen>(3), B<strncasecmp>(3), B<strncat>(3), B<strncmp>(3), "
"B<strncpy>(3), B<strpbrk>(3), B<strrchr>(3), B<strsep>(3), B<strspn>(3), "
"B<strstr>(3), B<strtok>(3), B<strxfrm>(3)"
msgstr ""
